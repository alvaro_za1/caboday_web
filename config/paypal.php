<?php

return [

  /** set your paypal credential **/
  'client_id' => 'AUNrgbFqrnIMWLne5rn3-eYS5tx2eQPWJAm_i-w7v1oJ-pQlHBvttjPznEEThpvvmCeMGnlWrvq4YD2x',
  'secret' => 'EFfMFpjtkjCfMC2iUT0jBygWnE28Qkttfg1jAFM5l40W7tzcDv_w0pXX-VAgrcZOYrK3xF_nT3YIsPW4',

  /**
  * SDK configuration
  */
  'settings' => array(
    /**
    * Available option 'sandbox' or 'live'
    */
    'mode' => 'sandbox',

    /**
    * Specify the max request time in seconds
    */
    'http.ConnectionTimeOut' => 1000,

    /**
    * Whether want to log to a file
    */
    'log.LogEnabled' => true,

    /**
    * Specify the file that want to write on
    */

    'log.FileName' => storage_path() . '/logs/paypal.log',

    /**
    * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
    *
    * Logging is most verbose in the 'FINE' level and decreases as you
    * proceed towards ERROR
    */
    'log.LogLevel' => 'FINE'
  )
  
];
