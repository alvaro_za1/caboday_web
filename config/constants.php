<?php

$URL_ADMIN = 'https://admin.cabodaytrips.com/';

return [
  'BANNERS_CATEGORIAS_TOURS_RES' => $URL_ADMIN.'assets/files/imagenes/categorias_tours/responsivo/',
  'BANNERS_CATEGORIAS_TOURS_NORMAL' => $URL_ADMIN.'assets/files/imagenes/categorias_tours/normal/',
  'BANNERS_CATEGORIAS_TOURS_WIDE' => $URL_ADMIN.'assets/files/imagenes/categorias_tours/wide/',
  'BANNERS_TOURS_RES' => $URL_ADMIN.'assets/files/imagenes/tours/responsivo/',
  'BANNERS_TOURS_NORMAL' => $URL_ADMIN.'assets/files/imagenes/tours/normal/',
  'BANNERS_TOURS_WIDE' => $URL_ADMIN.'assets/files/imagenes/tours/wide/',
  'THUMBNAIL_TOURS' => $URL_ADMIN.'assets/files/imagenes/tours/thumbnail/',
  'THUMBNAIL_CATEGORIA_TRANSPORTACION' => $URL_ADMIN.'assets/files/imagenes/categorias_transportaciones/thumbnail/',
  'THUMBNAIL_TRANSPORTACION' => $URL_ADMIN.'assets/files/imagenes/transportaciones/thumbnail/',
  'FACEBOOK_LINK' => 'https://www.facebook.com/caboday.trips',
  'YOUTUBE_LINK' => 'https://www.youtube.com/channel/UCKkOwxO6ePD_KgKNn5do6VA?view_as=subscriber',
  'INSTAGRAM_LINK' => 'https://www.instagram.com/sohappygirl123/',
];
