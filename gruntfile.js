//Gruntfile
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    // Paths variables
    paths: {
	     js:        'public/assets/js/',
      css:        'public/assets/css/',
      stylus:     'resources/assets/stylus/',
      javascript: 'resources/assets/javascript/'
    },

    // Concat
    concat:{
      basic:{
        src:[
          '<%= paths.javascript %>jquery-1-11-3.min.js',
          '<%= paths.javascript %>jquery-ui-1.10.2.custom.min.js',
          '<%= paths.javascript %>formatCurrency.js',
          '<%= paths.javascript %>jquery.maskedinput.js',
          '<%= paths.javascript %>parallax.js',
          '<%= paths.javascript %>../venobox/venobox.min.js',
          '<%= paths.javascript %>../chosen_v1.8.7/chosen.jquery.min.js',
          '<%= paths.javascript %>../jquery-modal/jquery.modal.min.js',
          '<%= paths.javascript %>../jquery-time/jquery.timepicker.min.js',

          '<%= paths.javascript %>main.js',
          '<%= paths.javascript %>cart.js',
          '<%= paths.javascript %>payment.js'
        ],
        dest:'<%= paths.js %>main.js'
      }
    },
    // Uglify
    uglify: {
     	options: { sourceMap: true },
      build: {
        files: { '<%= paths.js %>main.js': '<%= paths.js %>main.js' }
      }
    },

    // Stylus
    stylus: {
      compile: {
        options: { compress: true },
        files: [{ '<%= paths.css %>main.css':'<%= paths.stylus %>main.styl' }]
      }
    },

    // Watch
    watch: {
    	options: { livereload: true },
      concat: {
      	files: [
          '<%= paths.javascript %>main.js'
        ],
        tasks: 'concat'
      },
      stylus: {
        files: ['<%= paths.stylus %>main.styl', '<%= paths.stylus %>home.styl', '<%= paths.stylus %>estructura.styl', '<%= paths.stylus %>responsive.styl', '<%= paths.stylus %>transport-detail.styl'],
        tasks: ['stylus'],
        options: { livereload: true }
      },
    },
    browserSync: {
      dev: {
          bsFiles: {
              src : [
                  'public/assets/css/*.css',
                  'public/assets/js/*.js',
                  'resources/views/*.php',
                  'resources/views/site/*.php',
                  'resources/views/layouts/*.php'
              ]
          },
          options: {
              watchTask: true,
              port: 3000,
              proxy: "http://localhost:8888/caboday_web/public"

          }
      }
    }
  });

  // Plugin loading
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  // Default task(s).
  // grunt.registerTask('default', ['browserSync', 'watch']);
  grunt.registerTask('default', ['watch']);
};
