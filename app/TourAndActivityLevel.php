<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourAndActivityLevel extends Model
{
    protected $table = 'nivel_actividad';
}
