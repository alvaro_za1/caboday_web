<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationLunch extends Model
{
  protected $table = 'box_lunch_por_reservacion';

  public function lunch() {
    return $this->belongsTo('App\Lunch', 'id_box_lunch');
  }
}
