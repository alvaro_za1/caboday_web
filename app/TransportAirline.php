<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportAirline extends Model
{
  protected $table = 'aerolineas';
}
