<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationTransport extends Model
{
  protected $table = 'reservaciones_transportacion';

  public function transport(){
    return $this->belongsTo('App\Transport', 'id_transportacion');
  }

  // FALSE FOR TRANSPORTACION
  public function type(){
    return false;
  }

  public function aerolinea_llegada(){
    return $this->belongsTo('App\TransportAirline', 'id_airline_arrival');
  }

  public function hotel_llegada(){
    return $this->belongsTo('App\TransportHotel', 'id_hotel_arrival');
  }

  public function aerolinea_salida(){
    return $this->belongsTo('App\TransportAirline', 'id_airline_departure');
  }

  public function hotel_salida(){
    return $this->belongsTo('App\TransportHotel', 'id_hotel_departure');
  }

  public function extras() {
    return $this->hasMany('App\ReservationExtra', 'id_reservacion_transportacion', 'id')
      ->where('eliminado', 0);
  }
}
