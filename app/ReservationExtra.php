<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationExtra extends Model
{
  protected $table = 'extras_por_transportacion';

  public function extra(){
    return $this->belongsTo('App\TransportExtra', 'id_extra');
  }
}
