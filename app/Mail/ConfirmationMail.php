<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\TourAndActivity;
use App\Transport;

use App\Reservation;
use App\ReservationExtra;
use App\ReservationFin;
use App\ReservationLunch;
use App\ReservationTour;
use App\ReservationTransport;
use App\ReservationVehicle;

class ConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $reservation;

    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      $reservation = $this->reservation;
      $reservations_tours = ReservationTour::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();
      $reservations_transport = ReservationTransport::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();

      $Pathtofile = public_path('files').'/confirmation-'.$reservation->confirmation_number.'.pdf';

      return $this->view('emails.confirmation')
        ->from('no-reply@cabodaytrips.com')
        ->subject('Cabo Day Trips confirmation')
        ->with('reservation', $reservation)
        ->with('reservations_tours', $reservations_tours)
        ->with('reservations_transport', $reservations_transport)
        ->attach($Pathtofile);
    }
}
