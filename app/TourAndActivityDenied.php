<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourAndActivityDenied extends Model
{
    protected $table = 'tipo_restricciones';
}
