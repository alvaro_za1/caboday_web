<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourAndActivityRecommended extends Model
{
    protected $table = 'tipo_recomendaciones';
}
