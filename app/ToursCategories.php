<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToursCategories extends Model
{
  protected $table = 'categorias_tours';

  // Get the image associated with the category tour.
  public function image()
  {
    return $this->hasOne('App\TourCategoryImage', 'id_categoria_tour', 'id')->where('eliminado', 0);
  }

  public function tours(){
    return $this->hasMany('App\TourAndActivity', 'id_categoria_tour')->where('eliminado', 0);
  }
}
