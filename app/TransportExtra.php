<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportExtra extends Model
{
  protected $table = 'extras';
}
