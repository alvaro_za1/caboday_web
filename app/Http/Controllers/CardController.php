<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Redirect;
use Cart;
use Validator;

use App\TourAndActivity;
use App\Transport;

use App\Reservation;
use App\ReservationExtra;
use App\ReservationFin;
use App\ReservationLunch;
use App\ReservationTour;
use App\ReservationTransport;
use App\ReservationVehicle;
use Openpay;
use Exception;
use OpenpayApiError;
use OpenpayApiAuthError;
use OpenpayApiRequestError;
use OpenpayApiConnectionError;
use OpenpayApiTransactionError;


// require_once '../vendor/autoload.php';

class CardController extends Controller
{
  
  public function postPaymentWithCard(Request $request) {
    if (Cart::instance('payment')->count() == 0)
    {
      \Session::put('error','Cart empty');
      return Redirect::route('checkout');
    }

    $v = Validator::make($request->all(), [
      'name' => 'required|string',
      'email' => 'required|email',
      'phone' => 'required|string',
      'payment-method' => 'required',
      'card_name' => 'required',
      'card_number' => 'required',
      'expires_mm' => 'required',
      'expires_yy' => 'required',
      'cvv' => 'required',
    ]);

    if ($v->fails()){
      return redirect('checkout');
    }

    if ($request->get('payment-method') == 1) {
      return redirect('checkout');
    }

    // Discount and extras
    $discount = 0;
    $extras = 0;
    foreach (Cart::instance('payment')->content() as $item) {
      $discount += $item->options->discount;
      foreach ($item->options->extras as $extra)
        $extras += $extra->precio * $item->options->passengers;
    }
    $subtotal = Cart::instance('payment')->subtotal() + $extras;
    $total = $subtotal - $discount;

    $openpay = Openpay::getInstance(env('OPENPAY_MERCHANT_ID'), env('OPENPAY_PRIVATE_KEY'));

    $customer = array(
      'name' => $request->name,
      'last_name' => $request->name,
      'phone_number' => $request->phone,
      'email' => $request->email
    );

    $chargeRequest = array(
      'method' => 'card',
      'source_id' => $request->card_id,
      'amount' => $total,
      'currency' => 'USD',
      'description' => 'Cabo day trips',
      'order_id' => uniqid(),
      'device_session_id' => $request->device_session_id,
      'customer' => $customer,
      'redirect_url' => asset('/checkout/charge'),
      'use_3d_secure' => 'true'
    );

    try {

      $charge = $openpay->charges->create($chargeRequest);
      // var_dump($charge);
      // echo $charge->card->card_number;
      // echo $charge->status;
      // die();
      // Start the payment, insert item in database
      $reservation = new Reservation;
      $reservation->nombre = $request->name;
      $reservation->email = $request->email;
      $reservation->phone = $request->phone;
      $reservation->importe = $subtotal;
      $reservation->descuento = $discount;
      $reservation->subtotal = $subtotal;
      $reservation->iva = 0;
      $reservation->total = $total;
      $reservation->estatus = 'pre-card';
      $reservation->eliminado = 0;
      $reservation->order = $charge->id;
      $reservation->confirmation_number = '';
      $reservation->save();
      \Session::put('ReservationId', $reservation->id);
      
      return Redirect::to($charge->payment_method->url);

    } catch (OpenpayApiTransactionError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiRequestError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiConnectionError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiAuthError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiError $e) {
      \Session::put('error',$e->getMessage());
    } catch (Exception $e) {
      \Session::put('error',$e->getMessage());
    }

    return Redirect::route('checkout-error');
  }

  public function getCharge(Request $request) {

    $openpay = Openpay::getInstance(env('OPENPAY_MERCHANT_ID'), env('OPENPAY_PRIVATE_KEY'));

    try {
      $charge = $openpay->charges->get($request->id);
      if($charge){
        $status = $charge->status;
        if($status != "completed"){
          //FAILURE
          \Session::put('error', $charge->error_message);
          return Redirect::route('checkout-error');
        }else{
          // SUCCESS
          return Redirect::route('checkout-success');
        }
      }
    } catch (OpenpayApiTransactionError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiRequestError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiConnectionError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiAuthError $e) {
      \Session::put('error',$e->getMessage());
    } catch (OpenpayApiError $e) {
      \Session::put('error',$e->getMessage());
    } catch (Exception $e) {
      \Session::put('error',$e->getMessage());
    }

    return Redirect::route('checkout-error');
  }

  public function success() {

    try {
      $reservation = self::saveReservation();
      MailController::confirmationMail($reservation);

      return Redirect::route('checkout-confirmation');

    } catch (Exception $e) {
      \Session::put('error',$e->getMessage());
    }
    
    return Redirect::route('checkout-error');
  }

  // Save reservation
  private function saveReservation()
  {
    $reservation = Reservation::find(\Session::get('ReservationId'));

    if($reservation->estatus == 'Card'){
      return $reservation;
    }

    foreach (Cart::instance('payment')->content() as $item)
    {
      // echo $item->options;
      // die();
      switch ($item->options->type) {
        case 1: // Tour And Activity
          $tour = TourAndActivity::find($item->id);
          $confirmationNumber = $tour->iniciales . date('ymdHis');

          $reservationTour = new ReservationTour;
          $reservationTour->id_reservacion = $reservation->id;
          $reservationTour->id_tour = $item->id;
          $reservationTour->adults = $item->options->request['adults'];
          $reservationTour->kids = $item->options->request['children'];
          $reservationTour->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['date'])));
          $reservationTour->id_pick_tour = self::getForm($item->options->request['pickup']);
          $reservationTour->condo_name = self::getForm($item->options->request['hotel_name']);
          $reservationTour->id_hotel = self::getFormHotel($item->options->request['hotel']);
          $reservationTour->importe = $item->price;
          $reservationTour->descuento = $item->options->discount;
          $reservationTour->subtotal = $item->price - $item->options->discount;
          $reservationTour->iva = 0;
          $reservationTour->total = $item->price - $item->options->discount;
          $reservationTour->eliminado = 0;
          $reservationTour->order = '';
          $reservationTour->confirmation_number = $confirmationNumber;
          $reservationTour->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Lunches
          if(isset($item->options->request['lunches']))
            foreach ($item->options->request['lunches'] as $lunch)
            {
              $reservationLunch = new ReservationLunch;
              $reservationLunch->id_reservacion_tour = $reservationTour->id;
              $reservationLunch->id_box_lunch = $lunch;
              $reservationLunch->eliminado = 0;
              $reservationLunch->save();
            }

          // Fins
          if(isset($item->options->request['fins']))
            foreach ($item->options->request['fins'] as $fin)
            {
              $reservationFin = new ReservationFin;
              $reservationFin->id_reservacion_tour = $reservationTour->id;
              $reservationFin->talla = $fin;
              $reservationFin->eliminado = 0;
              $reservationFin->save();
            }

          break;

        case 2: // Tour And Activity - Vehicle
          $tour = TourAndActivity::find($item->id);
          $confirmationNumber = $tour->iniciales . date('ymdHis');

          $reservationTour = new ReservationTour;
          $reservationTour->id_reservacion = $reservation->id;
          $reservationTour->id_tour = $item->id;
          $reservationTour->adults = 0;
          $reservationTour->kids = 0;
          $reservationTour->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['date'])));
          $reservationTour->id_pick_tour = $item->options->request['pickup'];
          $reservationTour->condo_name = self::getForm($item->options->request['hotel_name']);
          $reservationTour->id_hotel = self::getFormHotel($item->options->request['hotel']);
          $reservationTour->importe = $item->price;
          $reservationTour->descuento = $item->options->discount;
          $reservationTour->subtotal = $item->price - $item->options->discount;
          $reservationTour->iva = 0;
          $reservationTour->total = $item->price - $item->options->discount;
          $reservationTour->eliminado = 0;
          $reservationTour->order = '';
          $reservationTour->confirmation_number = $confirmationNumber;
          $reservationTour->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Vehicle
          $reservationVehicle = new ReservationVehicle;
          $reservationVehicle->id_reservacion_tour = $reservationTour->id;
          $reservationVehicle->id_vehiculos = $item->options->request['vehicle'];
          $reservationVehicle->precio = $item->price;
          $reservationVehicle->eliminado = 0;
          $reservationVehicle->save();

          break;

        case 3: // Shared Shuttle
          $transport = Transport::find($item->id);
          $confirmationNumber = $transport->nombre_marca[0].$transport['nombre_'.Session::get('language')][0].date('ymdHis');

          $reservationTransport = new ReservationTransport;
          $reservationTransport->id_reservacion = $reservation->id;
          $reservationTransport->id_transportacion = $item->id;
          $reservationTransport->id_airline_arrival = $item->options->request['arrival_airline'];
          $reservationTransport->flight_number_arrival = $item->options->request['arrival_flight'];
          $reservationTransport->arrival_hour = $item->options->request['arrival_hour'];
          $reservationTransport->adults_arriving = $item->options->request['arrival_adults'];
          $reservationTransport->kids_arriving = $item->options->request['arrival_kids'];
          $reservationTransport->id_hotel_arrival = $item->options->request['arrival_hotel'];
          $reservationTransport->condo_name_arrival = self::getForm($item->options->request['arrival_hotel_name']);
          $reservationTransport->id_airline_departure = $item->options->request['departure_airline'];
          $reservationTransport->flight_number_departure = $item->options->request['departure_flight'];
          $reservationTransport->departure_hour = $item->options->request['departure_hour'];
          $reservationTransport->adults_departing = $item->options->request['departure_adults'];
          $reservationTransport->kids_departing = $item->options->request['departure_kids'];
          $reservationTransport->id_hotel_departure = $item->options->request['departure_hotel'];
          $reservationTransport->condo_name_departure = self::getForm($item->options->request['departure_hotel_name']);
          $reservationTransport->importe = $item->price;
          $reservationTransport->descuento = $item->options->discount;
          $reservationTransport->subtotal = $item->price - $item->options->discount;
          $reservationTransport->iva = 0;
          $reservationTransport->total = $item->price - $item->options->discount;
          $reservationTransport->eliminado = 0;
          $reservationTransport->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['arrival_date'])));
          $reservationTransport->departure_date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['departure_date'])));
          $reservationTransport->order = '';
          $reservationTransport->confirmation_number = $confirmationNumber;
          $reservationTransport->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          break;

        case 4: // Transport
          $transport = Transport::find($item->id);
          $confirmationNumber = $transport->nombre_marca[0].$transport['nombre_'.Session::get('language')][0].date('ymdHis');

          $reservationTransport = new ReservationTransport;
          $reservationTransport->id_reservacion = $reservation->id;
          $reservationTransport->id_transportacion = $item->id;
          $reservationTransport->id_airline_arrival = $item->options->request['arrival_airline'];
          $reservationTransport->flight_number_arrival = $item->options->request['arrival_flight'];
          $reservationTransport->arrival_hour = $item->options->request['arrival_hour'];
          $reservationTransport->adults_arriving = $item->options->request['arrival_adults'];
          $reservationTransport->kids_arriving = $item->options->request['arrival_kids'];
          $reservationTransport->id_hotel_arrival = $item->options->request['arrival_hotel'];
          $reservationTransport->condo_name_arrival = self::getForm($item->options->request['arrival_hotel_name']);
          $reservationTransport->id_airline_departure = $item->options->request['departure_airline'];
          $reservationTransport->flight_number_departure = $item->options->request['departure_flight'];
          $reservationTransport->departure_hour = $item->options->request['departure_hour'];
          $reservationTransport->adults_departing = $item->options->request['departure_adults'];
          $reservationTransport->kids_departing = $item->options->request['departure_kids'];
          $reservationTransport->id_hotel_departure = $item->options->request['departure_hotel'];
          $reservationTransport->condo_name_departure = self::getForm($item->options->request['departure_hotel_name']);
          $reservationTransport->importe = $item->price;
          $reservationTransport->descuento = $item->options->discount;
          $reservationTransport->subtotal = $item->price - $item->options->discount;
          $reservationTransport->iva = 0;
          $reservationTransport->total = $item->price - $item->options->discount;
          $reservationTransport->eliminado = 0;
          $reservationTransport->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['arrival_date'])));
          $reservationTransport->departure_date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['departure_date'])));
          $reservationTransport->order = '';
          $reservationTransport->confirmation_number = $confirmationNumber;
          $reservationTransport->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Extras
          foreach ($item->options->extras as $extra)
          {
            $reservationExtra = new ReservationExtra;
            $reservationExtra->id_extra = $extra->id;
            $reservationExtra->id_reservacion_transportacion = $reservationTransport->id;
            $reservationExtra->eliminado = 0;
            $reservationExtra->save();
          }

          break;

        default:
          // code...
          break;
      }
    }

    Cart::instance('payment')->destroy();
    $reservation->estatus = 'Card';
    $reservation->save();
    return $reservation;
  }

  private function getForm($el)
  {
    return $el == null ? NULL : $el;
  }

  private function getFormHotel($el)
  {
    return $el == -1 ? NULL : $el;
  }
}
