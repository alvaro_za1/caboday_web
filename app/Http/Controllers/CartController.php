<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TourAndActivity;
use App\TourAndActivityVehicle;
use App\TourAndActivityPickUp;
use App\Transport;
use App\TransportExtra;
use Cart;
use Session;
use Exception;
use Validator;

class CartController extends Controller
{
  public function __construct(){
    $this->middleware('language');
  }

  // Add item to cart
  public function add(Request $request)
  {
    try
    {
      // Tour and Activity
      if ($request->type == 1)
      {
        $v = Validator::make($request->all(), [
          'id' => 'required|exists:tours',
          'date' => 'required',
          'vehicle' => 'nullable|exists:vehiculos,id',
          'pickup' => 'nullable|exists:recogidas,id'
        ]);

        if ($v->fails())
          throw new Exception('Validator errors');

        // Tour And Activity
        $item = TourAndActivity::find($request->id);

        // Vehicle
        if ($request->vehicle != '')
        {
          $date = $request->date;
          $vehicleId = $request->vehicle;
          $hotelId = $request->hotel;
          $pickupId = $request->pickup;

          $vehicle = TourAndActivityVehicle::find($vehicleId);

          Cart::instance('payment')->add($item->id, $item['nombre_'.Session::get('language')], 1, $vehicle->precio, [
            'type' => 2,
            'description' => $date . ' / '
              . $vehicle['nombre_'.Session::get('language')],
            // 'vehicle' => $vehicle,
            // 'hotel' => $hotelId,
            // 'lunches' => '',
            // 'fins' => '',
            'discount' => $vehicle->precio * $item->descuento,
            'request' => $request->all(),
            'extras' => []
          ]);
        }
        else
        {
          $date = $request->date;
          $adults = $request->adults;
          $children = $request->children;
          $hotelId = $request->hotel;
          $pickupId = $request->pickup;
          // Price of adults and children
          $price = ($item->precio_adulto * $adults) + ($item->precio_menor * $children);

          Cart::instance('payment')->add($item->id, $item['nombre_'.Session::get('language')], 1, $price, [
            'type' => 1,
            'description' => $date . ' / '
              . $adults . (Session::get('language') == 'en' ? ' adults, ' : ' adultos, ')
              . $children . (Session::get('language') == 'en' ? ' kids' : ' niños'),
            // 'vehicle' => '',
            // 'adults' => $adults,
            // 'kids' => $children,
            // 'hotel' => $hotelId,
            // 'lunches' => $request->lunches,
            // 'fins' => $request->fins,
            'discount' => $price * $item->descuento,
            'request' => $request->all(),
            'extras' => []
          ]);
        }
      }
      // Shared Shuttle
      elseif($request->type == 2)
      {
        $v = Validator::make($request->all(), [
          'arrival_airline' => 'required|exists:aerolineas,id',
          'arrival_flight' => 'required',
          'arrival_date' => 'required',
          'arrival_hour' => 'required',
          'arrival_adults' => 'required',
          'arrival_kids' => 'min:1',
          'arrival_hotel' => 'exists:hoteles,id',
          'departure_airline' => 'required|exists:aerolineas,id',
          'departure_flight' => 'required',
          'departure_date' => 'required',
          'departure_hour' => 'required',
          'departure_adults' => 'required',
          'departure_kids' => 'min:1',
          'departure_hotel' => 'exists:hoteles,id',
        ]);

        if ($v->fails())
          throw new Exception('Validator errors');

        // Shared Shuttle
        $item = Transport::where('id_categoria_transportacion', 2)->where('eliminado', 0)->first();
        if ($item != null)
        {
          $price = $item->precio * ($request->arrival_adults + $request->arrival_kids);
          Cart::instance('payment')->add($item->id, $item['nombre_'.Session::get('language')], 1, $price, [
            'type' => 3,
            'description' => $request->arrival_date . ' / '
              . $request->arrival_hour . ' / ',
            // 'vehicle' => '',
            // 'lunches' => '',
            // 'fins' => '',
            'discount' => 0,
            'request' => $request->all(),
            'extras' => []
          ]);
        }
      }
      // Transport
      else {
        $v = Validator::make($request->all(), [
          'id' => 'required|exists:transportaciones',
          'arrival_airline' => 'required|exists:aerolineas,id',
          'arrival_flight' => 'required',
          'arrival_date' => 'required',
          'arrival_hour' => 'required',
          'arrival_adults' => 'required',
          'arrival_kids' => 'min:1',
          'arrival_hotel' => 'exists:hoteles,id',
          'departure_airline' => 'required|exists:aerolineas,id',
          'departure_flight' => 'required',
          'departure_date' => 'required',
          'departure_hour' => 'required',
          'departure_adults' => 'required',
          'departure_kids' => 'min:1',
          'departure_hotel' => 'exists:hoteles,id'
        ]);

        if ($v->fails())
          throw new Exception('Validator errors');

        $item = Transport::find($request->id);
        $extras = ($request->has('extras')
          ? TransportExtra::whereIn('id', $request->extras)->get()
          : []);

        $extras_total = 0;
        $passengers = $request->arrival_adults + $request->arrival_kids;
        foreach ($extras as $extra) {
          $extras_total += $extra->precio * $passengers;
        }

        Cart::instance('payment')->add($item->id, (Session::get('language') == 'en' ? 'Private transportation' : 'Transporte privado' ), 1, $item->precio + $extras_total, [
          'type' => 4,
          'description' => $item['nombre_'.Session::get('language')] . ' / '
            . $passengers . (Session::get('language') == 'en' ? ' passengers' : ' pasajeros' ),
          // 'vehicle' => '',
          // 'lunches' => '',
          // 'fins' => '',
          'passengers' => $passengers,
          'discount' => 0,
          'request' => $request->all(),
          'extras' => $extras
        ]);
      }

      return response()->json(['success' => true, 'count' => Cart::instance('payment')->count()]);
    }
    catch(Exception $ex)
    {
      return response()->json(['success' => false, 'error' => $ex->getMessage()]);
    }
  }

  // Remove item to cart
  public function remove(Request $request)
  {
    try {
      Cart::instance('payment')->remove($request->rowId);

      // Discount
      $discount = 0;
      foreach (Cart::instance('payment')->content() as $item) {
        $discount += $item->options->discount;
      }

      return response()->json([
        'success' => true,
        'count' => Cart::instance('payment')->count(),
        'subtotal' => Cart::instance('payment')->subtotal(),
        'discount' => $discount,
        'total' => Cart::instance('payment')->total() - Cart::instance('payment')->tax() - $discount
      ]);
    } catch (\Exception $e) {
      return response()->json(['success' => false]);
    }
  }
}
