<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;
use Validator;
use Exception;
use Cart;

use App\ToursCategories;
use App\TourAndActivity;
use App\TourAndActivityVehicle;
use App\TourLunch;
use App\Transport;
use App\TransportAirline;
use App\TransportHotel;
use App\TransportExtra;
use App\TransportCategory;

use App\ReservationTour;
use App\ReservationExtra;
use App\ReservationFin;
use App\ReservationTransport;
use App\ReservationLunch;
use App\Reservation;
use App\ReservationVehicle;

use App\Encabezado;
use App\Mail\Contact;
use Mail;

class SitioController extends Controller
{
  public function __construct(){
    $this->middleware('language');
  }

  // Home
  public function index()
  {
    $data = [];
    $data['CategoriesTour'] = ToursCategories::where('eliminado', 0)->get();
    $data['CategoriesTransport'] = TransportCategory::where('eliminado', 0)->get();
    $data['encabezado'] = Encabezado::findOrFail(1);
    return view('sitio.home', $data);
  }

  // About us
  public function aboutus(Request $request)
  {
    $data = [];
    $data['encabezado'] = Encabezado::findOrFail(2);
    return view('sitio.aboutus', $data);
  }

  // Tours & Activities
  public function tours(Request $request)
  {
    $data = [];
    $data['categorias'] = ToursCategories::where('eliminado', 0)->get();
    $data['data'] = TourAndActivity::where('eliminado', 0)->get();
    $data['category'] = $request->category;
    $data['encabezado'] = Encabezado::findOrFail(3);
    return view('actividades.listado', $data);
  }

  // Tours & Activities Detail
  public function detail_tour($nombre, $id)
  {
    $data = [];
    $data['data'] = TourAndActivity::find($id);

    // Get only 2 in random
    $transportCount = Transport::where('id_categoria_transportacion', 1)->where('eliminado', 0)->count();
    $data['hoteles'] = TransportHotel::where('eliminado', 0)->get();
    $data['transports'] = Transport::where('id_categoria_transportacion', 1)
      ->where('eliminado', 0)->skip(rand(0, $transportCount-2))->take(2)->get();
    $data['lunches'] = TourLunch::where('eliminado', 0)->where('id_tour', $id)->get();
    $data['vehicles'] = TourAndActivityVehicle::where('eliminado', 0)->where('id_tour', $id)->get();
    return view('actividades.detalle', $data);
  }

  // Tranport
  public function transportation(Request $request)
  {
    $data = [];
    $data['data'] = Transport::where('eliminado', 0)->where('id_categoria_transportacion', 1)->get();
    $data['shared'] = Transport::where('eliminado', 0)->where('id_categoria_transportacion', 2)->first();
    $data['airlines'] = TransportAirline::where('eliminado', 0)->pluck('nombre','id');
    $data['hoteles'] = TransportHotel::where('eliminado', 0)->get();
    $data['category'] = $request->category;
    $data['encabezado'] = Encabezado::findOrFail(4);
    return view('transportacion.listado', $data);
  }

  // Transport detail
  public function detail_transportation($nombre, $id)
  {
    $data = [];
    $data['data'] = Transport::find($id);
    $data['airlines'] = TransportAirline::where('eliminado', 0)->pluck('nombre','id');
    $data['hoteles'] = TransportHotel::where('eliminado', 0)->get();
    $data['extras'] = TransportExtra::where('eliminado', 0)->get();
    return view('transportacion.detalle', $data);
  }

  // Contact us
  public function contactus(Request $request)
  {
    $data = [];
    $data['encabezado'] = Encabezado::findOrFail(5);
    return view('sitio.contactus', $data);
  }

  // Contact us POST
  public function contactusPOST(Request $request)
  {
    
    $v = Validator::make($request->all(), [
      'name' => 'required|string',
      'email' => 'required|email',
      'phone' => 'required|string',
      'subject' => 'required|string',
      'message' => 'required|string',
    ]);

    if ($v->fails()){
      Session::flash('contactMsg', 'Please fill all the fields.');
      return redirect('contact-us');
    }

    $correos = ['alvaro_za1@hotmail.com', 'blanca.garcia@cabodaytrips.com', 'heinzsaradeth@hotmail.com', 'garcia863@hotmail.com'];
    foreach ($correos as $correo) {
      Mail::to($correo)->send(new Contact($request->all()));
    }

    Session::flash('contactMsg', 'Thanks for contacting us, we will contact you as soon as possible.');
    return redirect('contact-us');
  }

  public function addArrival(Request $request) {
    try {
      // Tour and Activity
      if($request->type == 1) {
        $v = Validator::make($request->all(), [
          'id' => 'required|exists:tours',
          'date' => 'required',
          'vehicle' => 'nullable|exists:vehiculos,id',
          'pickup' => 'nullable|exists:recogidas,id'
        ]);

        if ($v->fails())
          throw new Exception('Validator errors');

        // Tour And Activity
        $item = TourAndActivity::find($request->id);

        // Vehicle
        if ($request->vehicle != '')
        {
          $date = $request->date;
          $vehicleId = $request->vehicle;
          $hotelId = $request->hotel;
          $pickupId = $request->pickup;

          $vehicle = TourAndActivityVehicle::find($vehicleId);
          Cart::instance('payarrival')->destroy();
          Cart::instance('payarrival')->add($item->id, $item['nombre_'.Session::get('language')], 1, $vehicle->precio, [
            'type' => 2,
            'description' => $date . ' / '
              . $vehicle['nombre_'.Session::get('language')],
            // 'vehicle' => $vehicle,
            // 'hotel' => $hotelId,
            // 'lunches' => '',
            // 'fins' => '',
            'discount' => $vehicle->precio * $item->descuento,
            'request' => $request->all(),
            'extras' => []
          ]);
        }
        else
        {
          $date = $request->date;
          $adults = $request->adults;
          $children = $request->children;
          $hotelId = $request->hotel;
          $pickupId = $request->pickup;
          // Price of adults and children
          $price = ($item->precio_adulto * $adults) + ($item->precio_menor * $children);

          Cart::instance('payarrival')->destroy();
          Cart::instance('payarrival')->add($item->id, $item['nombre_'.Session::get('language')], 1, $price, [
            'type' => 1,
            'description' => $date . ' / '
              . $adults . (Session::get('language') == 'en' ? ' adults, ' : ' adultos, ')
              . $children . (Session::get('language') == 'en' ? ' kids' : ' niños'),
            // 'vehicle' => '',
            // 'adults' => $adults,
            // 'kids' => $children,
            // 'hotel' => $hotelId,
            // 'lunches' => $request->lunches,
            // 'fins' => $request->fins,
            'discount' => $price * $item->descuento,
            'request' => $request->all(),
            'extras' => []
          ]);
        }
      } // Shared Shuttle
      else if ($request->type == 2) {
        $v = Validator::make($request->all(), [
          'arrival_airline' => 'required|exists:aerolineas,id',
          'arrival_flight' => 'required',
          'arrival_date' => 'required',
          'arrival_hour' => 'required',
          'arrival_adults' => 'required',
          'arrival_kids' => 'min:1',
          'arrival_hotel' => 'exists:hoteles,id',
          'departure_airline' => 'required|exists:aerolineas,id',
          'departure_flight' => 'required',
          'departure_date' => 'required',
          'departure_hour' => 'required',
          'departure_adults' => 'required',
          'departure_kids' => 'min:1',
          'departure_hotel' => 'exists:hoteles,id',
        ]);

        if ($v->fails())
          throw new Exception('Validator errors');

        // Shared Shuttle
        $item = Transport::where('id_categoria_transportacion', 2)->where('eliminado', 0)->first();
        if ($item != null)
        {
          $price = $item->precio * ($request->arrival_adults + $request->arrival_kids);
          Cart::instance('payarrival')->destroy();
          Cart::instance('payarrival')->add($item->id, $item['nombre_'.Session::get('language')], 1, $price, [
            'type' => 3,
            'description' => $request->arrival_date . ' / '
              . $request->arrival_hour . ' / ',
            // 'vehicle' => '',
            // 'lunches' => '',
            // 'fins' => '',
            'discount' => 0,
            'request' => $request->all(),
            'extras' => []
          ]);
        }
      } else {
        $v = Validator::make($request->all(), [
          'id' => 'required|exists:transportaciones',
          'arrival_airline' => 'required|exists:aerolineas,id',
          'arrival_flight' => 'required',
          'arrival_date' => 'required',
          'arrival_hour' => 'required',
          'arrival_adults' => 'required',
          'arrival_kids' => 'min:1',
          'arrival_hotel' => 'exists:hoteles,id',
          'departure_airline' => 'required|exists:aerolineas,id',
          'departure_flight' => 'required',
          'departure_date' => 'required',
          'departure_hour' => 'required',
          'departure_adults' => 'required',
          'departure_kids' => 'min:1',
          'departure_hotel' => 'exists:hoteles,id'
        ]);
    
        if ($v->fails())
          throw new Exception('Validator errors');
    
        $item = Transport::findOrFail($request->id);
        $extras = ($request->has('extras')
          ? TransportExtra::whereIn('id', $request->extras)->get()
          : []);
    
        $extras_total = 0;
        $passengers = $request->arrival_adults + $request->arrival_kids;
        foreach ($extras as $extra) {
          $extras_total += $extra->precio * $passengers;
        }
    
        Cart::instance('payarrival')->destroy();
        Cart::instance('payarrival')->add($item->id, (Session::get('language') == 'en' ? 'Private transportation' : 'Transporte privado' ), 1, $item->precio + $extras_total, [
          'type' => 4,
          'description' => $item['nombre_'.Session::get('language')] . ' / '
            . $passengers . (Session::get('language') == 'en' ? ' passengers' : ' pasajeros' ),
          // 'vehicle' => '',
          // 'lunches' => '',
          // 'fins' => '',
          'passengers' => $passengers,
          'discount' => 0,
          'request' => $request->all(),
          'extras' => $extras
        ]);
      }

      return response()->json(['success' => true, 'count' => Cart::instance('payarrival')->count()]);
    } catch(Exception $e) {
      return response()->json(['success' => false, 'error' => $ex->getMessage()]);
    }
  }  

  public function checkoutArrival(Request $request) {
    if (Cart::instance('payarrival')->count() < 1) {
      return redirect('safe-transportation');
    }
    return view('checkout.checkoutArrival');
  }

  public function postCheckoutArrival(Request $request) {
    if (Cart::instance('payarrival')->count() == 0)
    {
      \Session::put('error','Cart empty');
      return redirect('checkout-arrival');
    }

    $v = Validator::make($request->all(), [
      'name' => 'required|string',
      'email' => 'required|email',
      'phone' => 'required|string',
    ]);

    if ($v->fails()){
      return redirect('checkout-arrival');
    }

    // Discount and extras
    $discount = 0;
    $extras = 0;
    foreach (Cart::instance('payarrival')->content() as $item) {
      $discount += $item->options->discount;
      foreach ($item->options->extras as $extra)
        $extras += $extra->precio * $item->options->passengers;
    }
    $subtotal = floatval(str_replace(",","",Cart::instance('payarrival')->subtotal()));
    $total = $subtotal - $discount;

    // Start the payment, insert item in database
    $reservation = new Reservation;
    $reservation->nombre = $request->name;
    $reservation->email = $request->email;
    $reservation->phone = $request->phone;
    $reservation->importe = $subtotal;
    $reservation->descuento = $discount;
    $reservation->subtotal = $subtotal;
    $reservation->iva = 0;
    $reservation->total = $total;
    $reservation->estatus = 'Pay on arrival';
    $reservation->eliminado = 0;
    $reservation->order = '';
    $reservation->confirmation_number = '';
    $reservation->save();
    \Session::put('ReservationId', $reservation->id);

    self::saveReservation($reservation);
    Cart::instance('payarrival')->destroy();
    MailController::confirmationMail($reservation);
    return redirect('checkout/purchase-confirmation');
  }

  // CHECKOUT
  public function checkout(Request $request)
  {
    return view('checkout.checkout');
  }

  public function checkoutPurchaseConfirmation(Request $request)
  {
    $data = [];
    $reservation = Reservation::find(\Session::get('ReservationId'));
    $data['purchased'] = [];
    $data['reservations_tours'] = ReservationTour::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();
    $data['reservations_transport'] = ReservationTransport::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();
    foreach ($data['reservations_tours'] as $tour) {
      array_push($data['purchased'], $tour->tour->nombre_en);
    }

    foreach ($data['reservations_transport'] as $trans) {
      array_push($data['purchased'],$trans->transport->nombre_marca.' '.$trans->transport->nombre_en);
    }

    $data['data'] = $reservation;
    return view('checkout.purchase-confirmation', $data);
  }

  public function checkoutPurchaseError(Request $request)
  {
    return view('checkout.purchase-error');
  }

  public function privacy_policies(){
    return view('sitio.privacy_policies');
  }

  public function terms_conditions(){
    return view('sitio.terms_conditions');
  }

  private function getForm($el)
  {
    return $el == null ? NULL : $el;
  }

  private function getFormHotel($el)
  {
    return $el == -1 ? NULL : $el;
  }

  private function saveReservation($reservation) {
    foreach (Cart::instance('payarrival')->content() as $item)
    {
      // echo $item->options;
      // die();
      switch ($item->options->type) {
        case 1: // Tour And Activity
          $tour = TourAndActivity::find($item->id);
          $confirmationNumber = $tour->iniciales . date('ymdHis');

          $reservationTour = new ReservationTour;
          $reservationTour->id_reservacion = $reservation->id;
          $reservationTour->id_tour = $item->id;
          $reservationTour->adults = $item->options->request['adults'];
          $reservationTour->kids = $item->options->request['children'];
          $reservationTour->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['date'])));
          $reservationTour->id_pick_tour = self::getForm($item->options->request['pickup']);
          $reservationTour->condo_name = self::getForm($item->options->request['hotel_name']);
          $reservationTour->id_hotel = self::getFormHotel($item->options->request['hotel']);
          $reservationTour->importe = $item->price;
          $reservationTour->descuento = $item->options->discount;
          $reservationTour->subtotal = $item->price - $item->options->discount;
          $reservationTour->iva = 0;
          $reservationTour->total = $item->price - $item->options->discount;
          $reservationTour->eliminado = 0;
          $reservationTour->order = '';
          $reservationTour->confirmation_number = $confirmationNumber;
          $reservationTour->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Lunches
          if(isset($item->options->request['lunches']))
            foreach ($item->options->request['lunches'] as $lunch)
            {
              $reservationLunch = new ReservationLunch;
              $reservationLunch->id_reservacion_tour = $reservationTour->id;
              $reservationLunch->id_box_lunch = $lunch;
              $reservationLunch->eliminado = 0;
              $reservationLunch->save();
            }

          // Fins
          if(isset($item->options->request['fins']))
            foreach ($item->options->request['fins'] as $fin)
            {
              $reservationFin = new ReservationFin;
              $reservationFin->id_reservacion_tour = $reservationTour->id;
              $reservationFin->talla = $fin;
              $reservationFin->eliminado = 0;
              $reservationFin->save();
            }

          break;

        case 2: // Tour And Activity - Vehicle
          $tour = TourAndActivity::find($item->id);
          $confirmationNumber = $tour->iniciales . date('ymdHis');

          $reservationTour = new ReservationTour;
          $reservationTour->id_reservacion = $reservation->id;
          $reservationTour->id_tour = $item->id;
          $reservationTour->adults = 0;
          $reservationTour->kids = 0;
          $reservationTour->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['date'])));
          $reservationTour->id_pick_tour = $item->options->request['pickup'];
          $reservationTour->condo_name = self::getForm($item->options->request['hotel_name']);
          $reservationTour->id_hotel = self::getFormHotel($item->options->request['hotel']);
          $reservationTour->importe = $item->price;
          $reservationTour->descuento = $item->options->discount;
          $reservationTour->subtotal = $item->price - $item->options->discount;
          $reservationTour->iva = 0;
          $reservationTour->total = $item->price - $item->options->discount;
          $reservationTour->eliminado = 0;
          $reservationTour->order = '';
          $reservationTour->confirmation_number = $confirmationNumber;
          $reservationTour->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Vehicle
          $reservationVehicle = new ReservationVehicle;
          $reservationVehicle->id_reservacion_tour = $reservationTour->id;
          $reservationVehicle->id_vehiculos = $item->options->request['vehicle'];
          $reservationVehicle->precio = $item->price;
          $reservationVehicle->eliminado = 0;
          $reservationVehicle->save();

          break;

        case 3: // Shared Shuttle
          $transport = Transport::find($item->id);
          $confirmationNumber = $transport->nombre_marca[0].$transport['nombre_'.Session::get('language')][0].date('ymdHis');

          $reservationTransport = new ReservationTransport;
          $reservationTransport->id_reservacion = $reservation->id;
          $reservationTransport->id_transportacion = $item->id;
          $reservationTransport->id_airline_arrival = $item->options->request['arrival_airline'];
          $reservationTransport->flight_number_arrival = $item->options->request['arrival_flight'];
          $reservationTransport->arrival_hour = $item->options->request['arrival_hour'];
          $reservationTransport->adults_arriving = $item->options->request['arrival_adults'];
          $reservationTransport->kids_arriving = $item->options->request['arrival_kids'];
          $reservationTransport->id_hotel_arrival = $item->options->request['arrival_hotel'];
          $reservationTransport->condo_name_arrival = self::getForm($item->options->request['arrival_hotel_name']);
          $reservationTransport->id_airline_departure = $item->options->request['departure_airline'];
          $reservationTransport->flight_number_departure = $item->options->request['departure_flight'];
          $reservationTransport->departure_hour = $item->options->request['departure_hour'];
          $reservationTransport->adults_departing = $item->options->request['departure_adults'];
          $reservationTransport->kids_departing = $item->options->request['departure_kids'];
          $reservationTransport->id_hotel_departure = $item->options->request['departure_hotel'];
          $reservationTransport->condo_name_departure = self::getForm($item->options->request['departure_hotel_name']);
          $reservationTransport->importe = $item->price;
          $reservationTransport->descuento = $item->options->discount;
          $reservationTransport->subtotal = $item->price - $item->options->discount;
          $reservationTransport->iva = 0;
          $reservationTransport->total = $item->price - $item->options->discount;
          $reservationTransport->eliminado = 0;
          $reservationTransport->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['arrival_date'])));
          $reservationTransport->departure_date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['departure_date'])));
          $reservationTransport->order = '';
          $reservationTransport->confirmation_number = $confirmationNumber;
          $reservationTransport->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          break;

        case 4: // Transport
          $transport = Transport::find($item->id);
          $confirmationNumber = $transport->nombre_marca[0].$transport['nombre_'.Session::get('language')][0].date('ymdHis');

          $reservationTransport = new ReservationTransport;
          $reservationTransport->id_reservacion = $reservation->id;
          $reservationTransport->id_transportacion = $item->id;
          $reservationTransport->id_airline_arrival = $item->options->request['arrival_airline'];
          $reservationTransport->flight_number_arrival = $item->options->request['arrival_flight'];
          $reservationTransport->arrival_hour = $item->options->request['arrival_hour'];
          $reservationTransport->adults_arriving = $item->options->request['arrival_adults'];
          $reservationTransport->kids_arriving = $item->options->request['arrival_kids'];
          $reservationTransport->id_hotel_arrival = $item->options->request['arrival_hotel'];
          $reservationTransport->condo_name_arrival = self::getForm($item->options->request['arrival_hotel_name']);
          $reservationTransport->id_airline_departure = $item->options->request['departure_airline'];
          $reservationTransport->flight_number_departure = $item->options->request['departure_flight'];
          $reservationTransport->departure_hour = $item->options->request['departure_hour'];
          $reservationTransport->adults_departing = $item->options->request['departure_adults'];
          $reservationTransport->kids_departing = $item->options->request['departure_kids'];
          $reservationTransport->id_hotel_departure = $item->options->request['departure_hotel'];
          $reservationTransport->condo_name_departure = self::getForm($item->options->request['departure_hotel_name']);
          $reservationTransport->importe = $item->price;
          $reservationTransport->descuento = $item->options->discount;
          $reservationTransport->subtotal = $item->price - $item->options->discount;
          $reservationTransport->iva = 0;
          $reservationTransport->total = $item->price - $item->options->discount;
          $reservationTransport->eliminado = 0;
          $reservationTransport->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['arrival_date'])));
          $reservationTransport->departure_date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['departure_date'])));
          $reservationTransport->order = '';
          $reservationTransport->confirmation_number = $confirmationNumber;
          $reservationTransport->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Extras
          foreach ($item->options->extras as $extra)
          {
            $reservationExtra = new ReservationExtra;
            $reservationExtra->id_extra = $extra->id;
            $reservationExtra->id_reservacion_transportacion = $reservationTransport->id;
            $reservationExtra->eliminado = 0;
            $reservationExtra->save();
          }

          break;

        default:
          // code...
          break;
      }
    }
  }
}
