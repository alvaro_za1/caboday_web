<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Session;
use Redirect;
use Cart;
use Validator;

use App\TourAndActivity;
use App\Transport;

use App\Reservation;
use App\ReservationExtra;
use App\ReservationFin;
use App\ReservationLunch;
use App\ReservationTour;
use App\ReservationTransport;
use App\ReservationVehicle;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

// https://laravelcode.com/post/how-to-integrate-paypal-payment-gateway-in-laravel-54
class PayPalController extends Controller
{
  private $_api_context;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      /** setup PayPal api context **/
      $paypal_conf = \Config::get('paypal');
      $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
      $this->_api_context->setConfig($paypal_conf['settings']);
  }

  /**
  * Store a details of payment with paypal.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function postPaymentWithpaypal(Request $request)
  {
    if (Cart::instance('payment')->count() == 0)
    {
      \Session::put('error','Cart empty');
      return Redirect::route('checkout');
    }

    $v = Validator::make($request->all(), [
      'name' => 'required|string',
      'email' => 'required|email',
      'phone' => 'required|string',
      'payment-method' => 'required',
    ]);

    if ($v->fails()){
      return redirect('checkout');
    }

    if ($request->get('payment-method') == 2) {
      return redirect('checkout');
    }

    // Discount and extras
    $discount = 0;
    $extras = 0;
    foreach (Cart::instance('payment')->content() as $item) {
      $discount += $item->options->discount;
      foreach ($item->options->extras as $extra)
        $extras += $extra->precio * $item->options->passengers;
    }
    $subtotal = floatval(str_replace(",","",Cart::instance('payment')->subtotal()));
    $total = $subtotal - $discount;

    // Start the payment, insert item in database
    $reservation = new Reservation;
    $reservation->nombre = $request->name;
    $reservation->email = $request->email;
    $reservation->phone = $request->phone;
    $reservation->importe = $subtotal;
    $reservation->descuento = $discount;
    $reservation->subtotal = $subtotal;
    $reservation->iva = 0;
    $reservation->total = $total;
    $reservation->estatus = 'Pre PayPal';
    $reservation->eliminado = 0;
    $reservation->order = '';
    $reservation->confirmation_number = '';
    $reservation->save();
    \Session::put('ReservationId', $reservation->id);

    // PayPal
    $payer = new Payer();
    $payer->setPaymentMethod('paypal');

 	  $item_1 = new Item();

    $item_1->setName('Cabo Day Trips') /** item name **/
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setPrice($total); /** unit price **/

    $item_list = new ItemList();
    $item_list->setItems(array($item_1));

    $amount = new Amount();
    $amount->setCurrency('USD')
        ->setTotal($total);

    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setItemList($item_list)
        ->setDescription('Cabo Day Trips');

    $redirect_urls = new RedirectUrls();
    $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
        ->setCancelUrl(URL::route('checkout'));

    $payment = new Payment();
    $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
    try {
      $payment->create($this->_api_context);
    } catch (\PayPal\Exception\PPConnectionException $ex) {
      if (\Config::get('app.debug')) {
        \Session::put('error','Connection timeout');
        return Redirect::route('checkout');
        /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
        /** $err_data = json_decode($ex->getData(), true); **/
        /** exit; **/
      } else {
        \Session::put('error','Some error occur, sorry for inconvenient');
        return Redirect::route('checkout');
        /** die('Some error occur, sorry for inconvenient'); **/
      }
    }

    foreach($payment->getLinks() as $link) {
      if($link->getRel() == 'approval_url') {
          $redirect_url = $link->getHref();
            break;
      }
    }

    /** add payment ID to session **/
    Session::put('paypal_payment_id', $payment->getId());

    if(isset($redirect_url)) {
      /** redirect to paypal **/
      return Redirect::away($redirect_url);
    }

    \Session::put('error','Unknown error occurred');
 	  return Redirect::route('checkout');
  }

  public function getPaymentStatus(Request $request)
  {

     /** Get the payment ID before session clear **/
    $payment_id = Session::get('paypal_payment_id');
    /** clear the session payment ID **/
    Session::forget('paypal_payment_id');
    if (empty($request->get('PayerID')) || empty($request->get('token'))) {
        \Session::put('error','Payment failed');
        return Redirect::route('checkout-error');
    }
    $payment = Payment::get($payment_id, $this->_api_context);
    /** PaymentExecution object includes information necessary **/
    /** to execute a PayPal account payment. **/
    /** The payer_id is added to the request query parameters **/
    /** when the user is redirected from paypal back to your site **/
    $execution = new PaymentExecution();
    $execution->setPayerId($request->get('PayerID'));
    /**Execute the payment **/
    $result = $payment->execute($execution, $this->_api_context);
    /** dd($result);exit; /** DEBUG RESULT, remove it later **/
    if ($result->getState() == 'approved') {
    /** it's all right **/
    /** Here Write your database logic like that insert record or value in database if you want **/
        $reservation = self::saveReservation();
        MailController::confirmationMail($reservation);
        return Redirect::route('checkout-confirmation');
    }
    
    return Redirect::route('checkout-error');
  }

  // Save reservation
  private function saveReservation()
  {
    $reservation = Reservation::find(\Session::get('ReservationId'));

    if($reservation->estatus == 'PayPal'){
      return $reservation;
    }

    foreach (Cart::instance('payment')->content() as $item)
    {
      // echo $item->options;
      // die();
      switch ($item->options->type) {
        case 1: // Tour And Activity
          $tour = TourAndActivity::find($item->id);
          $confirmationNumber = $tour->iniciales . date('ymdHis');

          $reservationTour = new ReservationTour;
          $reservationTour->id_reservacion = $reservation->id;
          $reservationTour->id_tour = $item->id;
          $reservationTour->adults = $item->options->request['adults'];
          $reservationTour->kids = $item->options->request['children'];
          $reservationTour->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['date'])));
          $reservationTour->id_pick_tour = self::getForm($item->options->request['pickup']);
          $reservationTour->condo_name = self::getForm($item->options->request['hotel_name']);
          $reservationTour->id_hotel = self::getFormHotel($item->options->request['hotel']);
          $reservationTour->importe = $item->price;
          $reservationTour->descuento = $item->options->discount;
          $reservationTour->subtotal = $item->price - $item->options->discount;
          $reservationTour->iva = 0;
          $reservationTour->total = $item->price - $item->options->discount;
          $reservationTour->eliminado = 0;
          $reservationTour->order = '';
          $reservationTour->confirmation_number = $confirmationNumber;
          $reservationTour->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Lunches
          if(isset($item->options->request['lunches']))
            foreach ($item->options->request['lunches'] as $lunch)
            {
              $reservationLunch = new ReservationLunch;
              $reservationLunch->id_reservacion_tour = $reservationTour->id;
              $reservationLunch->id_box_lunch = $lunch;
              $reservationLunch->eliminado = 0;
              $reservationLunch->save();
            }

          // Fins
          if(isset($item->options->request['fins']))
            foreach ($item->options->request['fins'] as $fin)
            {
              $reservationFin = new ReservationFin;
              $reservationFin->id_reservacion_tour = $reservationTour->id;
              $reservationFin->talla = $fin;
              $reservationFin->eliminado = 0;
              $reservationFin->save();
            }

          break;

        case 2: // Tour And Activity - Vehicle
          $tour = TourAndActivity::find($item->id);
          $confirmationNumber = $tour->iniciales . date('ymdHis');

          $reservationTour = new ReservationTour;
          $reservationTour->id_reservacion = $reservation->id;
          $reservationTour->id_tour = $item->id;
          $reservationTour->adults = 0;
          $reservationTour->kids = 0;
          $reservationTour->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['date'])));
          $reservationTour->id_pick_tour = $item->options->request['pickup'];
          $reservationTour->condo_name = self::getForm($item->options->request['hotel_name']);
          $reservationTour->id_hotel = self::getFormHotel($item->options->request['hotel']);
          $reservationTour->importe = $item->price;
          $reservationTour->descuento = $item->options->discount;
          $reservationTour->subtotal = $item->price - $item->options->discount;
          $reservationTour->iva = 0;
          $reservationTour->total = $item->price - $item->options->discount;
          $reservationTour->eliminado = 0;
          $reservationTour->order = '';
          $reservationTour->confirmation_number = $confirmationNumber;
          $reservationTour->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Vehicle
          $reservationVehicle = new ReservationVehicle;
          $reservationVehicle->id_reservacion_tour = $reservationTour->id;
          $reservationVehicle->id_vehiculos = $item->options->request['vehicle'];
          $reservationVehicle->precio = $item->price;
          $reservationVehicle->eliminado = 0;
          $reservationVehicle->save();

          break;

        case 3: // Shared Shuttle
          $transport = Transport::find($item->id);
          $confirmationNumber = $transport->nombre_marca[0].$transport['nombre_'.Session::get('language')][0].date('ymdHis');

          $reservationTransport = new ReservationTransport;
          $reservationTransport->id_reservacion = $reservation->id;
          $reservationTransport->id_transportacion = $item->id;
          $reservationTransport->id_airline_arrival = $item->options->request['arrival_airline'];
          $reservationTransport->flight_number_arrival = $item->options->request['arrival_flight'];
          $reservationTransport->arrival_hour = $item->options->request['arrival_hour'];
          $reservationTransport->adults_arriving = $item->options->request['arrival_adults'];
          $reservationTransport->kids_arriving = $item->options->request['arrival_kids'];
          $reservationTransport->id_hotel_arrival = $item->options->request['arrival_hotel'];
          $reservationTransport->condo_name_arrival = self::getForm($item->options->request['arrival_hotel_name']);
          $reservationTransport->id_airline_departure = $item->options->request['departure_airline'];
          $reservationTransport->flight_number_departure = $item->options->request['departure_flight'];
          $reservationTransport->departure_hour = $item->options->request['departure_hour'];
          $reservationTransport->adults_departing = $item->options->request['departure_adults'];
          $reservationTransport->kids_departing = $item->options->request['departure_kids'];
          $reservationTransport->id_hotel_departure = $item->options->request['departure_hotel'];
          $reservationTransport->condo_name_departure = self::getForm($item->options->request['departure_hotel_name']);
          $reservationTransport->importe = $item->price;
          $reservationTransport->descuento = $item->options->discount;
          $reservationTransport->subtotal = $item->price - $item->options->discount;
          $reservationTransport->iva = 0;
          $reservationTransport->total = $item->price - $item->options->discount;
          $reservationTransport->eliminado = 0;
          $reservationTransport->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['arrival_date'])));
          $reservationTransport->departure_date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['departure_date'])));
          $reservationTransport->order = '';
          $reservationTransport->confirmation_number = $confirmationNumber;
          $reservationTransport->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          break;

        case 4: // Transport
          $transport = Transport::find($item->id);
          $confirmationNumber = $transport->nombre_marca[0].$transport['nombre_'.Session::get('language')][0].date('ymdHis');

          $reservationTransport = new ReservationTransport;
          $reservationTransport->id_reservacion = $reservation->id;
          $reservationTransport->id_transportacion = $item->id;
          $reservationTransport->id_airline_arrival = $item->options->request['arrival_airline'];
          $reservationTransport->flight_number_arrival = $item->options->request['arrival_flight'];
          $reservationTransport->arrival_hour = $item->options->request['arrival_hour'];
          $reservationTransport->adults_arriving = $item->options->request['arrival_adults'];
          $reservationTransport->kids_arriving = $item->options->request['arrival_kids'];
          $reservationTransport->id_hotel_arrival = $item->options->request['arrival_hotel'];
          $reservationTransport->condo_name_arrival = self::getForm($item->options->request['arrival_hotel_name']);
          $reservationTransport->id_airline_departure = $item->options->request['departure_airline'];
          $reservationTransport->flight_number_departure = $item->options->request['departure_flight'];
          $reservationTransport->departure_hour = $item->options->request['departure_hour'];
          $reservationTransport->adults_departing = $item->options->request['departure_adults'];
          $reservationTransport->kids_departing = $item->options->request['departure_kids'];
          $reservationTransport->id_hotel_departure = $item->options->request['departure_hotel'];
          $reservationTransport->condo_name_departure = self::getForm($item->options->request['departure_hotel_name']);
          $reservationTransport->importe = $item->price;
          $reservationTransport->descuento = $item->options->discount;
          $reservationTransport->subtotal = $item->price - $item->options->discount;
          $reservationTransport->iva = 0;
          $reservationTransport->total = $item->price - $item->options->discount;
          $reservationTransport->eliminado = 0;
          $reservationTransport->date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['arrival_date'])));
          $reservationTransport->departure_date = date('Y-m-d', strtotime(str_replace(',','',$item->options->request['departure_date'])));
          $reservationTransport->order = '';
          $reservationTransport->confirmation_number = $confirmationNumber;
          $reservationTransport->save();

          // Reservation
          $reservation->confirmation_number = $confirmationNumber;
          $reservation->save();

          // Extras
          foreach ($item->options->extras as $extra)
          {
            $reservationExtra = new ReservationExtra;
            $reservationExtra->id_extra = $extra->id;
            $reservationExtra->id_reservacion_transportacion = $reservationTransport->id;
            $reservationExtra->eliminado = 0;
            $reservationExtra->save();
          }

          break;

        default:
          // code...
          break;
      }
    }

    Cart::instance('payment')->destroy();
    $reservation->estatus = 'PayPal';
    $reservation->save();
    return $reservation;
  }

  private function getForm($el)
  {
    return $el == null ? NULL : $el;
  }

  private function getFormHotel($el)
  {
    return $el == -1 ? NULL : $el;
  }
}
