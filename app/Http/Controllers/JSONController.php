<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TourAndActivityPickUp;

class JSONController extends Controller
{
  public function __construct(){
    $this->middleware('language');
  }

  // Tour and Activity Get Pick Ups
  public function tourDetailPickUps(Request $request)
  {
    $pickups = TourAndActivityPickUp::where('eliminado', 0)
      ->where('id_tour', $request->tour)
      ->where('id_area', $request->area)
      ->get();

    return response()->json(['success' => true, 'data' => $pickups]);
  }
}
