<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Reservation;
use App\ReservationTour;
use App\ReservationTransport;

class PDFController extends Controller
{
  public static function createConfirmationPDF($reservation){
    $data = [];
    $data['reservation'] = $reservation;
    $data['reservations_tours'] = ReservationTour::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();
    $data['reservations_transport'] = ReservationTransport::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();

    $pdf = PDF::loadView('pdf.confirmation', $data);
    return $pdf->save('files/confirmation-'.$reservation->confirmation_number.'.pdf');
  }

  public function downloadConfirmationPDF(){
    $reservation = Reservation::findOrFail(\Session::get('ReservationId'));
    $data['reservation'] = $reservation;
    $data['reservations_tours'] = ReservationTour::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();
    $data['reservations_transport'] = ReservationTransport::where('id_reservacion', $reservation->id)->where('eliminado', 0)->get();

    $pdf = PDF::loadView('pdf.confirmation', $data);
    return $pdf->download('confirmation-'.$reservation->confirmation_number.'.pdf');
  }
}
