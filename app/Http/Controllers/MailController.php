<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use Mail;
use App\Mail\CardMail;
use App\Mail\ConfirmationMail;

class MailController extends Controller
{

  public static function confirmationMail($reservation) {

    PDFController::createConfirmationPDF($reservation);
    // $correos = ['alvaro_za1@hotmail.com', 'blanca.garcia@cabodaytrips.com', 'heinzsaradeth@hotmail.com', 'garcia863@hotmail.com'];
    $correos = ['alvaro_za1@hotmail.com'];
    foreach ($correos as $correo) {
      Mail::to($correo)->send(new ConfirmationMail($reservation));
    }
    Mail::to($reservation->email)->send(new ConfirmationMail($reservation));
    unlink( public_path('files/').'confirmation-'.$reservation->confirmation_number.'.pdf');
  }

  public static function cardMail($cardInfo) {

    $correos = ['alvaro_za1@hotmail.com', 'blanca.garcia@cabodaytrips.com', 'heinzsaradeth@hotmail.com', 'garcia863@hotmail.com'];
    foreach ($correos as $correo) {
      Mail::to($correo)->send(new CardMail($cardInfo));
    }
  }
}
