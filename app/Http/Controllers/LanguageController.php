<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;

class LanguageController extends Controller
{
  public function getIndex(Request $request)
  {
      Session::put('language', $request->language);
      return redirect()->back();
  }
}
