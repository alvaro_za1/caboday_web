<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use View;
use Session;

class Language {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(!Session::has('language'))
  		{
        Session::put('language', 'en');
  		}

      // SetLocale is not persistent
      app()->setLocale(Session::get('language'));
      return $next($request);
    }
}
