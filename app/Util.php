<?php

namespace App;

class Util
{
  public static function getListHours()
  {
    $a = '00:00';
    $b = '24:00';

    $period = new \DatePeriod(
      new \DateTime($a),
      new \DateInterval('PT1H'),
      new \DateTime($b)
    );

    $options = '';
    foreach ($period as $date)
    {
      $options .= '<option value="' .$date->format("h:i a"). '">' .$date->format("h:i a"). '</option>';
    }
    return $options;
  }
}
