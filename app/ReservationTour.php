<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationTour extends Model
{
  protected $table = 'reservaciones_tours';

  public function tour(){
    return $this->belongsTo('App\TourAndActivity', 'id_tour');
  }

  public function vehiculos() {
    return $this->hasMany('App\ReservationVehicle', 'id_reservacion_tour', 'id')->where('eliminado', 0);
  }

  public function box_lunch() {
    return $this->hasMany('App\ReservationLunch', 'id_reservacion_tour', 'id')->where('eliminado', 0);
  }

  public function aletas() {
    return $this->hasMany('App\ReservationFin', 'id_reservacion_tour', 'id')->where('eliminado', 0);
  }
}
