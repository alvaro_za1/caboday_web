<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourAndActivity extends Model
{
  protected $table = 'tours';

  // Get the level associated with the tour and activity.
  public function level()
  {
    return $this->hasOne('App\TourAndActivityLevel', 'id', 'id_nivel_actividad');
  }

  // Get the recommended associated with the tour and activity.
  public function recommended()
  {
    return $this->hasOne('App\TourAndActivityRecommended', 'id', 'id_recomendado');
  }

  // Get the denied associated with the tour and activity.
  public function denied()
  {
    return $this->hasOne('App\TourAndActivityDenied', 'id', 'id_restriccion');
  }

  // Get the images for the tour and activity
  public function images()
  {
    return $this->hasMany('App\TourAndActivityImage', 'id_tour', 'id')->where('eliminado', 0);
  }

  // Get the restriccion for the tour and activity
  public function restriccions()
  {
    return $this->hasMany('App\TourAndActivityRestriccion', 'id_tour', 'id')->where('eliminado', 0);
  }

  // Get the include for the tour and activity
  public function includes()
  {
    return $this->hasMany('App\TourAndActivityInclude', 'id_tour', 'id')->where('eliminado', 0);
  }

  // Get the bring for the tour and activity
  public function brings()
  {
    return $this->hasMany('App\TourAndActivityBring', 'id_tour', 'id')->where('eliminado', 0);
  }
}
