<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourCategoryImage extends Model
{
    protected $table = 'imagenes_por_categorias_tours';
}
