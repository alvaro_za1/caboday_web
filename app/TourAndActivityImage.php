<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourAndActivityImage extends Model
{
    protected $table = 'imagenes_por_tours';
}
