<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportCategory extends Model
{
  protected $table = 'categorias_transportaciones';
}
