<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationVehicle extends Model
{
  protected $table = 'vehiculos_por_reservacion';

  public function vehicle(){
    return $this->belongsTo('App\Vehicle', 'id_vehiculos');
  }
}
