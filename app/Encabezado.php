<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encabezado extends Model
{
    /**
     * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'secciones_encabezados';
}
