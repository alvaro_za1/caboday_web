<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourLunch extends Model
{
  protected $table = 'box_lunches';
}
