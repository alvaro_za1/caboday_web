<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// LANGUAGES
Route::post('lang','LanguageController@getIndex');

// SITIO
Route::get('/','SitioController@index');
Route::get('about-us','SitioController@aboutus');
Route::get('contact-us','SitioController@contactus');
Route::post('contact-us','SitioController@contactusPOST');
Route::get('privacy-policies','SitioController@privacy_policies');
Route::get('terms-and-conditions','SitioController@terms_conditions');

// TOURS
Route::get('tours-and-activities','SitioController@tours');
Route::get('tours-and-activities/{nombre}/{id}','SitioController@detail_tour');

// TRANSPORT
Route::get('safe-transportation','SitioController@transportation');
Route::get('safe-transportation/{nombre}/{id}','SitioController@detail_transportation');

// CHECKOUT
Route::get('checkout', array('as' => 'checkout','uses' => 'SitioController@checkout'));
Route::get('checkout/charge', array('as' => 'checkout','uses' => 'CardController@getCharge'));
Route::get('checkout/success', array('as' => 'checkout-success','uses' => 'CardController@success'));
Route::get('checkout/purchase-confirmation', array('as' => 'checkout-confirmation','uses' => 'SitioController@checkoutPurchaseConfirmation'));
Route::get('checkout/purchase-error', array('as' => 'checkout-error','uses' => 'SitioController@checkoutPurchasEerror'));
Route::get('checkout/download-pdf', array('as' => 'download-pdf','uses' => 'PDFController@downloadConfirmationPDF'));

// PAY ARRIVAL
Route::post('cart/add-arrival', 'SitioController@addArrival');
Route::get('checkout-arrival', 'SitioController@checkoutArrival');
Route::post('checkout-arrival', 'SitioController@postCheckoutArrival');

// CART
Route::post('cart/add', 'CartController@add');
Route::post('cart/remove', 'CartController@remove');
Route::get('cart/destroy', function(){ Cart::instance('payment')->destroy(); });
Route::get('cart', function(){ echo Cart::instance('payment')->content(); });

// PAYPAL
Route::post('paypal', array('as' => 'paypal','uses' => 'PayPalController@postPaymentWithpaypal'));
Route::get('paypal/status', array('as' => 'status', 'uses' => 'PayPalController@getPaymentStatus'));

// CARD PAYMENT
Route::post('card-payment', array('as' => 'card', 'uses' => 'CardController@postPaymentWithCard'));

// JSON
Route::post('json/tour-detail/pickups', 'JSONController@tourDetailPickUps');

Route::get('correo/prueba/{id}', function($id){
  $reservation = App\Reservation::findOrFail($id);
  App\Http\Controllers\MailController::confirmationMail($reservation);
});
