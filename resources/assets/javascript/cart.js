const ventana = '<div class="loading-container"><div class="pulse"></div></div>';
$(function(){
  activeButtons();

  // Add tour and activity
  $('a[data-accion="cart-add"]').click(function(e){
    e.preventDefault();

    // Validar
    if (validFormTour())
    {
      var accion = $(this).attr('data-button');
      var formTour = $('#form-tour');
      if (accion === 'payarrival') {
        formTour.attr('action', base_url + '/cart/add-arrival');
      }
      formTour.find('[name="date"]').val($('.detail-column-right').find('[name="date"]').val());
      formTour.find('[name="vehicle"]').val($('.detail-column-right').find('[name="vehicles"]').val());
      formTour.find('[name="adults"]').val($('.detail-column-right').find('[name="adults"]').val());
      formTour.find('[name="children"]').val($('.detail-column-right').find('[name="children"]').val());
      formTour.find('[name="hotel"]').val($('.detail-column-right').find('[name="hotel"]').val());
      formTour.find('[name="hotel_name"]').val($('.detail-column-right').find('[name="hotel_name"]').val());
      formTour.find('[name="pickup"]').val($('.detail-column-right').find('[name="pick"]').val());

      $('.container').append(ventana);
      $.ajax({
      	type: "POST",
      	url: formTour.attr('action'),
      	data: formTour.serialize(),
      	success: function( data ) {
          if (data.success) {
            $('.icon-cart').find('b').show().text(data.count);
            if (accion == 'booknow' )
              location.href = base_url + '/checkout';
            else
              location.href = base_url + '/checkout-arrival';
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
  		   console.log('error');
         $('.loading-container').remove();
  		   }
      });
    }
  });

  // Add shared shuttle
  $('#form-shared button').click(function(e){
    // Validar
    if (validFormShared())
    {
      var accion = $(this).attr('data-button');
      var formShared = $('#form-shared');
      if (accion === 'payarrival') {
        formShared.attr('action', base_url + '/cart/add-arrival');
      }
      $('.container').append(ventana);
      $.ajax({
        type: "POST",
        url: formShared.attr('action'),
        data: formShared.serialize(),
        success: function( data ) {
          if (data.success) {
            $('.icon-cart').find('b').show().text(data.count);
            if (accion == 'booknow' )
              location.href = base_url + '/checkout';
            else
              location.href = base_url + '/checkout-arrival';
          } else {
            alert(data.error)
            $('.loading-container').remove();
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
         console.log('error');
         $('.loading-container').remove();
         }
      });
    }
  });

  // Add transport
  $('#form-transport button').click(function(e){
    // Validar
    if (validFormTransport())
    {
      var accion = $(this).attr('data-button');
      var formTransport = $('#form-transport');
      if (accion === 'payarrival') {
        formTransport.attr('action', base_url + '/cart/add-arrival');
      }
      $('.container').append(ventana);
      $.ajax({
      	type: "POST",
      	url: formTransport.attr('action'),
      	data: formTransport.serialize(),
      	success: function( data ) {
          if (data.success) {
            $('.icon-cart').find('b').show().text(data.count);
            if (accion == 'booknow' )
              location.href = base_url + '/checkout';
            else
              location.href = base_url + '/checkout-arrival';
          } else {
            alert(data.error)
            $('.loading-container').remove();
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
  		   console.log('error');
         $('.loading-container').remove();
		   }
      });
    }
  });

  // Remove
  $('.order-summary .icon-delete-item').click(function(e){
    if (confirm("Estás seguro de eliminar el registro"))
    {
      var rowId = $(this).attr('data-rowId');
		  var tr = $(this).parent().parent();
      $('.container').append(ventana);
      $.ajax({
      	type: "POST",
      	url: base_url + '/cart/remove',
      	data: {
          _token: _token,
	    		rowId: rowId
        },
      	success: function( data ) {
          if (data.success) {
            $('.icon-cart').find('b').show().text(data.count);
            $('[data-subtotal] span').text(data.subtotal)
            $('[data-discount] span').text(data.discount)
            $('[data-total] span').text(data.total)

            tr.remove();

            if (data.count == 0)
              $('.checkout .purchase-order').removeClass('correct');
            $('.loading-container').remove();
          }
          else
          {
            $('.loading-container').remove();
            alert('Error');
          }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
  		   console.log('error');
         $('.loading-container').remove();
  		   }
      });
    }
  });
});

//
function activeButtons()
{
  // Form tour and activity
  $('.detail-column-right').find('input,select').change(validFormTour);
  // Form shared shuttle
  $('#form-shared').find('input,select').change(validFormShared);
  // Form transport
  $('#form-transport').find('input,select').change(validFormTransport);
}

// Valid form tour and activity
function validFormTour()
{
  var success = true;
  var formTour = $('.detail-column-right');

  if (formTour.find('[name="date"]').val() == '')
    success = false;

  if (formTour.find('[name="vehicle"]').length > 0 && formTour.find('[name="vehicle"]').val() == '')
    success = false;

  if (formTour.find('[name="adults"]').length > 0 && formTour.find('[name="adults"]').val() == '')
    success = false;

  if (formTour.find('[name="hotel"]').val() == 0)
    success = false;

  if (formTour.find('[name="hotel"]').val() == -1 && formTour.find('[name="hotel_name"]').val() == '')
    success = false;

  if (formTour.find('[name="hotel"]').val() != -1 && formTour.find('[name="pick"]').val() == '')
    success = false;

  // Buttons
  if (success)
  {
    formTour.find('.button-back-gray').removeClass('button-back-gray').addClass('button-back-gray-blue');
    formTour.find('.button-back-white').removeClass('button-back-white').addClass('button-back-white-blue');
  }
  else
  {
    formTour.find('.button-back-gray-blue').removeClass('button-back-gray-blue').addClass('button-back-gray');
    formTour.find('.button-back-white-blue').removeClass('button-back-white-blue').addClass('button-back-white');
  }

  return success;
}

// Valid form shared shuttle
function validFormShared()
{
  var success = true;
  var formShared = $('#form-shared');
  formShared.find('.required').removeClass('border-red');
  $.each(formShared.find('.required'), function() {
    if ($(this).val() == ''
        && $(this).attr('name') != 'arrival_kids'
        && $(this).attr('name') != 'arrival_hotel_name'
        && $(this).attr('name') != 'departure_kids'
        && $(this).attr('name') != 'departure_hotel_name'){
          if (success)
            $('html, body').animate( { scrollTop: $(this).offset().top - 200}, 500 );
          success = false;
          $(this).addClass('border-red');
        }
  });

  // ValidateCapacidad
  if ((parseInt($('[name="arrival_adults"]').val()) + parseInt($('[name="arrival_kids"]').val())) > $('[data-capacidad]').attr('data-capacidad'))
  {
    success = false;
    $('[name="arrival_adults"]').addClass('border-red');
  }

  success = validateHotel(formShared, success);

  // Buttons
  // if (success)
  // {
  //   formShared.find('.button-back-gray').removeClass('button-back-gray').addClass('button-back-gray-blue');
  //   formShared.find('.button-back-white').removeClass('button-back-white').addClass('button-back-white-blue');
  // }
  // else
  // {
  //   formShared.find('.button-back-gray-blue').removeClass('button-back-gray-blue').addClass('button-back-gray');
  //   formShared.find('.button-back-white-blue').removeClass('button-back-white-blue').addClass('button-back-white');
  // }

  return success;
}

// Valid form transport
function validFormTransport()
{
  var success = true;
  var formTransport = $('#form-transport');

  formTransport.find('.required').removeClass('border-red');
  $.each(formTransport.find('.required'), function() {
    if ($(this).val() == ''
        && $(this).attr('name') != 'arrival_kids'
        && $(this).attr('name') != 'arrival_hotel_name'
        && $(this).attr('name') != 'departure_kids'
        && $(this).attr('name') != 'departure_hotel_name') {
          if (success)
            $('html, body').animate( { scrollTop: $(this).offset().top - 200}, 500 );
          success = false;
          $(this).addClass('border-red');
        }
  });

  // ValidateCapacidad
  if ((parseInt($('[name="arrival_adults"]').val()) + parseInt($('[name="arrival_kids"]').val())) > $('[data-capacidad]').attr('data-capacidad'))
  {
    success = false;
    $('[name="arrival_adults"]').addClass('border-red');
  }

  success = validateHotel(formTransport, success);
  // Buttons
  // if (success)
  // {
  //   formTransport.find('.button-back-gray').removeClass('button-back-gray').addClass('button-back-gray-blue');
  //   formTransport.find('.button-back-white').removeClass('button-back-white').addClass('button-back-white-blue');
  // }
  // else
  // {
  //   formTransport.find('.button-back-gray-blue').removeClass('button-back-gray-blue').addClass('button-back-gray');
  //   formTransport.find('.button-back-white-blue').removeClass('button-back-white-blue').addClass('button-back-white');
  // }

  return success;
}

function validateHotel(form, success){
  form.find('.required-hotel').parent().removeClass('border-red');
  const arrival_hotel = form.find('[name="arrival_hotel"]');
  const arrival_condo = form.find('[name="arrival_hotel_name"]');

  const departure_hotel = form.find('[name="departure_hotel"]');
  const departure_condo = form.find('[name="departure_hotel_name"]');

  if (arrival_hotel.val() === '' && arrival_condo.val() === '') {
    if(success)
      $('html, body').animate( { scrollTop: arrival_hotel.offset().top - 200}, 500 );
    success = false;
    arrival_hotel.parent().addClass('border-red');
  }

  if (departure_hotel.val() === '' && departure_condo.val() === '') {
    if(success)
      $('html, body').animate( { scrollTop: arrival_hotel.offset().top - 200}, 500 );
    success = false;
    departure_hotel.parent().addClass('border-red');
  }

  return success;
}
