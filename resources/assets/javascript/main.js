var interval;
var home_carrousel_width = 370;
$(function(){
  // YouTube Venobox
  $('.venobox').venobox({ titleattr: 'data-title' });
  // Chosen Select
  $(".chosen").chosen();
  // Date Picker
  $('.date-picker').datepicker({dateFormat: 'MM, d yy', minDate: 3,
    onSelect: function(date){
      if (!$(this).hasClass('end'))
      {
        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);

        $(".date-picker.end").datepicker( "option", "minDate", endDate );
      }
    }
  });

  // MASK
	$('.mask-phone').mask('(999) 999-9999');
	$('input[name="expires_mm"]').mask('99');
  $('input[name="expires_yy"]').mask('99');
	// $('input[name="card_number"]').mask('9999999999999999');
	$('input[name="card_code"]').mask('999');

  // Time Picker
  $('.time-picker').timepicker({ 'scrollDefault': 'now' });
  // Only number
  $('.number').keypress(isNumberKey);

  // RESPONSIVE MENU
  // RESPONSIVE MENU
	$('.responsive-menu-button').click(function(){
		console.log('responsive show');
		$('.container-menu-responsive').show();
	});

	$('.responsive-close-button').click(function(){
		console.log('responsive show');
		$('.container-menu-responsive').hide();
	});

  // HOME
  // console.log('home');
  if($(window).width() > 850){
    $('.parallax--box').parallaxElement(0.17);
    $('.wave-right').parallaxElement(0.05);
    $('.wave-left').parallaxElement(0.05);
    $('.circle-left').parallaxElement(0.06);
    $('.big-circle-right').parallaxElement(0.04);
  }

  // CHANGE LANGUAGE
	$('.language-button').click(function(e){
    e.preventDefault();
		var lang = $(this).attr('data-lang');
		var form = $(this).closest('form');
		form.find('.input-language').val(lang);
		form.submit();
		console.log(lang);
	});

  if($('.container-table-activities').length > 0){
    var width = 0;
    var items = $('.container-table-activities .table-activities > div').length;
    if($(window).width() > 850){
      width = items * home_carrousel_width;
    }else{
      width = items * 900;
    }
    $('.container-table-activities').css('width', width);

    $(window).resize(function(){
      var width = 0;
      var items = $('.container-table-activities .table-activities > div').length;
      if($(window).width() > 850){
        width = items * home_carrousel_width;
      }else{
        width = items * 900;
      }
      $('.container-table-activities').css('width', width);
    });
  }

  // BANNER TOUR DETAIL
  if($(".slider li").length > 1) {
		$(".slider li").not(":first").css({left:$(window).width()+"px",visibility:"hidden"});
		interval=setInterval(banner,6e3);
		$(window).resize(function(){
			$(".slider li").not(".moved").each(function(){
				if( $(this).offset().left > 0 )
					$(this).css("left",$(window).width()+"px");
				else
					$(this).css("left","-"+$(window).width()+"px")
			});
		});
	}

  // ARROWS TOUR DETAIL
  $('.move-left, .move-right').click(function(){

    var click = $(this);

    if(!$(this).hasClass("wait")) {

    	var a = $(".slider .moved");
      var length = $(".slider li").length;
      var index = a.index();

      var side = $(this).attr('data-side');

      if(side == 'left'){
        var b = a.is("li:last-child") ? $(".slider li:first-child") : a.next();
      }else{
        var b = a.is("li:first-child") ? $(".slider li:last-child") : a.prev();
      }

      // // show_image(b);
    	b.css({left:"0px",visibility:"visible", display:'block'});
    	a.css("left","-"+$(window).width()+"px");
    	a.removeClass("moved")
      b.addClass("moved");
    	$(".container-arrows > div").addClass("wait");
      // // arrow_menu($(this), slider);
      setTimeout(function() {
    		a.css({left:$(window).width()+"px",visibility:"hidden"})
      },
    	1e3);
    	setTimeout(function(){
        $(".container-arrows > div").removeClass("wait");
      },1500);
    	clearInterval(interval);
      interval=setInterval(banner,6e3);
    }
  });

  $('.container-transport-list > div a').click(function(e){
    e.preventDefault();

    var button = $(this);
    var id = button.attr('data-id');

    if(!button.parent().hasClass('select-category')){
      var show = $('#'+id);
      $('.section-transport').hide();
      show.show();
      $('.select-category').removeClass('select-category');
      button.parent().addClass('select-category');
    }

  });

  // Scroll menu thumbnail
  $('.activities-number .icon-arrow-next').click(function() {
    $('.table-activities-overflow').animate( { scrollLeft: '+=350' }, 300);
  });

  $('.activities-number .icon-arrow-left').click(function() {
    $('.table-activities-overflow').animate( { scrollLeft: '-=350' }, 300);
  });

  $('.tours-arrows .icon-arrow-next').click(function() {
    console.log('click');
    $('.menu-tours-list').animate( { scrollLeft: '+=170' }, 300);
  });

  $('.table-activities-overflow').scroll(function() {
    var container_overflow = $('.table-activities-overflow');
    var overflow_offset = container_overflow.offset();
    var width_position = overflow_offset.left + container_overflow.width();
    var thumbnails = container_overflow.find('.table-activities > div');
    thumbnails.each(function(index, thumbnail){
      thumbnail = $(thumbnail);
      var thumbnail_offset_left = thumbnail.offset().left;
      var thumbnail_offset_right = thumbnail.offset().left + thumbnail.width();

      if(thumbnail_offset_left > overflow_offset.left && thumbnail_offset_right < width_position){
        thumbnail.removeClass('outside');
      }else{
        thumbnail.addClass('outside');
      }
    });

    const total_list = $('.table-activities-tours > div');
    const list = $('.table-activities-tours > div:not(.outside)');
    const index = $(list[list.length-1]).index();
    $('#index-category').text(index);
    const width_after = (100 / total_list.length) * index;
    $('.activities-bar .progress').css('width',width_after+'%');
  });

  $('.container-tours-list > div a').click(function(e){
    e.preventDefault();

    var button = $(this);
    var id = button.attr('data-id');

    if(!button.parent().hasClass('select-category')){
      var show = $('#'+id);
      $('.container-tours-item').hide();
      show.show();
      $('.select-category').removeClass('select-category');
      button.parent().addClass('select-category');
    }

  });

  // TOUR AND ACTIVITY DETAIL
  // Change price by adults and children
  $('.detail-column-right .detail-form .container-pax select').change(function(){
    $('label[for="'+$(this).attr('name')+'"] span').text(
      $(this).val() * $(this).attr('data-price')
    );

    // Calcular total
    tourDetailTotal();
  });
  // Change total by vehicle
  $('.detail-column-right .detail-form select[name="vehicles"]').change(function(){
    var subtotal = $(this).find('option:selected').attr('data-price');
    var descuento = $('.detail-column-right .detail-form label[for="descuento"]').attr('data-descuento');

    $('.detail-column-right .detail-form label[for="subtotal"] span').text(subtotal);
    $('.detail-column-right .detail-form label[for="total"] span').text(subtotal * (descuento > 0 ? descuento : 1));
  });
  // My hotel isn't there AND Get pick hours by tour and hotel
  $('.detail-column-right .detail-form select[name="hotel"]').change(function(){
    if ($(this).val() == -1)
    {
      $('[data-el="hotel-name"]').show();
      $('[data-el="pickup-note"]').show();
      $('[data-el="pickup-note"]').prev().hide();
    }
    else
    {
      $('[data-el="hotel-name"]').hide();
      $('[data-el="pickup-note"]').hide();
      $('[data-el="pickup-note"]').prev().show();
    }

    // AJAX Pick ups
    if ($(this).val() > 0)
    {
      $.post(base_url+"/json/tour-detail/pickups", {
        _token: _token,
        tour: $(this).attr('data-tourid'),
        area: $(this).find('option:selected').attr('data-area'),
      })
      .done(function(data) {
        if (data.success) {
          var pickUpOptions = '';
          $.each(data.data, function(index, value){
            pickUpOptions += '<option value="'+value.id+'">'+value.hora+'</option>'
          });
          $('.detail-column-right .detail-form select[name="pick"] option:not(:first-child)').remove()
          $('.detail-column-right .detail-form select[name="pick"]').append(pickUpOptions);
        }
      })
      .fail(function() {
        alert("error");
      })
    }
  });

  // Show the modal lunchfins
  $('.detail-column-right .detail-form select[name="adults"]').change(emptyLunchFins);
  $('.detail-column-right .detail-form select[name="children"]').change(emptyLunchFins);
  $('.detail-column-right .detail-form select[name="hotel"]').change(modalLunchFins);
  $('.detail-column-right .detail-form div[data-el="lunchesfins"] a').click(modalLunchFins);
  //

  // Transport
  $('.iwantit').click(function() {
    $('html,body').animate({scrollTop: $('input[name="arrival_flight"]').offset().top-300}, 200, function() {
      $('input[name="arrival_flight"]').focus();
    });
  });

  $('.detail-transport-form [data-el]').change(function() {
    $('[name="'+$(this).attr('data-el')+'"]').val($(this).val());

    // Update chosen
    if ($('[name="'+$(this).attr('data-el')+'"]').hasClass('chosen'))
      $('[name="'+$(this).attr('data-el')+'"]').trigger("chosen:updated");
  });
  //

  if ($('#container-shared-transport').hasClass('hide'))
    $('#container-shared-transport').hide();
  else
    $('#container-private-transport').hide();
});

function emptyLunchFins(e)
{
  // eliminar
  $('.modal.lunchfins table tbody').empty();

  // Adults
  for (i = 0; i < $('.detail-column-right .detail-form select[name="adults"]').val(); i++)
  {
    tbody = '<tr>' +
      '<td>Adult '+(i+1)+'</td>' +
      ($('[data-el="lunchesfins"]').attr('data-lunches') == 1
        ? '<td><select name="lunches[]"></select></td>'
        : '') +
      ($('[data-el="lunchesfins"]').attr('data-fins') == 1
        ? '<td><select name="fins[]"></select></td>'
        : '') +
    '</tr>';
    $('.modal.lunchfins table tbody').append(tbody);
    if ($('[data-el="lunchesfins"]').attr('data-lunches') == 1)
      $('select.lunches option').clone().appendTo('.modal.lunchfins table tbody tr:last select[name="lunches[]"]');
    if ($('[data-el="lunchesfins"]').attr('data-fins') == 1)
      $('select.fins option').clone().appendTo('.modal.lunchfins table tbody tr:last select[name="fins[]"]');
  }

  // Children
  for (i = 0; i < $('.detail-column-right .detail-form select[name="children"]').val(); i++)
  {
    tbody = '<tr>' +
      '<td>Children '+(i+1)+'</td>' +
      ($('[data-el="lunchesfins"]').attr('data-lunches') == 1
        ? '<td><select name="lunches[]"></select></td>'
        : '') +
      ($('[data-el="lunchesfins"]').attr('data-fins') == 1
        ? '<td><select name="fins[]"></select></td>'
        : '') +
    '</tr>';

    $('.modal.lunchfins table tbody').append(tbody);
    if ($('[data-el="lunchesfins"]').attr('data-lunches') == 1)
      $('select.lunches option').clone().appendTo('.modal.lunchfins table tbody tr:last select[name="lunches[]"]');
    if ($('[data-el="lunchesfins"]').attr('data-fins') == 1)
      $('select.fins option').clone().appendTo('.modal.lunchfins table tbody tr:last select[name="fins[]"]');
  }

  // Mostrar modal
  modalLunchFins(e);
}

// Show the modal lunchfins
function modalLunchFins(e) {
  e.preventDefault();

  var tbody = '';

  if (($('.detail-column-right .detail-form select[name="adults"]').length > 0
        && $('.detail-column-right .detail-form select[name="children"]').length > 0)
      && ($('.detail-column-right .detail-form select[name="adults"]').val() != ''
        || $('.detail-column-right .detail-form select[name="children"]').val() != '')
      && $('.detail-column-right .detail-form select[name="hotel"]').val() != ''
      && $('[data-el="lunchesfins"]').length > 0)
  {
    // Show link to modify modal
    $('[data-el="lunchesfins"]').show();

    $('.modal.lunchfins').modal({
      closeExisting: false,
      escapeClose: false,
      clickClose: false,
      fadeDuration: 50
    });

    // Has lunches
    if ($('[data-el="lunchesfins"]').attr('data-lunches') == 0)
      $('.modal.lunchfins thead tr td:nth-child(2)').remove();
    if ($('[data-el="lunchesfins"]').attr('data-fins') == 0)
      $('.modal.lunchfins thead tr td:nth-child(3)').remove();
  }
  else
  {
    $('[data-el="lunchesfins"]').hide();
  }

}

// Total Tour And Activity Detail
function tourDetailTotal() {
  var mountAdults = parseFloat($('.detail-column-right .detail-form label[for="adults"] span').text());
  var mountChildren = parseFloat($('.detail-column-right .detail-form label[for="children"] span').text());
  var subtotal = mountAdults + mountChildren;
  var descuento_porcentaje = $('.detail-column-right .detail-form label[for="descuento"]').attr('data-descuento');
  var cantidad_descuento = subtotal * descuento_porcentaje;
  var total = subtotal - cantidad_descuento;
  $('.detail-column-right .detail-form label[for="subtotal"] span').text(subtotal);
  $('.detail-column-right .detail-form label[for="total"] span').text(parseFloat(total).toFixed(2));
}

function banner() {
	var a = $(".slider .moved");
	var b = a.is("li:last-child") ? $(".slider li:first-child") : a.next();

	b.css({left:"0px",visibility:"visible", display:'block'});
	a.css("left","-"+$(window).width()+"px");
	a.removeClass("moved");
	b.addClass("moved");

	$(".container-arrows > div").addClass("wait");

	setTimeout(function() {
		a.css({left:$(window).width()+"px",visibility:"hidden"})},
	1e3);
	setTimeout(function(){
		$(".container-arrows > div").removeClass("wait");
  },
	1500)
}

function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

  return true;
}
