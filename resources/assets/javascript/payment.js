$(function(){

  $('input[name="payment-method"]').change(function(){
    var form = $('#form-payment');
    if ($(this).val() == 2) {
      $('#card-form').show();
      form.attr('action', base_url+'/card-payment');
    } else {
      $('#card-form').hide();
      form.attr('action', base_url+'/paypal');
    }
  });

  $('.checkout .purchase-order').click(function(e) {
    $('.required-card').removeClass('border-red');
    $('.checkout [data-payment]').removeClass('border-red');
    e.preventDefault();

    if ($(this).hasClass('correct'))
    {
      var form = $('#form-payment');

      // Validar
      var validar = true;
      $.each($('.checkout [data-payment]'), function(index, value){
        if ($(value).val() == ''){
          $(value).addClass('border-red');
          validar = false;
        }

      });

      if ($('.checkout [name="email"]').val() != $('.checkout [name="email2"]').val())
        validar = false;

      if ($('input[name="payment-method"]:checked').val() == 2) {
        $.each($('.required-card'), function(index, value){
          if ($(value).val() == '') {
            $(value).addClass('border-red');
            validar = false;
          }

        });
      }

      if (validar)
      {
        // CARD
        if ($('input[name="payment-method"]:checked').val() == 2) {
          OpenPay.setSandboxMode(false);
          OpenPay.setId('mnnnzb7rh237fqrjtbxy');
          OpenPay.setApiKey('pk_e25f91919c934e69b5a4a294c0b4e2ce');
          OpenPay.deviceData.setup("form-payment", "device_session_id");
          OpenPay.token.create({
            card_number: $('input[name="card_number"]').val(),
            holder_name: $('input[name="card_name"]').val(),
            expiration_year:$('input[name="expires_yy"]').val(),
            expiration_month: $('input[name="expires_mm"]').val(),
            cvv2: $('input[name="cvv"]').val(),
          }, SuccessCallback, ErrorCallback);
          function SuccessCallback(response) {
            console.log(response);
            $('.container').append(ventana);
            form.append('<input name="card_id" type="text"value="'+response.data.id+'" />');
            form.submit();
          }
        
          function ErrorCallback(response) {
            alert(response.data.description);
          }
        } else {
          // Paypal
          form.submit();
        }
        
      }
      else
      {
        // alert("Datos obligatorios");
      }
    }

  });
});
