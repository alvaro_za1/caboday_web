<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

		<link href="{{asset('assets/css/jquery-ui.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/css/venobox.css')}}" rel="stylesheet">

		<link type="image/png" rel="icon" href="{{asset('assets/img/favicon.png')}}">
		<link href="{{asset('assets/css/main.css?v=110')}}" rel="stylesheet">
		@yield('metatags')
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127147663-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-127419334-1');
		</script>
		<!--Start of Zendesk Chat Script-->
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		$.src="https://v2.zopim.com/?66JgfySh2oNgbBIevHo37NvBSL61BeGO";z.t=+new Date;$.
		type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		<!--End of Zendesk Chat Script-->
		<script type="text/javascript"> //<![CDATA[ 
		var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
		document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
		//]]>
		</script>
	</head>
	<body>
		@if(Cart::instance('payment')->count() > 0)
			<? $discount = 0; ?>
			<? $totalExtras = 0; ?>
			@foreach (Cart::instance('payment')->content() as $item)
				<? $discount += $item->options->discount ?>
				@foreach ($item->options->extras as $extra)
				<? $totalExtras += $extra->precio; ?>
				@endforeach
			@endforeach
			<div class="fixed-nav-cart">
				<div class="">
					<span class="">{{ Cart::instance('payment')->count() }}</span>
					<span class="">${{ floatval(str_replace(",","",Cart::instance('payment')->total()))+ $totalExtras - Cart::instance('payment')->tax() - $discount }} USD</span>
				</div>
				<div class="">
					<div class="">
						<a href="{{ url('checkout') }}">VER CARRITO</a>
					</div>
				</div>
			</div>
		@endif
		<div class="nav-responsive">
			<div class="">
				<img src="{{asset('assets/img/cabo-day-trips-logo.svg')}}" alt="">
			</div>
			<div class="">

			</div>
			<div>
				<a href="tel:+00526241307972">
					<span class="icon-phone"></span>
				</a>
			</div>
			<div>
				<a href="mailto:concierge@cabodaytrips.com">
					<span class="icon-mail"></span>
				</a>
			</div>
			<div class="">
				<a href="{{ url('checkout') }}">
					<span class="icon-cart">
						<b style="display:{{ Cart::instance('payment')->count() > 0 ? 'inline' : 'none' }}">{{ Cart::instance('payment')->count() }}</b>
					</span>
				</a>
			</div>
			<div class="">
				<span class="icon-burger-icon responsive-menu-button"></span>
			</div>
		</div>
		<div class="container-menu-responsive">
			<div class="responsive-close-button"><span class="icon-close"></span></div>
			<div class="clear"></div>
			<ul>
				<li class=""><a data-title="{{trans('app.menu_home')}}" class="@if(Request::segment(1) == '') select-title @endif" href="{{asset('')}}">{{__('app.menu_home')}}</a></li>
				<li><a data-title="{{trans('app.menu_about_us')}}" class="@if(Request::segment(1) == 'about-us') select-title select-title-100 @endif" href="{{asset('about-us')}}">{{trans('app.menu_about_us')}}</a></li>
				<li><a data-title="{{trans('app.menu_tours')}}" class="@if(Request::segment(1) == 'tours-and-activities') select-title select-title-100 @endif" href="{{asset('tours-and-activities')}}">{{trans('app.menu_tours')}}</a></li>
				<li><a data-title="{{trans('app.menu_transport')}}" class="@if(Request::segment(1) == 'safe-transportation') select-title select-title-100 @endif" href="{{asset('safe-transportation')}}">{{trans('app.menu_transport')}}</a></li>
				<li><a data-title="{{trans('app.menu_contact_us')}}" class="@if(Request::segment(1) == 'contact-us') select-title select-title-100 @endif" href="{{asset('contact-us')}}">{{trans('app.menu_contact_us')}}</a></li>
				<li>
					<a style="font-size:.7em;" href="tel:+00526241307972" >Tel. (0052) 624 1307972</a>
				</li>
				<li>
				<a style="font-size:.7em;" href="mailto:concierge@cabodaytrips.com" >concierge@cabodaytrips.com</a>
				</li>
			</ul>
			<!-- {!! Form::open(array('url' => 'lang', 'method'=> 'post', 'class' => 'lang-form')) !!}
				<input class="input-language" type="hidden" name="language">
				<ul>
					<li><a data-lang="es" data-title="ES" class="language-spanish language-button @if(Session::get('language') == 'es') select-title @endif" href="#">ES</a></li>
					<li><a data-lang="en" data-title="EN" class="language-english language-button @if(Session::get('language') == 'en') select-title @endif" href="#">EN</a></li>
				</ul>
			{!! Form::close() !!} -->
			<div class="footer-responsive-menu">
				<label for="">©2018. Cabo Day Trips. All rights reserved.</label>
			</div>
		</div>
  	<header>
				<nav>
          <div class="container-max-width">
						<div class="table height100">
							<div class="cell vertical-middle">
								<img src="{{asset('assets/img/cabo-day-trips-logo.svg')}}" alt="Cabo Day Trips">
							</div>
							<div class="cell ancho20"></div>
							<div class="cell vertical-middle ancho100">
								<ul class="">
			            <li class=""><a data-title="{{trans('app.menu_home')}}" class="@if(Request::segment(1) == '') select-title @endif" href="{{asset('')}}">{{__('app.menu_home')}}</a></li>
			            <li><a data-title="{{trans('app.menu_about_us')}}" class="@if(Request::segment(1) == 'about-us') select-title select-title-100 @endif" href="{{asset('about-us')}}">{{trans('app.menu_about_us')}}</a></li>
			            <li><a data-title="{{trans('app.menu_tours')}}" class="@if(Request::segment(1) == 'tours-and-activities') select-title select-title-100 @endif" href="{{asset('tours-and-activities')}}">{{trans('app.menu_tours')}}</a></li>
			            <li><a data-title="{{trans('app.menu_transport')}}" class="@if(Request::segment(1) == 'safe-transportation') select-title select-title-100 @endif" href="{{asset('safe-transportation')}}">{{trans('app.menu_transport')}}</a></li>
			            <li>
										<a data-title="{{trans('app.menu_contact_us')}}" class="@if(Request::segment(1) == 'contact-us') select-title select-title-100 @endif" href="{{asset('contact-us')}}">{{trans('app.menu_contact_us')}}</a>
									</li>
									<li>
										<a href="tel:+00526241307972" >Tel. (0052) 624 1307972</a>
										<br><a href="mailto:concierge@cabodaytrips.com" >concierge@cabodaytrips.com</a>
									</li>
									<li>
										<a href="{{ url('checkout') }}">
											<span class="icon-cart">
												<b style="display:{{ Cart::instance('payment')->count() > 0 ? 'inline' : 'none' }}">{{ Cart::instance('payment')->count() }}</b>
											</span>
										</a>
									</li>
									<!-- <li>
										<div class="">
											{!! Form::open(array('url' => 'lang', 'method'=> 'post', 'class' => 'lang-form')) !!}
												<input class="input-language" type="hidden" name="language">
												<a data-lang="es" data-title="ES" class="language-spanish language-button @if(Session::get('language') == 'es') select-title @endif" href="#">ES</a>
												/
												<a data-lang="en" data-title="EN" class="language-english language-button @if(Session::get('language') == 'en') select-title @endif" href="#">EN</a>
											{!! Form::close() !!}
										</div>
									</li> -->
			          </ul>
							</div>
						</div>
          </div>
				</nav>
    </header>
		<section class="container">
			<div class="push-top"></div>
			<section class="container-wrapper">
				<section class="content">
          <!-- CONTENIDO -->
          @yield('content')
				</section>
			</section>
			<div class="push-bottom"></div>
		</section>
		<footer>
			<div class="container-footer">
				<div class="container-max-width height100">
          <div class="table height100 responsive-hide">
          	<div class="cell vertical-middle align-left">
          		©{{date('Y')}}. Cabo Day Trips. {{trans('app.footer_rights')}} <br>
							<div style="margin:3px 0px">Tel. <a href="tel:+00526241307972" >(0052) 624 1307972</a></div>
          	</div>
						<div class="cell vertical-middle align-right table-footer">
							<ul class="table align-center">
								<li class="cell">
									<a href="{{asset('privacy-policies')}}">{{trans('app.link_policies')}}</a>
								</li>
								<li class="cell">
									<a href="{{asset('terms-and-conditions')}}">{{trans('app.link_conditions')}}</a>
								</li>
								<li class="cell">
									<a href="{{asset('contact-us')}}">{{trans('app.link_contact')}}</a>
								</li>
								<li class="cell">
									<img style="width:80px;" src="{{asset('assets/img/home/logo-money-back.png')}}" alt="">
								</li>
								<li class="cell">
									<a target="_blank" href="{{ Config::get('constants.FACEBOOK_LINK') }}">
									<span class="icon-facebook"></span>
									</a>
									<a target="_blank" href="{{ Config::get('constants.INSTAGRAM_LINK') }}">
									<span class="icon-instagram"></span>
									</a>
									<a target="_blank" href="{{ Config::get('constants.YOUTUBE_LINK') }}">
									<span class="icon-youtube"></span>
									</a>
								</li>
							</ul>
						</div>
          </div>
					<div class="responsive-show">
						<div class="responsive-footer">
							<div class="">
								<a target="_blank" href="{{ Config::get('constants.FACEBOOK_LINK') }}">
									<span class="icon-facebook"></span>
								</a>
								<a target="_blank" href="{{ Config::get('constants.INSTAGRAM_LINK') }}">
									<span class="icon-instagram"></span>
								</a>
								<a target="_blank" href="{{ Config::get('constants.YOUTUBE_LINK') }}">
									<span class="icon-youtube"></span>
								</a>
							</div>
							<div>
								<img style="width:100px;" src="{{asset('assets/img/home/logo-money-back.png')}}" alt="">
							</div>
							<div class="">
								<a href="{{asset('privacy-policies')}}">{{trans('app.link_policies')}}</a>
							</div>
							<div class="">
								<a href="{{asset('terms-and-conditions')}}">{{trans('app.link_conditions')}}</a>
							</div>
							<div class="">
								<a href="{{asset('contact-us')}}">{{trans('app.link_contact')}}</a>
							</div>
							<div>
								<a href="tel:+00526241307972" >Tel. (0052) 624 1307972</a>
							</div>
							<div class="">
								©{{date('Y')}}. Cabo Day Trips. {{trans('app.footer_rights')}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
    <script type="text/javascript" src="{{asset('assets/js/main.js?v=9')}}"></script>
		<script type="text/javascript">
			var base_url = '{{ url('') }}';
			var _token = '{{ csrf_token() }}';
		</script>
    @yield('javascript')
	</body>
</html>
