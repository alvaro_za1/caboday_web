<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
      html, body {margin:0;padding:0;}
      *{box-sizing: border-box;}
      .wrap-text{text-overflow:ellipsis;overflow:hidden;white-space:nowrap}
      .checkout{text-align:left;margin:10px 0}
      .checkout h1{font-size:24px;color:#163D4B;margin:0}
      .checkout h3{font-size:18px;color:#163D4B;margin: 10px 0px;}
      .checkout p{opacity:0.7;font-family:"Open Sans";font-size:16px;letter-spacing:0.57px;line-height:22px}
      .checkout .table{display:table;width:100%}
      .checkout .table > *{display:table-cell}
      .checkout .table > :nth-child(1){width:32%}
      .checkout .table > :nth-child(2){padding:0 10px;width:34%}
      .checkout .table > :nth-child(3){width:32%}
      .purchase-confirmation{padding:100px 0}
      .purchase-confirmation .container{max-width:534px;margin:auto;text-align:left}
      .purchase-confirmation h1{font-size:48px;letter-spacing:1.71px;line-height:55px;color:#163D4B;margin:0}
      .purchase-confirmation h3{font-size:18px;line-height:21px;color:#163D4B}
      .purchase-confirmation p{opacity:0.7;font-family:"Open Sans";font-size:16px;letter-spacing:0.57px;line-height:22px;margin:30px 0}
      .purchase-confirmation .wave{margin-bottom:15px}
      .purchase-confirmation .box{border:1px solid #EAEAEA;border-radius:4px;box-shadow:0 2px 20px 0 rgba(0,0,0,0.06);padding:20px 20px 0;margin-bottom:20px}
      .purchase-confirmation .box > div{opacity:0.7;font-family:"Open Sans";font-size:16px;letter-spacing:0.57px;margin:20px 0}
      .purchase-confirmation .notes{opacity:0.7;font-family:"Open Sans";font-size:12px;letter-spacing:0.43px;line-height:17px}
      .purchase-confirmation .actions{display:table;width:100%;margin-top:20px}
      .purchase-confirmation .actions > *{display:table-cell;width:50%}
      .purchase-confirmation .actions > :first-child{padding-right:10px}
      .purchase-confirmation .actions > :last-child{padding-left:10px}
      .purchase-confirmation .actions > :first-child a{background-color:#24AAC5;color:#F7F7F7;line-height:45px}
      .purchase-confirmation .actions > :last-child a{border:1px solid #24AAC5;color:#24AAC5;line-height:45px}
      .purchase-confirmation .actions a{height:46px;text-align:center;letter-spacing:0.5px;border-radius:4px;font-size:14px;display:inline-block;width:100%}
      .title-reservation{display: table;width: 100%;}
      .title-reservation > span{display: table-cell;width: 50%;}
      .title-reservation > span:first-child{text-align: left;}
      .title-reservation > span:last-child{text-align: right;}
      .confirmation{font-size: 12px}
      .flight-information table thead th{ border-bottom: 1px solid #EAEAEA;padding-top: 10px;padding-bottom:5px;}
      .flight-information table{ font-size: 12px;border-collapse: collapse;}
    </style>
  </head>
  <body>
    <div class="checkout purchase-confirmation container-max-width">
    	<div class="container">
        <div class="">
          <!-- <img style="width:60px;" src="{{asset('assets/img/cabo-day-trips-logo.svg')}}" alt=""> -->
        </div>
    		<h1>Thank you for your reservation.</h1>
    		<p>Your reservation is confirmed</p>

    		<!-- <img class="wave" src="{{asset('assets/img/wave.svg')}}" alt="Wave"> -->

        <div class="box">
    			<h3>Details</h3>
    			<div>
            @foreach($reservations_tours as $tour)
              <div class="">
                <div class="title-reservation">
                  <span>{{$tour->tour->nombre_en}}</span>
                  <span>${{ number_format( $tour->importe, 2, '.', ',') }}</span>
                </div>
                <div class="confirmation">
                  <span>Confirmation number: {{$tour->confirmation_number}}</span>
                </div>
                <label style="font-size:12px;">{{ date('l jS \of F Y', strtotime($tour->date)) }}</label>
                <? $pax = $tour->adults + $tour->kids; ?>
                @if($pax > 0)
                  / <label style="font-size:12px;">{{ $pax }} pax</label>
                @endif
                <div style="">
                  @foreach($tour->vehiculos as $vehiculo)
                    <div style="font-size:12px;" class="title-reservation">
                      <? $vehicle = $vehiculo->vehicle; ?>
                      <span>{{ $vehicle->nombre_es }}</span>
                      <span></span>
                    </div>
                  @endforeach
                </div>
                <div style="margin-top:5px;">
                  @foreach($tour->box_lunch as $box)
                    <div style="font-size:12px;" class="title-reservation">
                      <? $lunch = $box->lunch; ?>
                      <span>Box lunch: {{ $lunch->nombre_es }}</span>
                      <span></span>
                    </div>
                  @endforeach
                </div>
                <div style="margin-top:5px;">
                  @foreach($tour->aletas as $aleta)
                    <div style="font-size:12px;" class="title-reservation">
                      <span>Fin size: {{ $aleta->talla }}</span>
                      <span></span>
                    </div>
                  @endforeach
                </div>
              </div>
            @endforeach
            @foreach($reservations_transport as $res_transport)
              <div class="">
                <div class="title-reservation">
                  <span>{{$res_transport->transport->nombre_marca.' '.$res_transport->transport->nombre_en}}</span>
                  <span>${{ number_format( $res_transport->importe, 2, '.', ',') }}</span>
                </div>
                <div class="confirmation">
                  <span>Confirmation number: {{$res_transport->confirmation_number}}</span>
                </div>
                <div class="flight-information">
                  <table>
                    <thead>
                      <tr>
                        <th>Arrival Airline</th>
                        <th>Flight number</th>
                        <th>Arrival date</th>
                        <th>Arrival hour</th>
                        <th>Arrival hotel</th>
                        <th>Adults arriving</th>
                        <th>Kids arriving</th>
                      </tr>
                    </thead>
                    <tr>
                      <td>{{ $res_transport->aerolinea_llegada->nombre }}</td>
                      <td>{{ $res_transport->flight_number_arrival }}</td>
                      <td>{{ $res_transport->date }}</td>
                      <td>{{ $res_transport->arrival_hour }}</td>
                      <td>{{ $res_transport->hotel_llegada->nombre }}</td>
                      <td>{{ $res_transport->adults_arriving }}</td>
                      <td>{{ $res_transport->kids_arriving }}</td>
                    </tr>
                  </table>
                </div>
                <div class="flight-information">
                  <table>
                    <thead>
                      <tr>
                        <th>Departure Airline</th>
                        <th>Flight number</th>
                        <th>Departure date</th>
                        <th>Departure hour</th>
                        <th>Departure hotel</th>
                        <th>Adults departing</th>
                        <th>Kids departing</th>
                      </tr>
                    </thead>
                    <tr>
                      <td>{{ $res_transport->aerolinea_salida->nombre }}</td>
                      <td>{{ $res_transport->flight_number_departure }}</td>
                      <td>{{ $res_transport->departure_date }}</td>
                      <td>{{ $res_transport->departure_hour }}</td>
                      <td>{{ $res_transport->hotel_salida->nombre }}</td>
                      <td>{{ $res_transport->adults_departing }}</td>
                      <td>{{ $res_transport->kids_departing }}</td>
                    </tr>
                  </table>
                </div>
                <div style="margin-top:15px;">
                  @foreach($res_transport->extras as $extra)
                    <div style="font-size:12px;" class="title-reservation">
                      <? $trans_extra = $extra->extra; ?>
                      <span>{{ $trans_extra->nombre_en }}</span>
                      <? $pax = $res_transport->adults_arriving + $res_transport->kids_arriving ?>
                      <? $total = $trans_extra->precio * $pax; ?>
                      <span>${{ number_format( $total, 2, '.', ',') }}</span>
                    </div>
                  @endforeach
                </div>
              </div>
            @endforeach
    			</div>
          <table style="width:100%;">
            <tr>
              <td style="text-align:left;font-weight:bold;">Subtotal</td>
              <td style="text-align:right;">${{ number_format( $reservation->subtotal, 2, '.', ',') }}</td>
            </tr>
            <tr>
              <td style="text-align:left;font-weight:bold;">Online discount</td>
              <td style="text-align:right;">${{ number_format( $reservation->descuento, 2, '.', ',') }}</td>
            </tr>
            <tr>
              <td style="text-align:left;font-weight:bold;">Total</td>
              <td style="text-align:right;">${{ number_format( $reservation->total, 2, '.', ',') }}</td>
            </tr>
          </table>
    		</div>

    		<div class="box">
    			<h3>Contact information</h3>
    			<div>
    				{{ $reservation->nombre }}<br />
    				{{ $reservation->email }}<br />
    				{{ $reservation->phone }}
    			</div>
    		</div>

    		<div class="box">
    			<h3>Payment method</h3>
    			<div>{{$reservation->estatus}}</div>
    			<!-- <div>VISA Visa ending in 1877</div> -->
    		</div>

    		<div class="notes">If you see something wrong please give us a call or leave us a message.</div>

    		<div class="actions">
          <div class="notes">
            <label for="">Phone: </label><label for="">(0052) 624 1307972</label>
          </div>
          <div class="notes">
            <label for="">Email: </label><label for="">blanca.garcia@cabodaytrips.com</label>
          </div>
        </div>
    	</div>

    </div>
  </body>
</html>
