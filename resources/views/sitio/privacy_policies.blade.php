@extends('layouts.app')

@section('metatags')
  <title>Cabo Day Trips</title>
	<meta name="title" content="WELCOME TO CABO DAY TRIPS">
	<meta name="description" content="">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="Cabo Day Trips"/>
	<meta property="og:description"   content="WELCOME TO CABO DAY TRIPS"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="Cabo Day Trips">
	<meta itemprop="description" content="WELCOME TO CABO DAY TRIPS">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="information-section container-max-width">
  <h1>General</h1>
  <p>By using the Website, you accept our Policy. We may revise it at any time by updating the Website. This policy covers how Cabo Day Trips treats personal information that Cabo Day Trips collects and receives, including information related to your past use of Cabo Day Trips products and services.</p>
  <p>Personal information is information about you that is personally identifiable like your name, address, email address, or phone number, and that is not otherwise publicly available.</p>
  <p>This policy does not apply to the practices of companies that Cabo Day Trips does not own or control or to people that Cabo Day Trips does not manage or employ. Please read the following policy to understand how your personal information will be treated as you use our website, cabodaytrips.com</p>
  <p>This policy may change from time to time, so please check back periodically.</p>
  <h1>Personal Information</h1>
  <p>Cabo Day Trips will only obtain personal information from you when necessary and when you choose to provide it, such as if you enter a sweepstakes, contest or promotion; or when you register; or when you sign up for Email specials; or when you request information about various products and services we offer; or when you make travel arrangements using our website; or if you contact Cabo Day Trips using our website. If you contact Cabo Day Trips, we may keep a record of that correspondence. Wherever Cabo Day Trips collects personal information we try to include a link to this Privacy Policy on that page.</p>
  <p>Personal information that we may request from you to provide you with our product could include your name, date of birth, address, phone number, email address, password, credit card number, credit card expiration date, and the amount that is being charged or paid. This personal information is collected when you voluntarily complete the online booking form that requests this information. The information you provide may be used to provide you with a product, collect payment for our product, send your promotions and news bulletins, and/or to allow you access to our online services.</p>
  <h1>Account Information & Travel Preferences</h1>
  <p>You may choose to register with us and create a personal account. If you create an account, you will allow us to tailor your search results to your own personal interests and preferences. The personal information we collect from you, if you choose to register includes: name, address, email address, preferred travel agent, password and password reminder. The travel preference information we collect from you includes: departure state, preferred airport, destination/hobby interests, duration of stay preference, price range and seasonal preference.</p>
  <p>You must create a password to create an account, so your account is password-protected. This is unique and regarded as confidential; consequently, it should be carefully guarded. Ultimately, you are solely responsible for maintaining the secrecy of your passwords, User IDs, and/or any account information. Our secure server encrypts all personal information so that it cannot be read as it is transmitted over the Internet. You may modify your travel profile and account information at any time by updating your travel preferences and/or your account information.</p>
  <h1>E-mail Communications</h1>
  <p>Cabo Day Trips may, from time to time, send you e-mail messages regarding our product specials and services, you automatically opted-in to receive such messages by using any of our services. Once you have opted-in to receive e-mails from us, you may choose to opt-out of e-mail communications at any time by updating your account. Regardless of your communication preferences, even if you have opted-out of e-mail marketing messages, cabodaytrips.com retains the right to send you a transaction-specific email confirmation when you have booked a vacation service on our website.</p>
  <h1>Security</h1>
  <p>Cabo Day Trips has implemented technology and systems that protect the information you provide. In those instances when you are asked to submit private information such as a credit card number, you can be assured that we have security measures in place to protect this information. We protect your information with state-of-the-art Secure Socket Layer SSL Technology. This technology provides the advanced encryption tools necessary to protect information transmitted between your computer and our server. Encryption means that your information is converted into code before it is dispatched over the Internet.</p>
  <h1>Use of Cookies</h1>
  <p>To help us provide you with a more personalized experience, we may send a cookie to your browser. The cookie is a small text file that resides on your hard drive. This cookie will indicate if you are registered or not, your home gateway airport preference if you have registered and your last search values for a gateway and a destination. Most web browsers allow you to exercise control of cookie files on your computer by erasing them, blocking them, or notifying you when such a file is stored. If you choose to block cookies, you will not be able to use certain features of our website. Consult your web browser documentation for instructions on how to control this function.</p>
  <h1>Why does Cabo Day Trips collect information?</h1>
  <p>Cabo Day Trips primary goal in collecting personal information is to provide you, the user, with a customized experience on our site. This includes customized packages to meet your travel plans, budget and other preferences, interactive communications, and other types of services, which are completely free to you. Cabo Day Trips may advertise certain hotels, destinations, or excursions. By knowing a little about you, Cabo Day Trips can deliver more relevant advertisements, packages,
    and vacation plans to you. In this process, the advertiser never has access to your individual information only Cabo Day Trips has access to this information.</p>
  <p>Cabo Day Trips does research on our user's demographics, interests, and behaviors based upon the information provided to us through our website. We do this to better understand and serve our users. This research is compiled and analyzed on an aggregated basis. Cabo Day Trips may share this aggregated data with advertisers or business partners.</p>
  <h1>Who is collecting information?</h1>
  <p>When you are using Cabo Day Trips site and are asked for personal information, you are sharing that information with Cabo Day Trips alone, unless it is specifically stated otherwise. If data is being collected and/or maintained by any company other than Cabo Day Trips, you will be notified prior to the time of the data collection or transfer. If you do not want your data to be shared, you may choose not to allow the transfer by not using that particular service. Promotions that run on Cabo Day Trips website may be co-sponsored by Cabo Day Trips and another company.</p>
  <p>Some or all data collected during a promotion may be shared with the sponsor or co-sponsor. If data will be shared, you will be notified prior to the time of data collection or transfer. You can decide not to participate in the promotion if you don't want your data to be shared. Please be aware that Cabo Day Trips advertisers or websites that have links on our site may collect personally identifiable information about you. This privacy statement does not cover the information collections practices of those websites.</p>
  <h1>Legal Statement</h1>
  <p>Policy and guidelines for use of marks in advertising, promotion and display Cabo Day Trips and its affiliates have the exclusive rights to use the Cabo Day Trips service mark and other Cabo Day Trips marks and logos. Under the trademark laws, the Cabo Day Trips companies must prevent the use of their names and marks in ways that could give customers and potential customers the mistaken impression that a Cabo Day Trips company owns or is affiliated with an agency using its marks. Cabo Day Trips (and trademark law) allows the "fair use" of Cabo Day Trips marks and logos by agencies, in a truthful, informational way in accordance with the rules provided here. Please adhere to the following specific rules when using Cabo Day Trips names and marks in promotions on Internet websites and in print advertisements.</p>
  <h1>General Rules</h1>
  <p>All uses of the Cabo Day Trips name or any Cabo Day Trips mark must relate solely to Cabo Day Trips travel packages and not to the agencies selling them. Agencies that sell Cabo Day Trips packages may not refer to themselves in any context as "Cabo Day Trips travel agents" or to their businesses as "official Cabo Day Trips travel agencies."</p>
  <p>Travel agencies may only use signage, window displays, or point of purchase items employing the Cabo Day Trips name or any Cabo Day Trips mark (a) as provided by Cabo Day Trips, or (b) as approved in writing by Cabo Day Trips.</p>
  <p>No one may copy any content appearing in any Cabo Day Trips website, brochure, or advertising without express written permission from Cabo Day Trips.</p>
  <p>Cabo Day Trips marks should be used with the ® symbol, and the use should be accompanied by a legend appearing at the bottom of the page where used that the mark is registered; for example, Cabo Day Trips is a registered trademark.</p>
  <h1>Internet Websites</h1>
  <p>No one may use the name Cabo Day Trips or any Cabo Day Trips mark (or anything confusingly similar) as part of an Internet domain name.</p>
  <p>No agency may display the Cabo Day Trip mark by itself on a website without prior written approval.</p>
  <p>Travel agencies may use a Cabo Day Trip mark in a website in the following ways:</p>
  <p>A Cabo Day Trips mark may be used as part of a statement such as "We sell Cabo Day Trips travel packages," so long as all the words in the sentence are of a similar size.</p>
  <p>A Cabo Day Trips mark may be used in conjunction with links, schedules, product descriptions, or order forms, expressly referring to the sale of Cabo Day Trips travel products by the agency.</p>
  <p>In any case where the Cabo Day Trips name or mark is used, the name of the travel agency must appear on the same screen, above the Cabo Day Trips name or mark, and in a size at least 125% of the size of the Cabo Day Trips name or mark used.</p>
  <h1>Print Advertising</h1>
  <p>No Cabo Day Trips marks may appear in Yellow Pages advertising. As discussed above, truthful, informational statements are permitted in promotional materials and advertisements, such as, "We sell Cabo Day Trips travel packages." In any case where the Cabo Day Trips name or mark is used, the name of the travel agency must appear in close proximity, above the Cabo Day Trips name or mark, and in a size at least 125% of the size of the Cabo Day Trips name or mark used.</p>
  <p>Marks must not be used as an agency identifier; i.e., an Cabo Day Trips mark may not appear by itself or at the top of a page in an advertisement.</p>
  <h1>Our Contact Information</h1>
  <p>Questions regarding this statement should be directed to blanca@cabodaytrips.com If you do not receive acknowledgement or if your inquiry has not been satisfactorily addressed, please write to:</p>
  <p>Mexico</p>
  <p>blanca.garcia@cabodaytrips.com</p>
</section>
@endsection

@section('javascript')
@endsection
