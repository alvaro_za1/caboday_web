@extends('layouts.app')

@section('metatags')
	<title>{{$encabezado->title}}</title>
	<meta name="title" content="{{$encabezado->title}}">
	<meta name="description" content="{{$encabezado->description}}">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="{{$encabezado->title}}"/>
	<meta property="og:description"   content="{{$encabezado->description}}"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="{{$encabezado->title}}">
	<meta itemprop="description" content="{{$encabezado->description}}">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="padding-orientation">
	<div class="banner-mobile">
		<img src="{{asset('assets/img/home/home-hero.png')}}" alt="">
	</div>
	<div class="container-max-width">
		<div class="home-top">
			<div class="circle-left">
				<img src="{{asset('assets/img/yellow-circle.svg')}}" alt="">
			</div>
			<div class="home-left parallax--box">
				<div style="position:relative;" class="">
					<img src="{{asset('assets/img/home/home-hero.png')}}" alt="">
					<div class="wave-left">
						<img src="{{asset('assets/img/wave.svg')}}" alt="">
					</div>
					<div style="background-image:url({{asset('assets/img/home/home-hero.png')}});" class="img-blur"></div>
				</div>
			</div>
			<div class="home-right">
				<div style="position:relative;" class="">
					<div class="container-home-right">
						<h1>{{ $encabezado->h1_tag }}</h1>
						<div class="">
							<h2>{{ $encabezado->h2_tag }}</h2>
							<a class="link-text" href="{{asset('tours-and-activities')}}">{{trans('app.book_now')}}</a>
						</div>
					</div>
					<div class="wave-right">
						<img src="{{asset('assets/img/wave.svg')}}" alt="">
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		</div>
	</div>
</section>
<div class="relative-circle relative-circle-height-right">
	<div class="big-circle-right">
		<img src="{{asset('assets/img/yellow-circle.svg')}}" alt="">
	</div>
</div>
<section class="padding-orientation">
	<div>
		<div class="introduction-home">
			<p>{!! trans('home.about_us_home_text') !!}</p>
				<label for="">Blanca García, CEO & Owner</label>
				<div style="margin:30px 0px;" class="">
					<a class="link-text" href="{{asset('about-us')}}">{{trans('app.learn_more')}}</a>
				</div>
		</div>
	</div>
</section>
<div class="relative-circle relative-circle-height-left">
	<div class="big-circle-left">
		<img src="{{asset('assets/img/yellow-circle.svg')}}" alt="">
	</div>
	<div class="">

	</div>
	<div class="container-max-width container-max-circle">
		<div class="container-chosen">
			<div class="">
				<h3>{!! trans('home.home_chosen_text') !!}</h3>
			</div>
			<img src="{{asset('assets/img/wave.svg')}}" alt="">
		</div>
	</div>
</div>
<div class="container-home-tours">
	<div class="container-max-width">
		<!-- <div class="home-activities-container" style="display:block"> -->
		<div class="home-activities-container">
			<div class="home-activities-left">
				<div class="">
					<h3>{!! trans('home.tour_activities_title') !!}</h3>
					<div class="">
						<div class="activities-bar">
							<div class="progress"></div>
						</div>
						<div class="activities-number">
							<div class="">
								<span class="icon-arrow-left"></span>
							</div>
							<div class="">
								<label id="index-category" for="">1</label>
								<label for="">/</label>
								<label for="">{{count($CategoriesTour)}}</label>
							</div>
							<div class="">
								<span class="icon-arrow-next"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="home-activities-right" style="overflow-x:auto"> -->
			<div class="home-activities-right">
				<div class="table-activities-overflow">
					<div class="container-table-activities" style="width:{{ count($CategoriesTour) * 360 }}px">
						<div class="table-activities table-activities-tours">
							@foreach($CategoriesTour as $row)
							<div class="">
								<a href="{{ url('tours-and-activities?category='.str_slug($row->nombre_es)) }}" style="display:block">
									<div class="" style="margin-bottom:20px">
										<img src="{{ Config::get('constants.BANNERS_CATEGORIAS_TOURS_NORMAL') . $row->image->imagen_normal }}" alt="{{ $row['nombre_'.Session::get('language')] }}">
										<div style="background-image: url({{asset('assets/img/home/waiting-for-img.png')}});" class="img-blur"></div>
									</div>
									<label for="">{{ $row['nombre_'.Session::get('language')] }}</label>
								</a>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-home-tours">
	<div class="container-max-width">
		<div class="home-activities-container">
			<div class="home-activities-left">
				<div class="">
					<h3>{!! trans('home.safe_trans_title') !!}</h3>
					<div class="transportation-bottom">
						<a class="link-text" href="{{asset('safe-transportation')}}">{{trans('app.learn_more')}}</a>
					</div>
				</div>
			</div>
			<div class="home-activities-right-70">
				<div class="table-activities">
					@foreach($CategoriesTransport as $row)
					<div class="">
						<a href="{{ url('safe-transportation?category='.str_slug($row->nombre_es)) }}" style="display:block">
							<div class="" style="margin-bottom:20px">
								<img src="{{ Config::get('constants.THUMBNAIL_CATEGORIA_TRANSPORTACION') . $row->imagen }}" alt="{{ $row['nombre_'.Session::get('language')] }}">
								<div style="background-image: url({{ Config::get('constants.THUMBNAIL_CATEGORIA_TRANSPORTACION') . $row->imagen }}) ;" class="img-blur"></div>
							</div>
							<label for="">{{ $row['nombre_'.Session::get('language')] }}</label>
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<div class="padding-orientation" style="overflow:hidden;">
	<div class="container-max-width">
		<div class="waiting-section">
			<div class="">
				<div class="">
					<h3>{!! trans('home.waiting_for_title') !!}</h3>
					<img src="{{asset('assets/img/wave.svg')}}" alt="">
					<div style="padding-top:50px;" class="">
						<a href="{{asset('tours-and-activities')}}">
							<button class="blue-button" type="button" name="button">{{trans('app.book_now')}}</button>
						</a>
					</div>
				</div>
			</div>
			<div class="">
				<div style="position:relative;" class="">
					<div class="waiting-wave">
						<img src="{{asset('assets/img/wave.svg')}}" alt="">
					</div>
					<img src="{{asset('assets/img/home/waiting-for-img.png')}}" alt="">
					<div class="waiting-circle">
						<img src="{{asset('assets/img/yellow-circle.svg')}}" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascript')

@endsection
