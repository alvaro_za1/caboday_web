@extends('layouts.app')

@section('metatags')
  <title>{{$encabezado->title}}</title>
	<meta name="title" content="{{$encabezado->title}}">
	<meta name="description" content="{{$encabezado->description}}">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="{{$encabezado->title}}"/>
	<meta property="og:description"   content="{{$encabezado->description}}"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="{{$encabezado->title}}">
	<meta itemprop="description" content="{{$encabezado->description}}">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="contatc-us container-max-width">
  <h1>{!! $encabezado->h1_tag !!}</h1>
  <h2>{!! $encabezado->h2_tag !!}</h2>
  <h4>{{ Session::get('contactMsg') }}</h4>

  <form class="" action="{{ url('contact-us') }}" method="post">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="text" name="name"  placeholder="{{trans('contact.form_name')}}">
    <input type="text" name="email"  placeholder="{{trans('contact.form_email')}}">
    <input type="text" name="phone"  placeholder="{{trans('contact.form_phone')}}">
    <input type="text" name="subject"  placeholder="{{trans('contact.form_subject')}}">
    <textarea name="message"  rows="8" cols="80" placeholder="{{trans('contact.form_message')}}"></textarea>

    <input type="submit" value="{{trans('contact.form_submit')}}">

    <!-- Info -->
    <div class="contact-info">
      <h2>{{trans('contact.contact_info_title')}}</h2>
      <img class="wave" src="{{asset('assets/img/wave.svg')}}" alt="Wave">

      <h3>{{trans('contact.form_office')}}</h3>
      <p>Retorno Tifon 1878,<br />Col. Rosarito, San Jose del Cabo<br />Baja California Sur, 23440 <br />México.</p>

      <h3>{{trans('contact.form_telephone')}}</h3>
      <p>(0052) 624 1307972</p>

      <h3>{{trans('contact.form_email')}}</h3>
      <p><a href="mailto:blanca.garcia@cabodaytrips.com">blanca.garcia@cabodaytrips.com</a></p>
    </div>

    <!-- Image -->
    <div class="contact-image">
      <div style="position:relative;">
        <img src="{{ asset('assets/img/contact/contact-img.png') }}" alt="Contact us">
        <div style="background-image:url({{ asset('assets/img/contact/contact-img.png') }});" class="img-blur"></div>
      </div>
    </div>
  </form>
</section>
@endsection

@section('javascript')
@endsection
