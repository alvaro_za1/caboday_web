@extends('layouts.app')

@section('metatags')
  <title>{{$encabezado->title}}</title>
	<meta name="title" content="{{$encabezado->title}}">
	<meta name="description" content="{{$encabezado->description}}">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="{{$encabezado->title}}"/>
	<meta property="og:description"   content="{{$encabezado->description}}"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="{{$encabezado->title}}">
	<meta itemprop="description" content="{{$encabezado->description}}">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="aboutus container-max-width">

  <div class="welcome">
    <div class="welcome-top">
      <div class="gray-box"></div>
      <div class="gray-line"></div>
      <img class="aboutus" src="{{asset('assets/img/aboutus/hero-about-us.png')}}" alt="About us">

      <h1>{!! $encabezado->h1_tag !!}</h1>
      <img class="wave" src="{{asset('assets/img/wave.svg')}}" alt="Wave">
    </div>
  </div>

  <h2>{!! $encabezado->h2_tag !!}</h2>
  <p style="margin: 15px;" class="welcome-text">Me, Blanca, and my husband combined have more than 40 years of experience in transportation and fun tours and activities in Los Cabos Mexico.</p>

  <div class="services">
    <div class="gray-box"></div>

    <img class="most-important" src="{{asset('assets/img/aboutus/most-important-img.png')}}" alt="Most important">
    <img class="wave" src="{{asset('assets/img/wave.svg')}}" alt="Wave">
    <div class="gray-line"></div>

    <article>
      <h3>You are the most important</h3>
      <p>After working for larger companies and corporations we have identified the single most important value for our customers, SERVICE. While in large companies and corporations you are just a number and not important, for us You are the most important person in the entire world. You guarantee the education of our Children and their well-begin.</p>
    </article>
  </div>

  <div class="services-text">
    <div>
      We have carefully and with love chosen the safest and best options for transportation in Los Cabos,
      we have carefully pre-screened all activities offered with your safety in mind.
      All our suppliers carry proper Insurance, have safety procedures in place and are the best and safest options
      for Excursions and transportation in Los Cabos. With us you will get passion and love, you count and are not just a number
      in a million of dollars bank statement. We have taken the decision to open Cabo Day Trips after listening and seeing
      first hand thousands and thousands of unhappy people, people that had absolutely
    </div>
    <div>
      the right to be upset, they never were important to the company that got the Money, the follow-up and response were late
      or nonexistent, after they have paid for a service those companies did and do not deliver, did not care and even worse
      do not even respond and refund quickly, it can take weeks or month to get your money back, not even considering the fact
      of your bad experience. You call them, and you get the runaround.
      <br /><br />
      Therefore, we decided to open up our little company, we will avoid all those problems with</div>
    <div>
      the experience we have, because we care.
      <br /><br />
      We are looking forward receiving you in our little Paradise and make Your Vacation an easy and worry-free Vacation.
      We will take care of all the little details for you.
    </div>
  </div>

  <div class="attention">
    <p>Sincerely yours</p>
    <h2>Blanca Estela García Ramírez & Family</h2>
    <div>Passionate owner & COO of Cabo Trips</div>
  </div>

</section>
@endsection

@section('javascript')
@endsection
