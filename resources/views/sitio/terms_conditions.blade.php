@extends('layouts.app')

@section('metatags')
  <title>Cabo Day Trips</title>
	<meta name="title" content="WELCOME TO CABO DAY TRIPS">
	<meta name="description" content="">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="Cabo Day Trips"/>
	<meta property="og:description"   content="WELCOME TO CABO DAY TRIPS"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="Cabo Day Trips">
	<meta itemprop="description" content="WELCOME TO CABO DAY TRIPS">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="information-section container-max-width">
  <h1>TERMS AND CONDITIONS</h1>
  <p>The information data and material contained in this website has been prepared solely for the purpose of providing information about abo Day Trips, its subsidiaries and partners and the services that they offer.</p>
  <p>Your access to the website is subject to the following terms and conditions. By using the website, you agree to be bound by the Terms and Conditions and we therefore encourage you to click through to read the Terms and Conditions in full. If you do not agree to these Terms and Conditions, please do not use the website. Please also see our Privacy page which explains how we treat your information.</p>
  <h1>1. YOUR USE OF THE WEBSITE</h1>
  <p>You agree:</p>
  <p>1.1 to abide by all applicable laws, regulations and codes of conduct when using the website and to be solely responsible for all things arising from your use of the website;</p>
  <p>1.2 not to use the website in any way which might infringe any rights of any third party or give rise to a legal claim against Cabo Day Trips by any third party;</p>
  <p>1.3 not to damage, interfere with or disrupt access to the website or do anything that may interrupt or impair its functionality;</p>
  <p>1.4 not to obtain or attempt to obtain unauthorized access, through whatever means, to the website or other services or computer systems or areas of our, or any of our partners’, networks which are identified as restricted;</p>
  <p>1.5 not to collect or store personal data about other users for commercial purposes;</p>
  <p>1.6 to respect the privacy of your fellow Internet users;</p>
  <p>1.7 to provide true, accurate, complete and current information to us and notify us immediately of any change.</p>
  <h1>2. CONTENT</h1>
  <p>2.1 The material and content provided to you on the website is solely for your personal non- commercial use and you agree not for yourself or through any third party to distribute or commercially exploit all or any part of the Content.</p>
  <p>2.2 All Content (including, but not limited to articles, features, photographs, images, brands, logos, illustrations, audio clips and video clips, as well as all products, software, technology or processes described in this website are protected by copyright, trademarks, service marks and/or other intellectual property rights and laws and all Rights in relation to the website are and shall remain owned or controlled by Cabo Day Trips, or as appropriate, the third-party Rights owner. You shall abide by all additional copyright notices, information, or restrictions contained in any Content accessed through this website.</p>
  <p>2.3 Nothing contained on the website should be construed as granting, by implication or otherwise, any license or right to use, deal with or copy in any way in party or in whole any Rights without our written permission or, as appropriate, the permission of the third-party Rights owner. Your misuse of the Rights, except as expressly provided in these Terms and Conditions, is strictly prohibited.</p>
  <h1>3. ACCESS AND AVAILABILITY OF SERVICE AND LINKS</h1>
  <p>This website from time to time contains links to other related World Wide Web Internet sites, resources and sponsors of this website. Since Cabo Day Trips does not approve, check, edit, vet or endorse such sites, you agree that Cabo Day Trips is not responsible or liable in any way for the content, advertising or products available from such sites or any dealings that you may have, or the consequences of such dealings, with the operators of such sites. You agree that any dealings you have with such third party site operators shall be on the terms and conditions (if any) of the third party site operator and should direct any concerns regarding any external link to the site administrator or Webmaster of such site. Cabo Day Trips makes no representations nor does it take any responsibility in relation to the content of any sites accessed through these links.</p>
  <h1>4. CHANGES TO TERMS AND CONDITIONS</h1>
  <p>Cabo Day Trips may from time to time change, alter, adapt, add or remove portions of these Terms and Conditions and, if it does so, will post any such changes on this website. Your continued use of the website after such changes constitutes your acceptance of those changes.</p>
  <h1>5. CHANGES TO WEBSITE</h1>
  <p>Cabo Day Trips may also change, suspend or discontinue any aspect of the website, including the availability of any features, information, database or content or restrict your access to parts or all of the website at its discretion without notice or liability.</p>
  <h1>6. NO WARRANTIES</h1>
  <p>6.1 The website is provided "as is" without any representations or warranties (either express or implied), including but not limited to any implied warranties or implied terms of reliability, quality, functionality, absence of contaminants (including viruses, worms, trojan horses or similar), availability, satisfactory quality, fitness for a particular purpose or non-infringement. All such implied terms and warranties are hereby excluded. Please note that some jurisdictions may not allow the exclusion of implied warranties, so some of the above exclusions may not apply to you. Check your local laws for any restrictions of limitations regarding the exclusion of implied warranties.</p>
  <p>6.2 While Cabo Day Trips uses reasonable efforts to include accurate and up to date information on the website, it makes no warranties or representations as to its accuracy or completeness. Cabo Day Trips is not responsible for any errors or omissions or for the results obtained from the use of such information. The information does not constitute any form of advice, recommendation or arrangement by Cabo Day Trips or its affiliates or any other party involved in the website and is not intended to be relied upon by users in making (or refraining from making) any decisions based on such information. You must make your own decisions on whether or not to rely on any information posted on the website.</p>
  <p>6.3 While Cabo Day Trips takes all reasonable steps to ensure a fast and reliable service it will not be held responsible for the security of the website or for any disruption of the website however caused, loss of or corruption of any material in transit, or loss of or corruption of material or data when downloaded onto any computer system. You will remain responsible and liable for material you upload on to or access from the website and you will indemnify Cabo Day Trips in the manner set out in paragraph 9.2 below in the Terms and Conditions in relation to your accessing or uploading.</p>
  <h1>7. LIABILITY FOR LOSSES/INDEMNITY</h1>
  <p>7.1 By accessing this website you agree that Cabo Day Trips will not be held liable to you or any third party for any direct, indirect, special, consequential or any other loss or damage arising from the use of or inability to use the website or from your access of other material on the internet via web links from this website.</p>
  <p>7.2 You agree to indemnify, keep indemnified, defend and hold harmless Cabo Day Trips and its parent companies, subsidiaries, affiliates and their respective officers, directors, employees, owners, agents, information providers and licensors from and against any and all claims, damages, liability, losses, costs and expenses (including legal fees) (whether or not foreseeable or avoidable) incurred or suffered by any Indemnified Party and any claims or legal proceedings which are brought or threatened arising from your use of, connection with or conduct on the website or any breach by you of these Terms and Conditions. Cabo Day Trips reserves the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you, and in such case, you agree to co-operate without defense of such claim.</p>
  <h1>8. EXCLUSIONS</h1>
  <p>The exclusions and limitations contained in these Terms and Conditions apply only to the extent permitted by law.</p>
  <h1>9. LEGAL JURISDICTION AND APPLICABLE LAW</h1>
  <p>Cabo Day Trips is a Mexican company. The terms and conditions of the use of this website shall be governed in accordance with the laws of the Republic of Mexico. The user is deemed to hereby submit and agree to the exclusive jurisdiction of the courts of Los Cabos, Baja California Sur, Mexico in respect of any disputes arising out of or in connection with this Web Site, so the user expressly waives any jurisdiction that may correspond by reason of his domicile. These terms and conditions or any further terms and conditions referenced on this Web Site or any matter related to or in connection herewith.</p>
  <h1>10. LIABILITY</h1>
  <p>10.1 Online Booking of Independent Third-Party Suppliers: Booking services provided for excursions on this site involve services offered by independent third-party suppliers. Cabo Day Trips is not liable, nor does it accept liability for actions or omissions of the independent contractors supplying the excursions for which booking services are provided; and the purchaser of the excursions here provided shall be deemed to have waived any claims against Cabo Day Trips in connection with the excursions purchased.</p>
  <p>10.2 Products, services or excursions on this site for which booking services may be requested involve activities that may involve risk. The consumer of this service assumes the risk inherent in all such activities. By accepting these services, the purchaser thereof agrees that Cabo Day Trips is not responsible for losses or damages including bodily injury, property damage, or economic loss incurred while participating in the activity for which booking services are provided.</p>
  <p>10.3 Excursions subject to change without notice. Cabo Day Trips provides access to the very best excursions operated by the most reliable vendors. Excursions & Transfers are operated by local third-party venders. Some excursions require physical activity. Please consider the nature of the excursion to make sure it is appropriate for your age and physical condition.</p>
  <h1>11. CONDITIONS OF CONTRACT OF ONLINE BOOKINGS</h1>
  <p>The service supplier reserves the right to cancel, shorten or alter the excursion due to circumstances outside of their control. In the event of such an occurrence, a full or partial refund may be given, however, the consumer hereby waives any claim against Cabo Day Trips, or the service supplier for any consequential damages arising as a result thereof.</p>
  <h1>12. REFUNDS, CANCELLATIONS AND CHANGES</h1>
  <p>100% of purchase price when cancellation is made more than 72 hours in advance.</p>
  <p>50% fee of purchase price when cancellation is made between 48 and 72 hours in advance.</p>
  <p>All cancellations made less than 48 hours prior to the start time of the service will be charged in full unless otherwise supported by a medical note from a qualified doctor stating the valid medical conditions for which the client cannot receive the reserved service(s).</p>
  <p>All changes to the day/time of the same service shall be subject to the following conditions:</p>
  <p>No charges shall apply to change requests made more than 24 hours in advance.</p>
  <p>Some charges may apply when the request is made less than 24 hours in advance.</p>
  <h1>13. TERMINATION AND SUSPENSION</h1>
  <p>Cabo Day Trips (and any persons authorized by it), may at its sole discretion immediately suspend or terminate your right to use the website without any warning if it considers that you have contravened any of these Terms and Conditions. This is without prejudice to any other rights or remedies that Cabo Day Trips may have.</p>
  <h1>14. ASSIGNMENT</h1>
  <p>Cabo Day Trips may assign its rights and obligations under these Terms and Conditions and upon any such assignment it shall be relieved of any further obligation hereunder.</p>
  <h1>15. PAYMENT</h1>
  <p>Cabo Day Trips provides as payment option <b>Openpay</b> as a secure and easy Credit Card Payment Option</p>
</section>
@endsection

@section('javascript')
@endsection
