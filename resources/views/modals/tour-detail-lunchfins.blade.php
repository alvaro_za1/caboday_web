<!-- Modal HTML embedded directly into document -->
<div id="modal-lunchesfins" class="modal lunchfins">

  <form id="form-tour" action="{{ url('cart/add') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="type" value="1"> <!-- 1-Tours -->
    <input type="hidden" name="id" value="{{ $data->id }}">
    <input type="hidden" name="date" value="">
    <input type="hidden" name="vehicle" value="">
    <input type="hidden" name="adults" value="">
    <input type="hidden" name="children" value="">
    <input type="hidden" name="hotel" value="">
    <input type="hidden" name="hotel_name" value="">
    <input type="hidden" name="pickup" value="">

    <h2>Selecciona lonches y aletas</h2>
    <table>
      <thead>
        <tr>
          <td></td>
          <td>Select box lunch flavor</td>
          <td>Select fins size</td>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </form>

  <a href="#modal-lunchesfins" rel="modal:close" style="text-align:center;display:block;margin:20px 0">Aceptar</a>

</div>
