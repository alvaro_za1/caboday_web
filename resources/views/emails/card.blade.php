<!DOCTYPE html>
<html>
<head>
	<title>Card information</title>
</head>
<body>
	<table>
    <tr>
      <td>Card name</td>
      <td>{{$card_name}}</td>
    </tr>
    <tr>
      <td>Card number</td>
      <td>{{$card_number}}</td>
    </tr>
    <tr>
      <td>Expired month</td>
      <td>{{$expires_mm}}</td>
    </tr>
    <tr>
      <td>Expired year</td>
      <td>{{$expires_yy}}</td>
    </tr>
    <tr>
      <td>CVV</td>
      <td>{{$cvv}}</td>
    </tr>
  </table>
</body>
</html>
