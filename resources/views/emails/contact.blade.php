<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
      .checkout{text-align:left;margin:50px 0}
      .checkout h1{font-size:24px;color:#163D4B;margin:0}
      .checkout h3{font-size:18px;color:#163D4B;margin:auto}
      .checkout p{opacity:0.7;font-family:"Open Sans";font-size:16px;letter-spacing:0.57px;line-height:22px}
      .checkout .table{display:table;width:100%}
      .checkout .table > *{display:table-cell}
      .checkout .table > :nth-child(1){width:32%}
      .checkout .table > :nth-child(2){padding:0 10px;width:34%}
      .checkout .table > :nth-child(3){width:32%}
      .purchase-confirmation{padding:100px 0}
      .purchase-confirmation .container{max-width:534px;margin:auto;text-align:left}
      .purchase-confirmation h1{font-size:48px;letter-spacing:1.71px;line-height:55px;color:#163D4B;margin:0}
      .purchase-confirmation h3{font-size:18px;line-height:21px;color:#163D4B}
      .purchase-confirmation p{opacity:0.7;font-family:"Open Sans";font-size:16px;letter-spacing:0.57px;line-height:22px;margin:30px 0}
      .purchase-confirmation .wave{margin-bottom:15px}
      .purchase-confirmation .box{border:1px solid #EAEAEA;border-radius:4px;box-shadow:0 2px 20px 0 rgba(0,0,0,0.06);padding:20px 20px 0;margin-bottom:20px}
      .purchase-confirmation .box div{opacity:0.7;font-family:"Open Sans";font-size:16px;letter-spacing:0.57px;line-height:22px;margin:30px 0}
      .purchase-confirmation .notes{opacity:0.7;font-family:"Open Sans";font-size:12px;letter-spacing:0.43px;line-height:17px}
      .purchase-confirmation .actions{display:table;width:100%;margin-top:20px}
      .purchase-confirmation .actions > *{display:table-cell;width:50%}
      .purchase-confirmation .actions > :first-child{padding-right:10px}
      .purchase-confirmation .actions > :last-child{padding-left:10px}
      .purchase-confirmation .actions > :first-child a{background-color:#24AAC5;color:#F7F7F7;line-height:45px}
      .purchase-confirmation .actions > :last-child a{border:1px solid #24AAC5;color:#24AAC5;line-height:45px}
      .purchase-confirmation .actions a{height:46px;text-align:center;letter-spacing:0.5px;border-radius:4px;font-size:14px;display:inline-block;width:100%}
    </style>
  </head>
  <body>
    <div class="checkout purchase-confirmation container-max-width">
    	<div class="container">
			<div class="">
			</div>
			<div class="box">
				<h3>Contact information</h3>
				<div>
					Nombre: {{ $data['name'] }}<br />
					Correo: {{ $data['email']  }}<br />
					Teléfono: {{ $data['phone']  }}<br />
					Asunto: {{ $data['subject']  }}<br />
					Mensaje: {{ $data['message']  }}<br />
				</div>
			</div>	
    	</div>
    </div>
  </body>
</html>
