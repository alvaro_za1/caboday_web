@extends('layouts.app')

@section('metatags')
	<title>{{$encabezado->title}}</title>
	<meta name="title" content="{{$encabezado->title}}">
	<meta name="description" content="{{$encabezado->description}}">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="{{$encabezado->title}}"/>
	<meta property="og:description"   content="{{$encabezado->description}}"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="{{$encabezado->title}}">
	<meta itemprop="description" content="{{$encabezado->description}}">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="padding-orientation">
  <div class="container-max-width">
		<h1 class="tours-h1">{!! $encabezado->h1_tag !!}</h1>
		<h2 class="tours-h2">{!! $encabezado->h2_tag !!}</h2>
    <div class="menu-tours-list">
			<div class="container-tours-list">
				<div class="{{ $category ? '' : 'select-category' }}">
	        <a data-id="all-tours" href="">{{trans('app.all_tours_title')}}</a>
	      </div>
				@foreach($categorias as $categoria)
					<div class="{{ $category == str_slug($categoria->nombre_es) ? 'select-category' : '' }}">
						<a data-id="{{str_slug($categoria->nombre_es)}}" class="select-category-tour" href="#">{{$categoria['nombre_'.Session::get('language')]}}</a>
					</div>
				@endforeach
	    </div>
    </div>
	<div class="align-right show-responsive tours-arrows">
		<div class="inline">
			<span style="font-size:25px;color:#163d4b;margin-top:25px;margin-right:15px;" class="icon-arrow-next"></span>
		</div>
	</div>
		<div id="all-tours" class="container-tours-item" style="display:{{ $category ? 'none' : 'block' }}">
			<div class="pan" data-grande>
				@foreach($data as $row)
					<figure>
						<a href="{{ asset('tours-and-activities/'.str_slug($row['nombre_'.Session::get('language')]).'/'.$row->id)}} ">
							@if ($row->descuento > 0)
							<div data-descuento="{{ ($row->descuento*100).'% OFF' }}" class="imagen">
							@else
							<div class="imagen">
							@endif
								<img src="{{ Config::get('constants.THUMBNAIL_TOURS') . $row->thumbnail }} " alt="{{ $row['nombre_'.Session::get('language')] }}">
								<div style="background-image:url({{ Config::get('constants.THUMBNAIL_TOURS') . $row->thumbnail }});" class="img-blur"></div>
							</div>
							<div class="title">
								<div class="">
									<figcaption>{{ $row['nombre_'.Session::get('language')] }}</figcaption>
								</div>
							</div>
							<div class="duration">
								<div class="">
									<span class="icon-duration"></span>
									<label for="">{{ $row->duracion }} hours</label>
								</div>
							</div>
						</a>
					</figure>
				@endforeach
			</div>
		</div>
	@foreach($categorias as $categoria)
		<div id="{{str_slug($categoria->nombre_es)}}" class="container-tours-item" style="display:{{ $category == str_slug($categoria->nombre_es) ? 'block' : 'none' }}">
			<div class="pan" data-grande>
				@foreach($categoria->tours as $row)
					<figure>
						<a href="{{ asset('tours-and-activities/'.str_slug($row['nombre_'.Session::get('language')]).'/'.$row->id)}} ">
							@if ($row->descuento > 0)
							<div data-descuento="{{ ($row->descuento*100).'% OFF' }}" class="imagen">
							@else
							<div class="imagen">
							@endif
								<img src="{{ Config::get('constants.THUMBNAIL_TOURS') . $row->thumbnail }} " alt="{{ $row['nombre_'.Session::get('language')] }}">
								<div style="background-image:url({{ Config::get('constants.THUMBNAIL_TOURS') . $row->thumbnail }});" class="img-blur"></div>
							</div>
							<div class="title">
								<div class="">
									<figcaption>{{ $row['nombre_'.Session::get('language')] }}</figcaption>
								</div>
							</div>
							<div class="duration">
								<div class="">
									<span class="icon-duration"></span>
									<label for="">{{ $row->duracion }} hours</label>
								</div>
							</div>
						</a>
					</figure>
				@endforeach
			</div>
		</div>
	@endforeach
</section>
@endsection

@section('javascript')

@endsection
