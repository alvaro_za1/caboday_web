@extends('layouts.app')

@section('metatags')
	@if($data->meta_title)<title>{{ $data->meta_title }}</title>@else <title>Cabo Day Trips</title> @endif
	<meta name="title" content="{{ $data->meta_title }}">
	<meta name="description" content="{{ $data->meta_descripcion }}">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="{{ $data->meta_title }}"/>
	<meta property="og:description"   content="{{ $data->meta_descripcion }}"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="{{ $data->meta_title }}">
	<meta itemprop="description" content="{{ $data->meta_descripcion }}">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
  @include('includes.banner_detail')
@endsection

@section('content')
<section class="padding-orientation">
  <div class="container-max-width">
		<div class="detail-title">
			<h1>{{ $data['nombre_'.Session::get('language')] }}</h1>
		</div>
    <div class="banner">
      <ul class="slider">
				@foreach ($data->images as $index => $image)
				<li class="{{ $index == 1 ? 'moved' : '' }}">
          <i data-descuento="{{ ($data->descuento*100).'% OFF' }}" class="@if($data->descuento > 0) descuento @endif fondo banner-{{ $image->id }}"></i>
        </li>
				@endforeach
      </ul>
      <div class="container-arrows">
        <div data-side="left" class="move-left">
          <span class="icon-arrow-left"></span>
        </div>
        <div data-side="right" class="move-right">
          <span class="icon-arrow-next"></span>
        </div>
      </div>
    </div>
		<div class="container-tour-detail">
			<div class="detail-title">
				<h2>{{ $data->h2_tag }}</h2>
			</div>
			<div class="detail-extras">
				<div class="">
					<div class="extra">
						<!-- <span class="icon-price"></span> -->
						<label for="">{{trans('tour.from')}} ${{ $data->precio_menor }} {{trans('tour.to')}} ${{ $data->precio_adulto }} USD</label>
					</div>
					<div class="extra">
						<span class="icon-duration"></span>
						<label for="">{{ $data->duracion }} {{trans('tour.hours')}}</label>
					</div>
					<div class="extra">
						<span class="icon-act-level"></span>
						<label for="">{{ $data->level['nombre_'.Session::get('language')] }}</label>
					</div>
					<div class="extra">
						<span class="icon-recommended-for"></span>
						<label for="">{{ $data->recommended['nombre_'.Session::get('language')] }}</label>
					</div>
					<div class="extra">
						<span class="icon-restriction"></span>
						<label for="">{{ $data->denied['nombre_'.Session::get('language')] }}</label>
					</div>
				</div>
				<div class="">
					<div class="extra">
						<a class="venobox" data-autoplay="true" data-vbtype="video" href="{{ $data->video_url }}">
							<span class="icon-youtube"></span>
							<span>{{trans('tour.play_video')}}</span>
						</a>
					</div>
				</div>
			</div>
			<div class="detail-table">
				<div class="">
					<div class="detail-column-left">
						<div class="detail-description">
							{!! $data['descripcion_'.Session::get('language')] !!}
						</div>
						<div class="detail-restrictions">
							<h3>{{trans('tour.restriction_title')}}</h3>
							<div class="restrictions-list">
								<ul class="one-column">
									@foreach ($data->restriccions as $restriccion)
									<li>{{ $restriccion['nombre_'.Session::get('language')] }}</li>
									@endforeach
								</ul>
							</div>
						</div>
						<div class="detail-includes">
							<h3>{{trans('tour.adventure_title')}}</h3>
							<div class="includes-list">
								<ul class="two-columns">
									@foreach ($data->includes as $include)
									<li>{{ $include['nombre_'.Session::get('language')] }}</li>
									@endforeach
								</ul>
							</div>
						</div>
						<div class="detail-bring">
							<h3>{{trans('tour.bring_title')}}</h3>
							<div class="includes-list">
								<ul class="two-columns">
									@foreach ($data->brings as $bring)
									<li>{{ $bring['nombre_'.Session::get('language')] }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="">
					@include('includes.column_right')
				</div>
			</div>
		</div>
		@include('includes.options_transportation')
  </div>
</section>
@endsection

@section('javascript')

@endsection
