@extends('layouts.app')

@section('metatags')
	<title>Cabo Day Trips</title>
	<meta name="title" content="WELCOME TO CABO DAY TRIPS">
	<meta name="description" content="">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="Cabo Day Trips"/>
	<meta property="og:description"   content="WELCOME TO CABO DAY TRIPS"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="Cabo Day Trips">
	<meta itemprop="description" content="WELCOME TO CABO DAY TRIPS">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
	<script type="text/javascript" src="https://resources.openpay.mx/lib/openpay.v1.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
	<script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>
@endsection

@section('content')
<section class="checkout container-max-width">
	<h1>{{trans('checkout.almost_there')}}</h1>
	<p>{{trans('checkout.review_order')}}</p>

	<div class="information-summary">

		<div>
			{!! Form::open(array('url' => 'card-payment', 'method' => 'POST', 'id' => 'form-payment')) !!}
				<div class="customer-information">
					<input type="hidden" name="device_session_id">
					<div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<h3>{{trans('checkout.costumer_information')}}</h3>
						<div class="inputs">
							<div>
								<label for="name">{{trans('contact.form_name')}}</label>
								<input type="text" name="name" placeholder="{{trans('checkout.placeholder_name')}}" data-payment />
							</div>
							<div>
								<label for="email">{{trans('contact.form_email')}}</label>
								<input type="text" name="email" placeholder="mail@example.com" data-payment />
							</div>
							<div>
								<label for="email">Confirmation {{trans('contact.form_email')}}</label>
								<input type="text" name="email2" placeholder="mail@example.com" data-payment />
							</div>
							<div>
								<label for="phone">{{trans('contact.form_phone')}}</label>
								<input type="text" name="phone" class="number" placeholder="{{trans('checkout.placeholder_phone')}}" data-payment />
							</div>
						</div>
					</div>

					<h3>{{trans('checkout.payment_method')}}</h3>
					<div class="container-methods">
						<div class="credit">
							<h4>
								<input type="radio" name="payment-method" checked value="2">
								<span><img style="vertical-align:middle;width:60px;" src="{{asset('assets/img/checkout/visa.svg')}}" alt=""></span>
								<span><img style="vertical-align:middle;width:60px;" src="{{asset('assets/img/checkout/mastercard.svg')}}" alt=""></span>
							</h4>
						</div>
						<div class="paypal">
							<h4>
								<input type="radio" name="payment-method" value="1">
								<img style="vertical-align:middle;width:60px;" src="{{asset('assets/img/checkout/paypal.svg')}}" alt="">
							</h4>
						</div>
					</div>
					<div id="card-form" class="inputs">
						<div>
							<label for="card_name">Card Name</label>
							<input class="required-card" type="text" name="card_name" placeholder="Card Name">
						</div>
						<div>
							<label for="card_number">Card Number</label>
							<input class="required-card" type="text" name="card_number" placeholder="16 digits" maxlength="16" />
						</div>
						<div class="table">
							<div>
								<div>
									<label for="expires_mm">Expire month</label>
									<input class="required-card" type="text" name="expires_mm" placeholder="MM" maxlength="2" />
								</div>
							</div>
							<div>
								<div>
									<label for="expires_yy">Expire year</label>
									<input class="required-card" type="text" name="expires_yy" placeholder="YY" maxlength="2" />
								</div>
							</div>
							<div>
								<div class="cvv-help">
									<label for="cvv">CVV</label>
									<input class="required-card" type="text" name="cvv" placeholder="000" maxlength="4" />
								</div>
							</div>
						</div>
						<!-- <h3>Address information</h3>
						<div class="table">
							<div>
								<label for="city">Country</label>
								<div class="container-select">
									<select name="country_code">
										<option value="">-- Choose a country --</option>
										<option value="MX">Mexico</option>
										<option value="USA">United States of America</option>
										<option value="CA">Canada</option>
									</select>
								</div>
							</div>
							<div>
								<label for="city">State</label>
								<input class="required-card" type="text" name="state" placeholder="State">
							</div>
							<div>
								<label for="city">City</label>
								<input class="required-card" type="text" name="city" placeholder="City">
							</div>
						</div>
						<div class="table">
							<div>
								<label for="city">Street</label>
								<input class="required-card" type="text" name="line1" placeholder="Street address">
							</div>
							<div>
								<label for="city">Number</label>
								<input class="required-card" type="text" name="line2" placeholder="Number">
							</div>
						</div>
						<div class="table">
							<div>
								<label for="city">References</label>
								<input class="required-card" type="text" name="line3" placeholder="References">
							</div>
							<div>
								<label for="city">Postal code</label>
								<input class="required-card" type="text" name="postal_code" placeholder="Postal code">
							</div>
						</div> -->
					</div>
				</div>
				{!! Form::close() !!}
		</div>

		<div>
			<div class="order-summary">
				<h3>{{trans('checkout.order_summary')}}</h3>

				<table class="summaries">
					<tbody>
						<? $discount = 0; ?>
						<? $totalExtras = 0; ?>
						@foreach (Cart::instance('payment')->content() as $item)
						<? $discount += $item->options->discount ?>
						<tr class="{{ count($item->options->extras) > 0 ? '' : 'border' }}">
							<td><span class="icon-delete-item" data-rowId="{{ $item->rowId }}"></span></td>
							<td>
								<img src="{{ asset('assets/img/aboutus/most-important-img.png') }}" alt="Image" width="62" height="50">
							</td>
							<td>
								<h2 class="wrap-text">{{ $item->name }}</h2>
								<div>{{ $item->options->description }}</div>
							</td>
							<td>
								<h2>${{ number_format($item->price,2) }}</h2>
							</td>
						</tr>
						<!-- Extras -->
						@if (count($item->options->extras) > 0)
						<tr class="extras border">
							<td></td>
							<td></td>
							<td>
								@foreach ($item->options->extras as $extra)
								<div>{{ $extra['nombre_'.Session::get('language')] }}</div>
								@endforeach
							</td>
							<td>
								@foreach ($item->options->extras as $extra)
								<? $totalExtras += $extra->precio * $item->options->passengers; ?>
								<div>${{ $extra->precio * $item->options->passengers }} USD</div>
								@endforeach
							</td>
						</tr>
						@endif
						@endforeach
					</tbody>
				</table>

				<div class="subtotal">
					<div>
						<div>Subtotal</div>
						@if ($discount > 0)
						<div>Online discount</div>
						@endif
					</div>
					<div>
						<div data-subtotal>$<span>{{ floatval(str_replace(",","",Cart::instance('payment')->subtotal())) }}</span> USD</div>
						@if ($discount > 0)
						<div data-discount>-$<span>{{ number_format($discount, 2) }}</span> USD</div>
						@endif
					</div>
				</div>

				<!-- <div class="promo-code">
					<div>
						<input type="text" name="promo_code" value="" placeholder="Apply a prome code">
					</div>
					<div>
						<button type="button" name="button">APPLY</button>
					</div>
				</div> -->

				<div class="order-total">
					<div>
						<div>Order Total</div>
					</div>
					<div>
						<div data-total>$<span>{{ floatval(str_replace(",","",Cart::instance('payment')->subtotal())) - $discount }}</span> USD</div>
					</div>
				</div>
				<div class="order-secure">Your order is secure
				<span style="vertical-align:middle;display: inline-block;">
				<script language="JavaScript" type="text/javascript">
					TrustLogo("https://cabodaytrips.com/assets/img/comodo-logo.png", "CL1", "none");
				</script>
					<a  href="https://ssl.comodo.com" id="comodoTL">Comodo SSL</a>
				</span>
				</div>
			</div>
			<p class="questions hide-responsive">Questions? Need help to complete your order?</p>
			<div class="hide-responsive">
				<div class="">
					<a href="tel:+523317804284">
						<span class="icon-phone"></span>
						<div>+(52) 624 1307972</div>
					</a>
				</div>
				<div style="margin-top:15px;">
					<a href="mailto:contacto@cabodaytrip.com">
						<span class="icon-mail"></span>
						<div>blanca.garcia@cabodaytrips.com</div>
					</a>
				</div>
			</div>
		</div>

	</div>

	<!-- Purcharse order -->
	<button type="button" name="button" class="purchase-order {{ Cart::instance('payment')->count() > 0 ? 'correct' : '' }}">{{trans('checkout.purchase_order')}}</button>
	<!-- <a href="{{ url('paypal') }}" ></a> -->
	<p>
		<span>By clicking "PURCHASE ORDER", you agree our</span>
		<b>
			<a href="{{asset('terms-and-conditions')}}">Terms and Conditions</a>
			<span>+</span>
			<a href="{{asset('privacy-policies')}}">Privacy Policy</a>
		</b>
	</p>
</section>
@endsection

@section('javascript')
@endsection
