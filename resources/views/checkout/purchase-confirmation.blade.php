@extends('layouts.app')

@section('metatags')
	<title>Cabo Day Trips</title>
	<meta name="title" content="WELCOME TO CABO DAY TRIPS">
	<meta name="description" content="">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="Cabo Day Trips"/>
	<meta property="og:description"   content="WELCOME TO CABO DAY TRIPS"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="Cabo Day Trips">
	<meta itemprop="description" content="WELCOME TO CABO DAY TRIPS">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="checkout purchase-confirmation container-max-width">

	<div class="container">
		<h1>Thank you for your reservation.</h1>
		<p>Your reservation is confirmed, we send you an email with all details. Please check also your Spam Folder. Email will arrive latest in 90 minutes after your confirmation.</p>

		<img class="wave" src="{{asset('assets/img/wave.svg')}}" alt="Wave">

		<h3>Details</h3>
		<p>You have reserved {{implode(', ', $purchased) }}. Your charge is ${{ $data->total }} USD.</p>

		<div class="box">
			<h3>Contact information</h3>
			<div>
				{{ $data->nombre }}<br />
				{{ $data->email }}<br />
				{{ $data->phone }}
			</div>
		</div>

		<div class="box">
			<h3>Payment method</h3>
			<div>{{$data->estatus}}</div>
			<!-- <div>VISA Visa ending in 1877</div> -->
		</div>

		<div class="notes">Any doubt or question please contact us.</div>

		<div class="actions">
			<div><a href="{{asset('')}}">GO TO HOMEPAGE</a></div>
			<div><a href="{{asset('checkout/download-pdf')}}">DOWNLOAD RECEIPT</a></div>
		</div>
	</div>

</section>
@endsection

@section('javascript')
@endsection
