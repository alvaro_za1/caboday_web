@extends('layouts.app')

@section('metatags')
	<title>Cabo Day Trips</title>
	<meta name="title" content="WELCOME TO CABO DAY TRIPS">
	<meta name="description" content="">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="Cabo Day Trips"/>
	<meta property="og:description"   content="WELCOME TO CABO DAY TRIPS"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="Cabo Day Trips">
	<meta itemprop="description" content="WELCOME TO CABO DAY TRIPS">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="checkout container-max-width">
	<h1>{{trans('checkout.almost_there')}}</h1>
	<p>{{trans('checkout.review_order')}}</p>

	<div class="information-summary">

		<div>
			{!! Form::open(array('url' => 'checkout-arrival', 'method' => 'POST', 'id' => 'form-payment')) !!}
				<div class="customer-information">

					<div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<h3>{{trans('checkout.costumer_information')}}</h3>
						<div class="inputs">
							<div>
								<label for="name">{{trans('contact.form_name')}}</label>
								<input type="text" name="name" placeholder="{{trans('checkout.placeholder_name')}}" data-payment />
							</div>
							<div>
								<label for="email">{{trans('contact.form_email')}}</label>
								<input type="text" name="email" placeholder="mail@example.com" data-payment />
							</div>
							<div>
								<label for="email">Confirmation {{trans('contact.form_email')}}</label>
								<input type="text" name="email2" placeholder="mail@example.com" data-payment />
							</div>
							<div>
								<label for="phone">{{trans('contact.form_phone')}}</label>
								<input type="text" name="phone" class="number" placeholder="{{trans('checkout.placeholder_phone')}}" data-payment />
							</div>
						</div>
					</div>
				</div>
				{!! Form::close() !!}
		</div>

		<div>
			<div class="order-summary">
				<h3>{{trans('checkout.order_summary')}}</h3>

				<table class="summaries">
					<tbody>
						<? $discount = 0; ?>
						<? $totalExtras = 0; ?>
						@foreach (Cart::instance('payarrival')->content() as $item)
						<? $discount += $item->options->discount ?>
						<tr class="{{ count($item->options->extras) > 0 ? '' : 'border' }}">
							<!-- <td><span class="icon-delete-item" data-rowId="{{ $item->rowId }}"></span></td> -->
							<td>
								<img src="{{ asset('assets/img/aboutus/most-important-img.png') }}" alt="Image" width="62" height="50">
							</td>
							<td>
								<h2 class="wrap-text">{{ $item->name }}</h2>
								<div>{{ $item->options->description }}</div>
							</td>
							<td>
								<h2>${{ number_format($item->price,2) }}</h2>
							</td>
						</tr>
						<!-- Extras -->
						@if (count($item->options->extras) > 0)
						<tr class="extras border">
							<td></td>
							<td></td>
							<td>
								@foreach ($item->options->extras as $extra)
								<div>{{ $extra['nombre_'.Session::get('language')] }}</div>
								@endforeach
							</td>
							<td>
								@foreach ($item->options->extras as $extra)
								<? $totalExtras += $extra->precio * $item->options->passengers; ?>
								<div>${{ $extra->precio * $item->options->passengers }} USD</div>
								@endforeach
							</td>
						</tr>
						@endif
						@endforeach
					</tbody>
				</table>

				<div class="subtotal">
					<div>
						<div>Subtotal</div>
						@if ($discount > 0)
						<div>Online discount</div>
						@endif
					</div>
					<div>
						<div data-subtotal>$<span>{{ floatval(str_replace(",","",Cart::instance('payarrival')->subtotal())) }}</span> USD</div>
						@if ($discount > 0)
						<div data-discount>-$<span>{{ number_format($discount, 2) }}</span> USD</div>
						@endif
					</div>
				</div>

				<!-- <div class="promo-code">
					<div>
						<input type="text" name="promo_code" value="" placeholder="Apply a prome code">
					</div>
					<div>
						<button type="button" name="button">APPLY</button>
					</div>
				</div> -->

				<div class="order-total">
					<div>
						<div>Order Total</div>
					</div>
					<div>
						<div data-total>$<span>{{ floatval(str_replace(",","",Cart::instance('payarrival')->subtotal())) - $discount }}</span> USD</div>
					</div>
				</div>
				<div class="order-secure">Your order is secure
				<span style="vertical-align:middle;display: inline-block;">
				<script language="JavaScript" type="text/javascript">
					TrustLogo("https://cabodaytrips.com/assets/img/comodo-logo.png", "CL1", "none");
				</script>
					<a  href="https://ssl.comodo.com" id="comodoTL">Comodo SSL</a>
				</span>
				</div>
			</div>
			<p class="questions hide-responsive">Questions? Need help to complete your order?</p>
			<div class="hide-responsive">
				<div class="">
					<a href="tel:+523317804284">
						<span class="icon-phone"></span>
						<div>+(52) 624 1307972</div>
					</a>
				</div>
				<div style="margin-top:15px;">
					<a href="mailto:contacto@cabodaytrip.com">
						<span class="icon-mail"></span>
						<div>blanca.garcia@cabodaytrips.com</div>
					</a>
				</div>
			</div>
		</div>

	</div>
	<button type="button" name="button" class="purchase-order {{ Cart::instance('payarrival')->count() > 0 ? 'correct' : '' }}">PAY ON ARRIVAL</button>
	<p>
		<span>By clicking "PURCHASE ORDER", you agree our</span>
		<b>
			<a href="{{asset('terms-and-conditions')}}">Terms and Conditions</a>
			<span>+</span>
			<a href="{{asset('privacy-policies')}}">Privacy Policy</a>
		</b>
	</p>
</section>
@endsection

@section('javascript')
@endsection
