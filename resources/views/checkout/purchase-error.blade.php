@extends('layouts.app')

@section('metatags')
	<title>Cabo Day Trips</title>
	<meta name="title" content="WELCOME TO CABO DAY TRIPS">
	<meta name="description" content="">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="Cabo Day Trips"/>
	<meta property="og:description"   content="WELCOME TO CABO DAY TRIPS"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="Cabo Day Trips">
	<meta itemprop="description" content="WELCOME TO CABO DAY TRIPS">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="checkout purchase-error container-max-width">
  <h1>Woops!</h1>
	<p>{{Session::get('error')}}</p>
  <p>
    Something went wrong and your purchase has been declined.
    We suggest you try it again or get in contact with your bank customer service.
  </p>
  <a href="{{asset('checkout')}}">GO BACK TO MY CART</a>
</section>
@endsection

@section('javascript')
@endsection
