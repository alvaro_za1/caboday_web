@extends('layouts.app')

@section('metatags')
	@if($data->meta_title)<title>{{ $data->meta_title }}</title>@else <title>Cabo Day Trips</title> @endif
	<meta name="title" content="{{ $data->meta_title }}">
	<meta name="description" content="{{ $data->meta_descripcion }}">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="{{ $data->meta_title }}"/>
	<meta property="og:description"   content="{{ $data->meta_descripcion }}"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="{{ $data->meta_title }}">
	<meta itemprop="description" content="{{ $data->meta_descripcion }}">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="padding-orientation">
  <div class="container-max-width">
    <div class="container-detail-transport">
      <div class="detail-top">
				<div class="">
					<div class="detail-img-transport">
						<img src="{{ Config::get('constants.THUMBNAIL_TRANSPORTACION') . $data['imagen_detalle'] }}" alt="">
					</div>
				</div>
				<div class="">
					<div class="detail-info-transport">
						<h3>{{ $data['nombre_marca'].' '.$data['nombre_'.Session::get('language')] }}</h3>
						<div class="detail-extras">
							<div class="extra">
								<span class="icon-recommended-for"></span>
								<label for="" data-capacidad="{{ $data->capacidad }}">1-{{ $data->capacidad }}</label>
							</div>
							<div class="extra">
								<span class="icon-duration"></span>
								<label for="">{{ $data['waiting_'.Session::get('language')] }}</label>
							</div>
							<div class="extra">
								<span class="icon-price"></span>
								<label for="">{{ $data->precio }}.00 USD</label>
							</div>
							<div class="extra">
								<label for="">Roundtrip transportation</label>
							</div>
						</div>
						<div class="detail-information">
							<p>{!! $data['descripcion_'.Session::get('language')] !!}</p>
						</div>
						<div class="detail-button">
							<button class="blue-button iwantit" type="button" name="button">{{trans('transportation.i_want_button')}}</button>
						</div>
					</div>
				</div>
      </div>
			<div class="detail-transport-form">
				{!! Form::open(array('url' => 'cart/add', 'method' => 'POST', 'id' => 'form-transport')) !!}
				<input type="hidden" name="type" value="3"> <!-- Transport -->
				<input type="hidden" name="id" value="{{ $data->id }}">

					<h3>{{trans('transportation.book_now_title')}} {{ $data['nombre_marca'].' '.$data['nombre_'.Session::get('language')] }}!</h3>
					<div class="detail-transport-info">
						<div class="title-form">
							<span class="icon-arrival-flight"></span>
							<h2>{{trans('transportation.arrival_flight_title')}}</h2>
						</div>
						<div class="form-inputs">
							<div class="renglon">
								<div class="container-el">
									<label for="">{{trans('transportation.arrival_airline')}}</label>
									<div class="container-select">
										{!! Form::select('arrival_airline', $airlines, '', array('data-el' => 'departure_airline', 'class' => 'required')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.flight_number')}}</label>
									<div class="container-select">
										{!! Form::text('arrival_flight','', array('placeholder' => trans('transportation.placeholder_flight'), 'data-el' => 'departure_flight', 'class' => 'required')) !!}
									</div>
								</div>
								<div class="container-el">
                  <label for="">{{trans('transportation.arrival_date')}}</label>
                  <div class="container-select">
                    {!! Form::text('arrival_date','', array('placeholder' => trans('transportation.placeholder_arrival_date'), 'class' => 'date-picker required', 'readonly' => 'readonly', 'data-el' => 'departure_date')) !!}
                  </div>
                </div>
							</div>
							<div class="renglon">
								<div class="container-el">
									<label for="">{{trans('transportation.arrival_hour')}}</label>
									<div class="container-select">
										{!! Form::text('arrival_hour','', array('placeholder' => 'Insert your arrival hour', 'class' => 'time-picker required', 'data-el' => 'departure_hour')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.adults_arriving')}}</label>
									<div class="container-select">
										<select name="arrival_adults" data-el="departure_adults" class="required">
                        @foreach (range(1, $data->capacidad) as $numero)
                        <option value="{{ $numero }}">{{ $numero }}</option>
                        @endforeach
                    </select>
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.kids_arriving')}}</label>
									<div class="container-select">
										<select name="arrival_kids" data-el="departure_kids">
                        @foreach (range(0, $data->capacidad) as $numero)
                        <option value="{{ $numero }}">{{ $numero }}</option>
                        @endforeach
                    </select>
									</div>
								</div>
							</div>
						</div>

						<div class="title-form">
							<span class="icon-hotel"></span>
							<h2>{{trans('transportation.arrival_hotel_title')}}</h2>
						</div>
						<div class="form-inputs">
							<div class="renglon">
								<div class="container-el">
									<label for="">Hotel</label>
									<div class="container-select">
										<select class="chosen required-hotel" name="arrival_hotel" data-el="departure_hotel">
						          <option value="">{{trans('tour.select_hotel')}}</option>
						          @foreach ($hoteles as $hotel)
						          <option value="{{ $hotel->id }}">{{ $hotel->nombre }}</option>
						          @endforeach
						        </select>
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.other_hotel')}}</label>
	                <input class="required-hotel" type="text" name="arrival_hotel_name" value="" data-el="departure_hotel_name" placeholder="{{trans('transportation.placeholder_other_hotel')}}">
								</div>
								<div class="container-el">

								</div>
							</div>
						</div>

						<div class="title-form">
							<span class="icon-hotel"></span>
							<h2>{{trans('transportation.departure_flight_title')}}</h2>
						</div>
						<div class="form-inputs">
							<div class="renglon">
								<div class="container-el">
									<label for="">{{trans('transportation.departure_airline')}}</label>
									<div class="container-select">
										{!! Form::select('departure_airline', $airlines, '', array('class' => 'required')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.flight_number')}}</label>
									<div class="container-select">
										{!! Form::text('departure_flight','', array('placeholder' => trans('transportation.placeholder_flight'), 'class' => 'required')) !!}
									</div>
								</div>
								<div class="container-el">
                  <label for="">{{trans('transportation.departure_date')}}</label>
                  <div class="container-select">
                    {!! Form::text('departure_date','', array('placeholder' => trans('transportation.placeholder_departure_date'), 'class' => 'date-picker end required', 'readonly' => 'readonly')) !!}
                  </div>
                </div>
							</div>
							<div class="renglon">
								<div class="container-el">
									<label for="">{{trans('transportation.departure_hour')}}</label>
									<div class="container-select">
										{!! Form::text('departure_hour','', array('placeholder' => 'Insert your departure hour', 'class' => 'time-picker required')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.adults_departing')}}</label>
									<div class="container-select">
                    <input type="text" name="departure_adults" value="1" readonly>
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.kids_departing')}}</label>
									<div class="container-select">
										<input type="text" name="departure_kids" value="0" readonly>
									</div>
								</div>
							</div>
						</div>

						<div class="title-form">
							<span class="icon-hotel"></span>
							<h2>{{trans('transportation.departure_hotel_title')}}</h2>
						</div>
						<div class="form-inputs">
							<div class="renglon">
								<div class="container-el">
									<label for="">Hotel</label>
									<div class="container-select">
										<select class="chosen required-hotel" name="departure_hotel">
						          <option value="">{{trans('tour.select_hotel')}}</option>
						          @foreach ($hoteles as $hotel)
						          <option value="{{ $hotel->id }}">{{ $hotel->nombre }}</option>
						          @endforeach
						        </select>
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.other_hotel')}}</label>
	                <input class="required-hotel" type="text" name="departure_hotel_name" value="" placeholder="{{trans('transportation.placeholder_other_hotel')}}">
								</div>
								<div class="container-el">

								</div>
							</div>
						</div>

						<!-- <div class="title-form">
							<span class="icon-extras"></span>
							<h2>Extras</h2>
						</div> -->
						<!-- <div class="extras-form">
							<p>{{trans('transportation.extras_information')}}</p>
							<div class="inputs-extras">
								@foreach ($extras as $extra)
								<div class="">
									<input type="checkbox" name="extras[]" value="{{ $extra->id }}">
									<label for="">{{ $extra['nombre_'.Session::get('language')] }} (${{ $extra->precio }} USD)</label>
								</div>
								@endforeach
							</div>
						</div> -->
					</div>
					<div class="detail-transport-buttons">
						<div></div>
						<div class="button">
							<button class="button-pay-arrival" type="button" name="button" data-button="payarrival">PAY ON ARRIVAL</button>
						</div>
						<!-- <div class="transport-cart">
							<button class="button-back-white-blue" type="button" name="button" data-button="continueshopping">
								<span class="icon-add-to-cart"></span>
			          <span>{{trans('app.add_continue_button')}}</span>
							</button>
						</div> -->
						<div class="">
							<button class="button-back-gray-blue" type="button" name="button" data-button="booknow">{{trans('app.book_now_button')}}</button>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
    </div>
  </div>
</section>
@endsection

@section('javascript')

@endsection
