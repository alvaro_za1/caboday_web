@extends('layouts.app')

@section('metatags')
	<title>{{$encabezado->title}}</title>
	<meta name="title" content="{{$encabezado->title}}">
	<meta name="description" content="{{$encabezado->description}}">

	<!-- FACEBOOK METADATA -->
	<meta property="og:title"         content="{{$encabezado->title}}"/>
	<meta property="og:description"   content="{{$encabezado->description}}"/>
	<meta property="og:image"         content="{{asset('assets/img/nav-logo.png')}}"/>

	<!-- GOOGLE METADA -->
	<meta itemprop="name" content="{{$encabezado->title}}">
	<meta itemprop="description" content="{{$encabezado->description}}">
	<meta itemprop="image" content="{{asset('assets/img/nav-logo.png')}}">
@endsection

@section('content')
<section class="padding-orientation">
  <div class="container-max-width">
		<h1 class="tours-h1">{!! $encabezado->h1_tag !!}</h1>
		<h2 class="tours-h2">{!! $encabezado->h2_tag !!}</h2>
    <div class="container-transport-list">
      <div class="select-category">
        <a data-id="container-private-transport" href="#">{{trans('app.private_transportation')}}</a>
      </div>
      <div class="">
        <a data-id="container-shared-transport" href="#">{{trans('app.shared_shuttle')}}</a>
      </div>
    </div>
		<section id="container-private-transport" class="section-transport">
			<div class="private-text">
	      <p>{{trans('app.transportation_text')}}</p>
	    </div>
	    <div class="container-transportation-items">
				@foreach ($data as $row)
				<figure>
	        <a href="{{ asset('safe-transportation/'.str_slug($row['nombre_'.Session::get('language')]).'/'.$row->id) }}">
	          <div class="transportation-item">
	            <div class="">
	              <img src="{{ Config::get('constants.THUMBNAIL_TRANSPORTACION') . $row->imagen }}" alt="{{ $row['nombre_'.Session::get('language')] }}">
	            </div>
	            <div class="">
	              <div class="transportation-info">
	                <div class="">
	                  <img src="{{ Config::get('constants.THUMBNAIL_TRANSPORTACION') . $row->imagen_marca }}" alt="{{ $row->nombre_marca }}">
	                </div>
	                <h3 class="h3-title">{{ $row['nombre_'.Session::get('language')] }}</h3>
	                <p>1-{{ $row->capacidad }} {{trans('app.passengers')}}</p>
	                <div class="">
	                  <label for="">{{trans('app.starting_at')}}</label>
	                </div>
	                <h3 class="h3-price">${{ $row->precio }} USD</h3>
	              </div>
	            </div>
	          </div>
	        </a>
	      </figure>
				@endforeach
	    </div>
		</section>
		<section id="container-shared-transport" class="section-transport {{ $category == 'compartido' ? '' : 'hide' }}">
			@include('transportacion.shared_detalle')
		</section>
  </div>
</section>
@endsection

@section('javascript')

@endsection
