<section class="padding-orientation">
  <div class="container-max-width">
    <div class="container-detail-transport">
      <div class="detail-top">
				<div class="">
					<div class="detail-img-transport">
						<img src="{{asset('assets/img/transportation/transit-large.png')}}" alt="">
					</div>
				</div>
				<div class="">
					<div class="detail-info-transport">
						<h3>{{ $shared['nombre_'.Session::get('language')] }}</h3>
						<div class="detail-extras">
							<div class="extra">
								<span class="icon-recommended-for"></span>
								<label for="" data-capacidad="{{ $shared->capacidad }}">1-{{ $shared->capacidad }}</label>
							</div>
							<div class="extra">
								<span class="icon-duration"></span>
								<label for="">{{ $shared['waiting_'.Session::get('language')] }}</label>
							</div>
							<div class="extra">
								<span class="icon-price"></span>
								<label for="">${{ number_format($shared->precio,2) }} USD</label>
							</div>
							<div class="extra">
								<label for="">Roundtrip transportation</label>
							</div>
						</div>
						<div class="detail-information">
							<p>{!! $shared['descripcion_'.Session::get('language')] !!}</p>
						</div>
						<div class="detail-button">
							<button class="blue-button iwantit" type="button" name="button">{{trans('transportation.i_want_button')}}</button>
						</div>
					</div>
				</div>
      </div>
			<div class="detail-transport-form">
				{!! Form::open(array('url' => 'cart/add', 'method' => 'POST', 'id' => 'form-shared')) !!}
          <input type="hidden" name="type" value="2"> <!-- Shared Shuttle -->
					<input type="hidden" name="id" value="{{ $shared->id }}">
					<h3>{{trans('app.book_now_button')}}</h3>
					<div class="detail-transport-info">
						<div class="title-form">
							<span class="icon-arrival-flight"></span>
							<h2>{{trans('transportation.arrival_flight_title')}}</h2>
						</div>
						<div class="form-inputs">
							<div class="renglon">
								<div class="container-el">
									<label for="">{{trans('transportation.arrival_airline')}}</label>
									<div class="container-select">
										{!! Form::select('arrival_airline', $airlines, '', array('data-el' => 'departure_airline', 'class' => 'required')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.flight_number')}}</label>
									<div class="container-select">
										{!! Form::text('arrival_flight','', array('placeholder' => trans('transportation.placeholder_flight'), 'data-el' => 'departure_flight', 'class' => 'required')) !!}
									</div>
								</div>
                <div class="container-el">
                  <label for="">{{trans('transportation.arrival_date')}}</label>
                  <div class="container-select">
                    {!! Form::text('arrival_date','', array('placeholder' => trans('transportation.placeholder_arrival_date'), 'class' => 'date-picker required', 'data-el' => 'departure_date')) !!}
                  </div>
                </div>
							</div>
							<div class="renglon">
                <div class="container-el">
									<label for="">{{trans('transportation.arrival_hour')}}</label>
									<div class="container-select">
                    {!! Form::text('arrival_hour','', array('placeholder' => 'Insert your arrival hour', 'class' => 'time-picker required', 'data-el' => 'departure_hour')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.adults_arriving')}}</label>
									<div class="container-select">
                    <select name="arrival_adults" data-el="departure_adults" class="required">
                        @foreach (range(1, $shared->capacidad) as $numero)
                        <option value="{{ $numero }}">{{ $numero }}</option>
                        @endforeach
                    </select>
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.kids_arriving')}}</label>
									<div class="container-select">
                    <select name="arrival_kids" data-el="departure_kids">
                        @foreach (range(0, $shared->capacidad) as $numero)
                        <option value="{{ $numero }}">{{ $numero }}</option>
                        @endforeach
                    </select>
									</div>
								</div>
							</div>
						</div>

						<div class="title-form">
							<span class="icon-hotel"></span>
							<h2>{{trans('transportation.arrival_hotel_title')}}</h2>
						</div>
						<div class="form-inputs">
							<div class="renglon">
								<div class="container-el">
									<label for="">Hotel</label>
									<div class="container-select">
                    <select class="chosen required-hotel" name="arrival_hotel" data-el="departure_hotel">
						          <option value="">{{trans('tour.select_hotel')}}</option>
						          @foreach ($hoteles as $hotel)
						          <option value="{{ $hotel->id }}">{{ $hotel->nombre }}</option>
						          @endforeach
						        </select>
									</div>
								</div>
								<div class="container-el">
                  <label for="">{{trans('transportation.other_hotel')}}</label>
                  <input class="required-hotel" type="text" name="arrival_hotel_name" value="" data-el="departure_hotel_name">
								</div>
								<div class="container-el">

								</div>
							</div>
						</div>

						<div class="title-form">
							<span class="icon-hotel"></span>
							<h2>{{trans('transportation.departure_flight_title')}}</h2>
						</div>
						<div class="form-inputs">
							<div class="renglon">
								<div class="container-el">
									<label for="">{{trans('transportation.departure_airline')}}</label>
									<div class="container-select">
										{!! Form::select('departure_airline', $airlines, '', array('class' => 'required')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.flight_number')}}</label>
									<div class="container-select">
										{!! Form::text('departure_flight','', array('placeholder' => trans('transportation.placeholder_flight'), 'class' => 'required')) !!}
									</div>
								</div>
                <div class="container-el">
                  <label for="">{{trans('transportation.departure_date')}}</label>
                  <div class="container-select">
                    {!! Form::text('departure_date','', array('placeholder' => trans('transportation.placeholder_departure_date'), 'class' => 'date-picker end required')) !!}
                  </div>
                </div>
							</div>
							<div class="renglon">
                <div class="container-el">
									<label for="">{{trans('transportation.departure_hour')}}</label>
									<div class="container-select">
                    {!! Form::text('departure_hour','', array('placeholder' => 'Insert your departure hour', 'class' => 'time-picker required')) !!}
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.adults_departing')}}</label>
									<div class="container-select">
                    <input type="text" name="departure_adults" value="1" readonly>
									</div>
								</div>
								<div class="container-el">
									<label for="">{{trans('transportation.kids_departing')}}</label>
									<div class="container-select">
                      <input type="text" name="departure_kids" value="0" readonly>
									</div>
								</div>
							</div>
						</div>

            <div class="title-form">
              <span class="icon-hotel"></span>
              <h2>{{trans('transportation.departure_hotel_title')}}</h2>
            </div>
            <div class="form-inputs">
              <div class="renglon">
                <div class="container-el">
                  <label for="">Hotel</label>
                  <div class="container-select">
                    <select class="chosen required-hotel" name="departure_hotel">
						          <option value="">{{trans('tour.select_hotel')}}</option>
						          @foreach ($hoteles as $hotel)
						          <option value="{{ $hotel->id }}">{{ $hotel->nombre }}</option>
						          @endforeach
						        </select>
                  </div>
                </div>
                <div class="container-el">
                  <label for="">{{trans('transportation.other_hotel')}}</label>
                  <input class="required-hotel" type="text" name="departure_hotel_name" value="">
                </div>
                <div class="container-el">

                </div>
              </div>
            </div>
					</div>

					<div class="detail-transport-buttons">
						<div class="">

						</div>
						<div class="button">
							<button class="button-pay-arrival" type="button" name="button" data-button="payarrival">PAY ON ARRIVAL</button>
						</div>
						<!-- <div class="shared-shuttle">
							<button class="button-back-white-blue" type="button" name="button" data-button="continueshopping">
								<span class="icon-add-to-cart"></span>
			          <span>{{trans('app.add_continue_button')}}</span>
							</button>
						</div> -->
						<div class="">
							<button class="button-back-gray-blue" type="button" name="button" data-button="booknow">{{trans('app.book_now_button')}}</button>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
    </div>
  </div>
</section>
