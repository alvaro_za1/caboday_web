<div class="detail-column-right">
  <h3>{{trans('app.book_now')}}</h3>
  <div class="container-calendar">
    <input type="text" name="date" value="{{ date('F, d Y', strtotime('+3 day')) }}" readonly="readonly" class="date-picker">
    <span class="icon-burger-icon"></span>
  </div>
  <div class="detail-form">
    @if ($data->has_vehicle)
    <div class="renglon">
      <div class="container-select">
        <select name="vehicles">
            <option value="">@if($data->id_categoria_tour == 7) Group price @else Vehicle @endif</option>
            @foreach ($vehicles as $vehicle)
            <option value="{{ $vehicle->id }}" data-price="{{ $vehicle->precio }}">{{ $vehicle['nombre_'.Session::get('language')] . ' $'.$vehicle->precio . ' USD' }}</option>
            @endforeach
        </select>
      </div>
    </div>
    @else
    <div class="renglon">
      <div class="container-pax">
        <div class="">
          <select name="adults" data-price="{{ $data->precio_adulto }}">
              <option value="">{{trans('tour.adults')}}</option>
              @foreach (range(1, 20) as $numero)
              <option value="{{ $numero }}">{{ $numero }} {{trans('tour.adults')}}</option>
              @endforeach
          </select>
        </div>
        <div class="">
          <label for="adults">$<span>0</span> USD</label>
        </div>
      </div>
    </div>
    <div class="renglon">
      <div class="container-pax">
        <div class="">
          <select name="children" data-price="{{ $data->precio_menor }}">
              <option value="0">{{trans('tour.children')}}</option>
              @foreach (range(1, 20) as $numero)
              <option value="{{ $numero }}">{{ $numero }} {{trans('tour.children')}}</option>
              @endforeach
          </select>
        </div>
        <div class="">
          <label for="children">$<span>0</span> USD</label>
        </div>
      </div>
    </div>
    @endif
    <div class="renglon">
      <div class="container-select">
        <select class="chosen" name="hotel" data-tourid="{{ $data->id }}">
          <option value="">{{trans('tour.select_hotel')}}</option>
          <option value="-1">{{trans('tour.no_hotel')}}</option>
          @foreach ($hoteles as $hotel)
          <option value="{{ $hotel->id }}" data-area="{{ $hotel->id_area }}">{{ $hotel->nombre }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="renglon" style="display:none" data-el="hotel-name">
      <div class="container-select">
        <input type="text" name="hotel_name" value="" placeholder="{{trans('tour.placeholder_hotel_name')}}">
      </div>
    </div>

    <div class="renglon">
      <div class="container-select">
        <select class="" name="pick">
          <option value="">{{trans('tour.pick_title')}}</option>
        </select>
      </div>
    </div>
    <div class="renglon" style="display:none" data-el="pickup-note">
        <p>{{trans('tour.no_hotel_msg')}}</p>
    </div>

    @if ($data->has_box_lunch == 1 || $data->has_fins == 1)
    <div class="renglon" style="display:none" data-el="lunchesfins" data-lunches="{{ $data->has_box_lunch }}" data-fins="{{ $data->has_fins }}">
        <a style="text-decoration:underline;" href="">{{trans('tour.modify_lunches')}}<a>
    </div>
    @endif

  </div>
  <div class="detail-form">
    <table class="table-summary">
      <tr>
        <td>
          <label for="">Sub total</label>
        </td>
        <td>
          <label for="subtotal">$<span>0</span> USD</label>
        </td>
      </tr>
      <tr>
        <td>
          <label for="">{{trans('tour.discount')}}</label>
        </td>
        <td>
          <label for="descuento" data-descuento="{{ $data->descuento }}">{{ $data->descuento*100 }}%</label>
        </td>
      </tr>
      <tr>
        <td>
          <label for="">TOTAL</label>
        </td>
        <td>
          <label for="total">$<span>0</span> USD</label>
        </td>
      </tr>
    </table>
  </div>
  <div class="container-buttons">
    <div class="button">
      <a href="#" data-accion="cart-add" data-id="{{ $data->id }}" data-button="booknow">
        <button class="button-back-gray" type="button" name="button">{{trans('app.book_now_button')}}</button>
      </a>
    </div>
    @if($data->pay_arrival)
      <div class="button">
        <a href="#" data-accion="cart-add" data-id="{{ $data->id }}" data-button="payarrival">
          <button class="button-pay-arrival" type="button" name="button">
            <!-- <span class="icon-add-to-cart"></span> -->
            <span>PAY ON ARRIVAL</span>
          </button>
        </a>
      </div>
    @endif
  </div>
</div>

<!-- Select lunches -->
<select class="lunches" name="" style="display:none">
@foreach ($lunches as $lunch)
<option value="{{ $lunch->id }}">{{ $lunch['nombre_'.Session::get('language')] }}</option>
@endforeach
</select>

<!-- Select fins -->
<select class="fins" name="" style="display:none">
@foreach (range(1, 14) as $numero)
<option value="{{ $numero }}">{{ $numero }}</option>
@endforeach
</select>

<!-- Modals -->
@include('modals.tour-detail-lunchfins')
