<style media="all">

@foreach ($data->images as $image)
.banner-{{ $image->id }}{
  background-image: url({{ Config::get('constants.BANNERS_TOURS_WIDE') . $image->imagen_wide }})
}
@endforeach

@media screen and (max-width: 1400px) and (min-width: 851px){
  @foreach ($data->images as $image)
  .banner-{{ $image->id }}{
    background-image: url({{ Config::get('constants.BANNERS_TOURS_NORMAL') . $image->imagen_normal }})
  }
  @endforeach
}


@media only screen and (max-width: 850px) {
  @foreach ($data->images as $image)
  .banner-{{ $image->id }}{
    background-image: url({{ Config::get('constants.BANNERS_TOURS_RES') . $image->imagen_responsiva }})
  }
  @endforeach
}
</style>
