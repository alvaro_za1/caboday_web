<div class="complete-options">
  <div class="top-options">
    <div class="">
      <h3>{{trans('tour.options_transportation')}}</h3>
    </div>
    <div class="">
      <div class="see-all">
        <a href="{{asset('safe-transportation')}}">{{trans('tour.see_all')}}</a>
      </div>
    </div>
  </div>
  <div class="container-transportation-items">
    @foreach ($transports as $transport)
    <figure>
      <a href="{{asset('safe-transportation/'.str_slug($transport['nombre_'.Session::get('language')]).'/'.$transport->id)}}">
        <div class="transportation-item">
          <div class="">
            <img src="{{ Config::get('constants.THUMBNAIL_TRANSPORTACION') . $transport->imagen }}" alt="{{ $transport['nombre_'.Session::get('language')] }}">
          </div>
          <div class="">
            <div class="transportation-info">
              <div class="">
                <div class="">
                  <img src="{{ Config::get('constants.THUMBNAIL_TRANSPORTACION') . $transport->imagen_marca }}" alt="{{ $transport->nombre_marca }}">
                </div>
                <div class="">
                  <h3 class="h3-title">{{ $transport['nombre_'.Session::get('language')] }}</h3>
                  <p>1-{{ $transport->capacidad }} {{trans('app.passengers')}}</p>
                </div>
              </div>
              <div class="">
                <div class="">
                  <label for="">{{trans('app.starting_at')}}</label>
                </div>
                <h3 class="h3-price">${{ $transport->precio }} USD</h3>
              </div>
            </div>
          </div>
        </div>
      </a>
    </figure>
    @endforeach
  </div>
</div>
