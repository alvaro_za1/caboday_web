<?php
return [
    'almost_there' => 'You\'re almost there',
    'review_order' => 'Please review your order and enter your information to purchase your order.',
    'costumer_information' => 'Customer information',
    'placeholder_name' => 'Full name',
    'placeholder_phone' => '(Lada) Phone number',
    'payment_method' => 'Payment method',
    'order_summary' => 'Order Summary',
    'purchase_order' => 'PURCHASE ORDER',
];
