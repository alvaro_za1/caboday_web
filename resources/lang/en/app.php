<?php
return [
	// HEADER
    'menu_home' => 'HOME',
    'menu_about_us' => 'ABOUT US',
    'menu_tours' => 'TOURS & ACTIVITIES',
    'menu_transport' => 'AIRPORT TRANSPORT',
    'menu_contact_us' => 'CONTACT US',

     // FOOTER
    'footer_payments' => 'We accept these types of payments',
    'footer_links' => 'Useful links',
    'footer_book_phone' => 'Book by phone',
    'footer_feedback' => 'Give your feedback',
    'footer_follow_us' => 'Follow us',

    // LINKS
    'link_conditions' => 'Terms and conditions',
    'link_contact' => 'Contact',
    'link_use' => 'Terms of use',
    'link_policies' => 'Privacy policies',

    // PHONE
    'footer_phone' => 'From Mexico, USA & Canada',

    // RIGHTS
    'footer_rights' => 'All rights reserved.',

    //MAIL
    'confirmation_mail' => 'TOUR CONFIRMATION',
    'download' => 'Download',
    'print' => 'Print',
    'contact_information' => 'CONTACT INFORMATION',

    'contact_mail' => 'CONTACT MESSAGE',
    'message_info' => 'MESSAGE INFORMATION',
    'message_form' => 'Message',

    'book_now' => 'Book now!',
    'learn_more' => 'Learn more',

    'all_tours_title' => 'All tours',
    'private_transportation' => 'Private transportation',
    'shared_shuttle' => 'Shared shuttle',
    'transportation_text' => 'Affordable, safe and punctual airport transportation to all hotels and villas in Los Cabos and its surrounding areas.',
    'passengers' => 'passengers',
    'starting_at' => 'Starting at',
    'add_continue_button' => 'Add and continue shopping',
    'book_now_button' => 'BOOK NOW!',
];
