<?php
return [
    'contact_title' => 'GET IN TOUCH',
    'contact_subtitle' => 'Have any questions? Don\'t be shy! Leave us a message.',
    'contact_info_title' => 'Contact info',
    'form_office' => 'Corporate office',
    'form_name' => 'Name',
    'form_email' => 'Email',
    'form_phone' => 'Phone',
    'form_subject' => 'Subject',
    'form_message' => 'Message',
    'form_submit' => 'SUBMIT',
    'form_telephone' => 'Telephone',
];
