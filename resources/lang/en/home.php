<?php
return [
    'welcome_title' => 'WELCOME TO <br> CABO DAY <br> TRIPS',
    'welcome_text' => 'The best way to explore and <br> live Los Cabo\'s life.',
    'about_us_home_text' => 'We have taken the decision to open Cabo Day Trips <br>after listening and seeing first hand thousands and thousands <br> of unhappy people.',
    'home_chosen_text' => 'WE’VE <br> CHOSEN THE<br>BEST FOR YOU',
    'tour_activities_title' => 'Exciting tours <br>& activities',
    'safe_trans_title' => 'Safe and punctual <br> airport transportation',
    'waiting_for_title' => 'WHAT ARE <br>YOU WAITING <br>FOR?',
];
