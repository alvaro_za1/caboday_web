<?php
return [
	// HEADER
    'menu_home' => 'INICIO',
    'menu_about_us' => 'NOSOTROS',
    'menu_tours' => 'TOURS Y ACTIVIDADES',
    'menu_transport' => 'TRANSPORTE',
    'menu_contact_us' => 'CONTACTO',

     // FOOTER
    'footer_payments' => 'We accept these types of payments',
    'footer_links' => 'Useful links',
    'footer_book_phone' => 'Book by phone',
    'footer_feedback' => 'Give your feedback',
    'footer_follow_us' => 'Follow us',

    // LINKS
    'link_conditions' => 'Términos y Condiciones',
    'link_contact' => 'Contacto',
    'link_use' => 'Terms of use',
    'link_policies' => 'Políticas de privacidad',

    // PHONE
    'footer_phone' => 'From Mexico, USA & Canada',

    // RIGHTS
    'footer_rights' => 'Todos los derechos reservados.',

    //MAIL
    'confirmation_mail' => 'TOUR CONFIRMATION',
    'download' => 'Download',
    'print' => 'Print',
    'contact_information' => 'CONTACT INFORMATION',

    'contact_mail' => 'CONTACT MESSAGE',
    'message_info' => 'MESSAGE INFORMATION',
    'message_form' => 'Message',

    'book_now' => '¡Reserva ahora!',
    'learn_more' => 'Ver más',

    'all_tours_title' => 'Todos los tours',
    'private_transportation' => 'Transportación privada',
    'shared_shuttle' => 'Transportación compartida',
    'transportation_text' => 'Transporte a bajo costo, seguro y puntual al aeropuerto a todos los hoteles y villas en Los Cabos y sus alrededores.',
    'passengers' => 'pasajeros',
    'starting_at' => 'A partir de',
    'add_continue_button' => 'Agregar y continuar comprando',
    'book_now_button' => '¡RESERVAR AHORA!',
];
