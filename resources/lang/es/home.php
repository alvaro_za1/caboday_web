<?php
return [
    'welcome_title' => 'BIENVENIDO A <br> CABO DAY <br> TRIPS',
    'welcome_text' => 'La mejor forma de explorar y vivir la vida de Los Cabos.',
    'about_us_home_text' => 'Hemos tomado la decisión de abrir Cabo Day Trips <br> después de escuchar y ver personalmente miles y miles de personas infelices.',
    'home_chosen_text' => 'HEMOS <br> ELEGIDO LO<br> MEJOR PARA TU',
    'tour_activities_title' => 'Emocionantes tours <br>y actividades',
    'safe_trans_title' => 'Seguro y puntual <br> transporte',
    'waiting_for_title' => '¿QUÉ ESTAS  <br> ESPERANDO?',
];
