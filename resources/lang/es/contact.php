<?php
return [
    'contact_title' => 'PONTE EN CONTACTO',
    'contact_subtitle' => '¿Tiene alguna pregunta? No seas tímido! Déjanos un mensaje.',
    'contact_info_title' => 'Información de contacto',
    'form_office' => 'Oficina',
    'form_name' => 'Nombre',
    'form_email' => 'Correo electrónico',
    'form_phone' => 'Teléfono',
    'form_subject' => 'Asunto',
    'form_message' => 'Mensaje',
    'form_submit' => 'ENVIAR',
    'form_telephone' => 'Teléfono',
];
