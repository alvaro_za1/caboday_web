<?php
return [
    'almost_there' => 'Ya casi finalizas tu compra',
    'review_order' => 'Revise su pedido e ingrese su información para comprar su pedido.',
    'costumer_information' => 'Información al cliente',
    'placeholder_name' => 'Nombre completo',
    'placeholder_phone' => '(Lada) Número de teléfono',
    'payment_method' => 'Método de pago',
    'order_summary' => 'Resumen de compra',
    'purchase_order' => 'COMPRAR ORDEN',
];
